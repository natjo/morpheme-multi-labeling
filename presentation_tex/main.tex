\documentclass[aspectratio=169]{beamer}

\input{mystyle.tex}

\usepackage{multirow}
\usepackage[round]{natbib}   % omit 'round' option if you prefer square brackets
\bibliographystyle{plainnat}

\usepackage{appendixnumberbeamer}
\usepackage[colorinlistoftodos]{todonotes}  % For TODOs
\usepackage{tikz}               % For tables and images
\usetikzlibrary{positioning}    % positioning of tikz stuff
\usetikzlibrary{backgrounds}
\usetikzlibrary{fit}
\usetikzlibrary{math}
\usetikzlibrary{decorations.pathreplacing}
\usepackage{algorithm}          % Pseudoalgorithms
\usepackage{algorithmic}


\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}


\addLogoToAllSlides{other/logo/Mercateo_Logo_Schutzraum_Rot_RGB.png}

\title{Improving Multi-Label Text Classification}
\date{17. May 2019}
\author{Jonas Natzer}
% \instituteWithLogo{other/logo/Mercateo_Logo_Schutzraum_Rot_RGB.png}
% \instituteWithLogoAndName{other/logo/Mercateo_Logo_Schutzraum_Rot_RGB.png}{Mercateo AG}
\instituteWithTwoLogos{other/logo/Mercateo_Logo_Schutzraum_Rot_RGB.png}{other/logo/logo_TUM.pdf}


\begin{document}
\maketitle

\begin{frame}{Outline}
    \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}{What is Multi-Label Text Classification}
    \begin{block}{Single-Label Classification}
        Every instance (feature vector) is associated with a single label
    \end{block}
    e.g. \textit{"appletree"} $\rightarrow$ \textit{"plant"}

    \pause

    \vspace{0.5cm}
    \begin{block}{Multi-Label Classification}
        Every instance (feature vector) is associated with a set of labels simultaneously
    \end{block}
    e.g. \textit{"appletree"} $\rightarrow$ [\textit{"plant"}, \textit{"exterior object"}, \textit{"fruit-bearing tree"}]

\end{frame}

\begin{frame}{Use Cases of ML Classification at Mercateo}
    \begin{wideitemize}
    \item All articles are associated with a set of labels\\
        {\small e.g. \textit{"HP Office Paper"} $\rightarrow$ [\textit{"Allroundpapier", "Druckerpapier", "Farbkopierpapier"}]}
    \item Improves article search, automatic recommendation system, etc.
    \item Currently most labels exist in German portal
    \item<conclusion@1-> Our algorithm learns automatic labelling of articles in other languages based on German duplicates
    \end{wideitemize}
\end{frame}


\begin{frame}{Evaluation Metrics: F1 Score}
    The $F_1$ score is a metric with the feature to unite precision and recall in a single value
    \begin{equation*}
        F_1 = 2 * \frac{precision * recall}{precision + recall}
    \end{equation*}

    \pause
    \vspace{0.3cm}

    %He proposes macro and micro, and can be cited for F1 score kind of: \cite{zhang2014review}
    Methods of combining separate metrics for each label into single values:
    \begin{itemize}
        \item \textit{macro-average:} $F_{1-macro} = \frac{1}{N}\sum_{l=1}^N F_1\left(TP_l, FP_l, TN_l, TP_l\right)$\\
        \item \textit{micro-average:} $F_{1-micro} = F_1\left(\sum_{l=1}^N TP_l, \sum_{l=1}^N FP_l, \sum_{l=1}^N TN_l, \sum_{l=1}^N TP_l\right)$
    \end{itemize}

\end{frame}

\begin{frame}{Our Model}
    \begin{figure}[H]
        \scalebox{0.5}{
            \input{other/tikz/full_model_cp}
        }
        \caption{Visualization of the model structure used in our experiments based on Till's work}
    \end{figure}
\end{frame}

\begin{frame}{Our Model}
    \begin{figure}[H]
        \scalebox{0.65}{
            \input{other/tikz/bilstm.tex}
        }
        \vspace{-0.2cm}
        \caption{Structure of a bidirectional LSTM (BiLSTM)}
    \end{figure}
\end{frame}

\section{Contribution of This Work}
\begin{frame}{Main Objectives of this Thesis}
    \begin{wideenumerate}
    \item Improvement of the \textbf{overall prediction performance} through \\ subword unit segmentation
    \item Improvement of prediction performance of \textbf{infrequent labels}
    \end{wideenumerate}
    % In the following list we want to shortly name the main proposed goals of this thesis, which we
    % aim to obtain through sophisticated algorithms for subword unit segmentations:
    % • Improvement of the prediction quality through the model’s ability to understand unknown
    % words during the classification task and through the enhanced learning of known words
    % due to a greater number of samples per word
    % • Decrease of the model size and its parameters and consequently reduction of the re-
    % quired training time until a model’s convergence
    % Besides the improvements through word segmentation approaches, as a second objective
    % we elaborate different techniques with the goal for the
    % • Improvement of the model’s prediction quality for infrequently occurring labels
\end{frame}

\subsection{Subword Unit Segmentations}
\begin{frame}{Motivation}
    \begin{wideitemize}
    \item Infinite number of possible words
    \item Most words are combinations of subword units
        \begin{itemize}
            \item "played" $\rightarrow$ "play-ed"
            \item "appletree" $\rightarrow$ "apple-tree" or "app-le-tr-ee"
        \end{itemize}
        \pause
    \item<conclusion@1-> Understanding the subword units often leads to understanding of the composed word
        \pause
    \item<conclusion@1-> Reduce embedding size and number of unknown words
    \end{wideitemize}
\end{frame}

\begin{frame}{Word Segmentation}
    \begin{wideitemize}
    \item Divide input text by whitespaces and punctuation characters
    \item Simplest approach, low computational costs
        % \item[] e.g. \textit{801803 - Demolition plate}
        \pause
    \item<conclusion@1-> Baseline for further approaches
    \end{wideitemize}
\end{frame}

\begin{frame}{Morphemes}
    \begin{block}{Definition}
        Smallest grammatical unit which still carries a defined meaning
    \end{block}

    % label: phoenix_id, 4329-PHO000001211
    e.g. \textit{801803 - Demolition plate} \qquad $\rightarrow$ \qquad \textit{8 01803 - Demo l ition plate}
    \pause

    \begin{itemize}
        \item Algorithm by \citet{virpioja2013morfessor}
        \item Create a lexicon with 2 design principles:
            \begin{itemize}
                \item minimize number of elements
                \item minimize description length
            \end{itemize}
        \item Generate lexicon in advance on large text corpus\\
            $\rightarrow$ efficient during segmentation
    \end{itemize}
\end{frame}

\begin{frame}{Byte Pair Encoding (BPE)}
    \begin{block}{Definition}
        Smallest lexicon of character sequences, which can still reconstruct the original data loss free
    \end{block}

    e.g. \textit{801803 - Demolition plate} \qquad $\rightarrow$ \qquad \textit{80 1803 - Dem oli tion plate}
    % 80@@ 1803 - Dem@@ oli@@ tion plate - TR - TEAR OFF PLATE

    \pause

    \begin{itemize}
        \item Simple compression algorithm by \citet{gage1994new}:
        \item Learn segmentation model in advance on large text corpus\\
            $\rightarrow$ efficient during segmentation
    \end{itemize}
\end{frame}

\begin{frame}{Byte Pair Encoding (BPE)}
    \begin{figure}[H]
        \scalebox{0.7}{
            \input{other/tikz/bpe_example}
        }
        \caption{Example of a segmentation into byte pairs}
    \end{figure}
\end{frame}

% \begin{frame}{Byte Pair Encoding (BPE)}
%     \begin{columns}[T] % align columns
%         \begin{column}{.58\textwidth}
%             \begin{figure}[H]
%                 \scalebox{0.7}{
%                     \input{other/tikz/bpe_example}
%                 }
%                 \caption{Example of a segmentation into byte pairs}
%             \end{figure}
%         \end{column}%
%         \hfill%
%         \begin{column}{.55\textwidth}
%             \begin{itemize}
%                 \item Identify most frequently appearing sequence of two symbols
%                 \item Replace this sequence with a single symbol
%                 \item Repeat until stopping condition fulfilled
%             \end{itemize}
%         \end{column}%
%     \end{columns}
% \end{frame}

\begin{frame}{Experiments: Data Sets}
    \begin{table}
        \scalebox{0.90}{
            \input{other/tables/datasets.tex}
        }
        \caption{Key figures of the different data sets used in this work}
    \end{table}
\end{frame}

\begin{frame}{Experiments: Improvements on the Embedding Process}
    \begin{table}[H]
        \input{other/tables/vocab_size.tex}
        \caption{Comparison of the vocabulary size}
        Vocabulary size reduced by up to 86.43\%. % 1,500,000 to 200,000
    \end{table}
\end{frame}

\begin{frame}{Experiments: Improvements on the Embedding Process}
    \begin{table}[H]
        \input{other/tables/ut.tex}
        \caption{Comparison of unknown words in validation set}
        Number of unknown words reduced by up to 98.80\%. % 0.25 to 0.009
    \end{table}
\end{frame}
\begin{frame}{Experiments: Results}
    \begin{table}[H]
        \input{other/tables/training_time.tex}
        \caption{Comparison of training times until convergence}
        Training time reduced by up to 55\% %33\%, 55\%, 16\% and 5\%
    \end{table}
\end{frame}


\begin{frame}{Experiments: Results}
    \begin{table}[H]
        \small
        \input{other/tables/all_segmentations_comparison.tex}
        \caption{Comparison of different data sets on a single run with optimal configuration}
    \end{table}
\end{frame}

\begin{frame}{Experiments: RCV1}
    \begin{table}[H]
        \input{other/tables/rcv1_comparison.tex}
        \caption{Comparison of results using morpheme segmentation on the RCV1 data set with state of the art}
    \end{table}
\end{frame}


\subsection{Improvement of Prediction of Infrequent Labels}
\begin{frame}{Motivation}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_until_convergence_segments_0.00-0.01_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_until_convergence_segments_0.75-1.0_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{Macro F1 score for different sets of labels using different segmentations on Mercateo-FR}
    \end{figure}
\end{frame}

\begin{frame}{Adjusting the Data Set}
    \begin{wideitemize}
    \item Model focuses on labels occurring frequently
    \item<conclusion@1-> Create new data set with an equal number of samples for each label
    \end{wideitemize}
\end{frame}

\begin{frame}{Adjusting the Data Set: Results}
    \begin{table}[H]
        \input{other/tables/adjusted_data_set.tex}
        \caption{Key figures of the newly created data sets from the original Mercateo-FR data set}
    \end{table}
\end{frame}

\begin{frame}{Inverse Frequency Loss}
    Improve \textit{sigmoid cross entropy loss} to consider each labels frequency in the data set:
    \vspace{0.5cm}
    \begin{equation*}
        l_{ifl}(y,y') = - \sum_{l}^N \underbrace{\frac{1}{f_l}}_\text{frequency factor} * \underbrace{\left({y_l' \log(y_l) + (1-y_l') \log (1-y_l)}\right)}_\text{Sigmoid Cross Entropy}
    \end{equation*}
\end{frame}

\begin{frame}{Experiments}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_improve_macro_bpe_on_converged_all_no_dropout_segments_0.00-0.01_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_improve_macro_bpe_on_converged_all_no_dropout_segments_0.75-1.0_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{Macro F1 score for different sets of labels on Mercateo-FR using BPE segmentation}
    \end{figure}
\end{frame}

\section{Conclusion \& Outlook}
\begin{frame}{Conclusion}
    \begin{block}{Subword Unit Segmentation}
        \begin{itemize}
            \item Reduction of embedding layer size, training time and number of unknown words
            \item Improvement of overall prediction performance
        \end{itemize}
    \end{block}

    \pause
    \vspace{1.0cm}

    \begin{block}{Infrequently Occurring Labels}
        \begin{itemize}
            \item Improve prediction performance of infrequent labels in exchange for decreased performance on frequent labels
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Outlook}
    \begin{wideitemize}
    \item Evaluate segmentation approach further on different languages
        \pause
    \item Utilize more advanced techniques for the task of improving the prediction quality of infrequent labels \textit{(e.g. adapt model architecture)}
    \end{wideitemize}
\end{frame}


% \begin{frame}{Third Frame}
% \begin{columns}[T] % align columns
%     \begin{column}{.48\textwidth}
%         Hi
%     \end{column}%
%     \hfill%
%     \begin{column}{.48\textwidth}
%         Hi
%     \end{column}%
% \end{columns}
% \end{frame}

% [allowframebreaks] if it is getting too long
\begin{frame}{References}
    \tiny
    \bibliographystyle{amsalpha}
    \bibliography{other/library.bib}
\end{frame}

% \appendix % Will not show up in toc, no page numbering and no progress bar, but logo is currently missing
\section*{Backup Slides}
\begin{frame}[noframenumbering]{Results: Mercateo-FR}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_until_convergence_micro_f1_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_until_convergence_macro_f1_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{F1 scores on Mercateo-FR for a single run with optimal configuration}
    \end{figure}
\end{frame}

\begin{frame}[noframenumbering]{Results: Mercateo-PL}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/M_PL_until_convergence_micro_f1_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/M_PL_until_convergence_macro_f1_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{F1 scores on Mercateo-PL for a single run with optimal configuration}
    \end{figure}
\end{frame}

\begin{frame}[noframenumbering]{Results: Amazon Reviews}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/amazon_until_convergence_micro_f1_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/amazon_until_convergence_macro_f1_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{F1 scores on Amazon Reviews for a single run with optimal configuration}
    \end{figure}
\end{frame}

\begin{frame}[noframenumbering]{Results: RCV1}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/rcv1_until_convergence_micro_f1_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/rcv1_until_convergence_macro_f1_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{F1 scores on RCV1 for a single run with optimal configuration}
    \end{figure}
\end{frame}

\begin{frame}[noframenumbering]{Morphemes}
    \begin{figure}[H]
        \scalebox{0.7}{
            \input{other/tikz/morpheme_example.tex}
        }
        \caption{Example of a segmentation into Morphemes by \cite{virpioja2013morfessor}}
    \end{figure}
\end{frame}

\begin{frame}[noframenumbering]{Adjusted Data Set}
    \begin{wideitemize}
    \item Design principle:
        \begin{itemize}
            \item Start with empty new data set
            \item Iterate multiple times over original data set
            \item Copy data point to new data set if its labels appear useful enough
        \end{itemize}
    \item Evaluation of data point through score function based on current appearance of all labels in the new data set
    \end{wideitemize}

    \vspace{-0.2cm}
    \begin{equation}
        f_{score}(x, c)=
        \begin{cases}
            x^{2*\frac{c+p}{p}},        & \text{if } x > 0\\
            -1 * x^2,                              & \text{otherwise}
        \end{cases}
        % \qquad x \in \mathbb{R}, c \in \mathbb{N}_+, p \in \mathbb{R}_+
    \end{equation}

    \vspace{-0.5cm}


    \begin{equation*}
        x \gets \text{difference between target number appearances and current number of appearances}
    \end{equation*}
\end{frame}

\begin{frame}[noframenumbering]{Algorithm Adjusted Data Set}
    \begin{algorithm}[H]
        \tiny
        \caption{Generate Adjusted Data Set}
        \begin{algorithmic}
            \REQUIRE $\textit{originalData}\;D_o, \textit{minScore}\;s_m, \textit{targetNumPointsPerLabel}\;n_t$
            \ENSURE $\textit{newData}\;D_n$
            \STATE $D_n \leftarrow \text{empty}$
            \WHILE{$\text{not all labels sufficiently in } D_n$}
            \STATE $\textit{currentDataPoint}\;p \leftarrow \text{ next data point of } D_o$
            \STATE $\textit{score}\;s_p \leftarrow 0$
            \FOR{$\textit{label}\;l \text{ in } p$}
            \STATE $\textit{NumPointsPerLabel}\;n_l \leftarrow \text{occurrences of } l \text{ in } D_n$
            \STATE $\delta \leftarrow  n_t - n_l$
            \STATE $s_p \leftarrow s_p + \text{scoreFunction(} \delta \text{)}$
        \ENDFOR
        \IF{$s_p \geq s_m$}
        \STATE $p \text{ add to } D_n$
    \ENDIF
\ENDWHILE
            \end{algorithmic}
        \end{algorithm}
    \end{frame}

    \begin{frame}[noframenumbering]{Experiments}
    \vspace{-0.7cm}
    \begin{figure}
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.85cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_improve_macro_bpe_on_converged_all_no_dropout_micro_f1_plot.pgf}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-0.5cm}
            \scalebox{0.5}{\input{other/all_plots/M_FR_improve_macro_bpe_on_converged_all_no_dropout_macro_f1_plot.pgf}}
        \end{minipage}
        \vspace{-0.9cm}
        \caption{Macro F1 score for different sets of labels on Mercateo-FR}
    \end{figure}
\end{frame}


\end{document}
