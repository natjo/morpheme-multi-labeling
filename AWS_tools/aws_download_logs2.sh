#!/bin/sh
set +x

read TARGETHOST < "./AWS_tools/host_address2.cfg"

downloadFolder="./aws_downloads2"

echo "Download logs and checkpoints from host1 to $downloadFolder"

rsyncCMD="rsync -avz --info=progress2 -e "ssh -i AWS_tools/AWS_permission/JonasNatzer.pem""

if [ -d $downloadFolder ]
then
    echo "Remove $downloadFolder before running this script"
    exit 1
else
    mkdir -p $downloadFolder/logs

    echo "Download logs into $downloadFolder/logs"
    $rsyncCMD "ubuntu@$TARGETHOST":~/morpheme-multi-labeling/log/* ./$downloadFolder/logs

    echo "Download checkpoints into $downloadFolder/checkpoints"
    $rsyncCMD "ubuntu@$TARGETHOST":~/morpheme-multi-labeling/checkpoints/* ./$downloadFolder/checkpoints
fi


