#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Not enough arguments supplied, INSTANCE_NUMBER should be a single number"
    echo "Usage: $0 INSTANCE_NUMBER"
    exit
fi

instanceNumber=$1

read TARGETHOST < "./AWS_tools/host_address$instanceNumber.cfg"

ssh -i "AWS_tools/AWS_permission/JonasNatzer.pem" ubuntu@$TARGETHOST
