#! /bin/sh
set -e
set -u

# Copy from s3 to local aws instance
# first input is the aws directory
# second parameter is the local folder

BUCKET_NAME="jonas-natzer-tf"

new_name=$(basename $1)

aws s3 cp s3://$BUCKET_NAME/$1 $2/$new_name --recursive
