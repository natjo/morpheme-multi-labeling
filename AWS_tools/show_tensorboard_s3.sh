#!/bin/sh
set -e
set -u


S3_NAME="jonas-natzer-tf"
BASE_LOG_DIR="saved_runs"
LOCAL_PORT=6006


if [ "$#" -eq 1 ]; then
    LOG_PATH="s3://$S3_NAME/$BASE_LOG_DIR"
elif [ "$#" -eq 2 ]; then
    LOG_PATH="s3://$S3_NAME/$BASE_LOG_DIR/$2"
elif [ "$#" -eq 3 ]; then
    LOG_PATH="s3://$S3_NAME/$BASE_LOG_DIR/$2,s3://$S3_NAME/$BASE_LOG_DIR/$3"
elif [ "$#" -eq 4 ]; then
    LOG_PATH="s3://$S3_NAME/$BASE_LOG_DIR/$2,s3://$S3_NAME/$BASE_LOG_DIR/$3,s3://$S3_NAME/$BASE_LOG_DIR/$4"
else
    echo "Not enough input arguments, use like this: $0 HOST_NUMBER [LOG_DIR1] [LOG_DIR2] [LOG_DIR3]"
    return
fi

instanceNumber=$1
read TARGETHOST < "./AWS_tools/host_address$instanceNumber.cfg"


ssh -i "AWS_tools/AWS_permission/JonasNatzer.pem" -o ServerAliveInterval=60  -t -L$LOCAL_PORT:localhost:6006 "ubuntu@$TARGETHOST" "
. .bashrc;
source activate tensorflow_p36;
export AWS_REGION=eu-west-1;
./workaround_tb_bug/venv/bin/tensorboard --logdir=$LOG_PATH"
# tensorboard --logdir=$LOG_PATH"
