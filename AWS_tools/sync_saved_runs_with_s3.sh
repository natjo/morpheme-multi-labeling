#! /bin/sh
set -e
set -u

LOCAL_DIR="$HOME/morpheme-multi-labeling/saved_runs"
S3_DIR="/saved_runs"
BUCKET_NAME="jonas-natzer-tf"

aws s3 sync $LOCAL_DIR s3://$BUCKET_NAME$S3_DIR
