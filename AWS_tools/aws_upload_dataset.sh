#! /bin/sh

if [ $# -ne 2 ]; then
    echo "Not enough arguments supplied"
    echo "Usage: $0 INSTANCE_NUMBER PREPROCESSED_FOLDER"
    exit
fi

instanceNumber=$1
preprocessedFolder=$2
TARGET_DIR="~/new_dataset"  # I still have to put it by hand in the right directory, that's on purpose
read TARGETHOST < "./AWS_tools/host_address$instanceNumber.cfg"

rsyncCMD="rsync -avz --copy-links --info=progress2 -e \"ssh -i AWS_tools/AWS_permission/JonasNatzer.pem\""


echo "Copy preprocessed folder in $preprocessedFolder to server in target location $TARGET_DIR/$datasetName"
eval "$rsyncCMD $preprocessedFolder ubuntu@$TARGETHOST:$TARGET_DIR"
