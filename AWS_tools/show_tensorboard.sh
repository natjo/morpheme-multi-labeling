#!/bin/sh
set -e
set -u

LOG_DIR="~/morpheme-multi-labeling/saved_runs/"

if [ $# -lt 1 ]; then
    echo "Not enough arguments supplied, INSTANCE_NUMBER should be a single number"
    echo "Usage: $0 INSTANCE_NUMBER [LOCAL_PORT]"
    exit
fi

instanceNumber=$1
read TARGETHOST < "./AWS_tools/host_address$instanceNumber.cfg"

instanceNumber=$1
if [ "$#" -eq 2 ]; then
    LOCAL_PORT=$2
else
    LOCAL_PORT=6006
fi

ssh -i "AWS_tools/AWS_permission/JonasNatzer.pem" -o ServerAliveInterval=60  -t -L$LOCAL_PORT:localhost:6006 "ubuntu@$TARGETHOST" "
. .bashrc;
source activate tensorflow_p36;
AWS_REGION=eu-west-1 tensorboard --samples_per_plugin text=10000 --logdir $LOG_DIR"
