\chapter{Introduction}
Multi-label classification studies the problem where each real-world object (sample point) is represented by a single instance (feature vector) and is associated with a set of labels simultaneously. Compared to traditional single-label classification in which each instance belongs to a single label, this additional characteristic increases the difficulty to create a proficient model for such a classification task.\\
To name one typical example for multi-label problems, news websites often require labels with the purpose of improved quality of search and recommendation systems in order to allow their users to find their preferred information with high efficiency and less disturbance through irrelevant information.\\
While similar approaches can be used for the multi-label problem as for the single-label problem, the prediction can be improved even further by the usage of additional information such as label co-occurrences. Furthermore, such a 1-vs-All technique using independent classifiers has major limitations. First, training a large number of high dimensional classifiers can be computationally expensive. Second, the cost of prediction might be high since all the classifiers would need to be evaluated every time a prediction is computed.\\
\\
This work focuses on text based multi-label classification, a natural language processing problem (NLP), which is a widely studied field in machine learning. This field gained traction through modern improvements of machine learning models, such as the \textit{long short-term memory} (LSTM) neural network architecture \cite{hochreiter1997long}, which enables the network to process long inputs as sequences, or powerful word embeddings, representing words with similar contexts in close proximity to one another in their embedded space (e.g. \textit{word2vec} \cite{mikolov2013distributed}).\\
We evaluate an additional approach, which can be applied jointly with most modern improvements. The main idea of this thesis is the segmentation of the original words into meaningful subword components. While the simplest algorithm would be to simply divide each word into its characters, we want to evaluate more sophisticated segmentation algorithms.\\
\\
During our experiments we further noticed a decline in the prediction quality of infrequently occurring labels compared to frequently occurring labels. This observation motivated us throughout this project to propose new techniques with the goal of improving the prediction quality of infrequently occurring labels.


\newpage
\section{Motivation}
Arbitrary combination of single characters enables the creation of an infinite number of different words, yet a proficient speaker of a language can effortlessly understand words they might have never encountered before, if they are related to a similar word they already know, as for example \textit{"funemployed"} or \textit{"blueish"}.\\
Such proficient speakers only require knowledge of related words - in our example \textit{"fun"} and \textit{"employed"} or \textit{"blue"} - and are able to conclude the meaning of the new word without explicitly learning it.\\
Beneath compositions of words, inflections of words such as for example \textit{"play"}, \textit{"plays"}, \textit{"played"}, \textit{"playing"} and so forth appear natural and easy to be understood for a human speaker.\\
\\
Machine learning models on the other hand have no concept of words as they simply learn arbitrary combinations of characters through numerical representations. They are not able to grasp an intrinsic meaning behind these combination of characters and therefore fail to identify relations between different words. Circumstances such as changing of a single character which can completely alter the meaning of a word (e.g. \textit{"sea"} and \textit{"see"}) complicate this task even further.\\
\\
Thus our goal is to evaluate approaches which enable a model to understand these words in a more natural way:\\
As already addressed we can split words into segments. To better illustrate our reason behind this approach, we would like to name the word \textit{"Apfelbaum"}. Supposing this word is unknown to an English speaker, we assume to explain to her, that \textit{"Apfel"} translates to \textit{"apple"} and \textit{"Baum"} to \textit{"tree"}. With this knowledge, she can conclude without any German language skills, that \textit{"Apfelbaum"} translates to \textit{"apple tree"} in the English language, just as a machine could conclude with this foreknowledge of the word's components.\\
This additional knowledge would allow the machine to label the word \textit{"Apfelbaum"} depending on the task as for example \textit{"fruit-bearing tree"} or \textit{"flowering plant"}. Even if the machine only learned the  word \textit{"Baum"} without knowing the word \textit{"Apfel"}, it could predict labels such as \textit{"garden element"} or \textit{"exterior object"}.\\
\\
As there are no clear rules of how to split all words in any language, we introduce a set of different algorithms for this purpose and evaluate their respective feasibility throughout this work.


% \section{Use Cases}
% There are several applications motivating
% - Good for learning large bodies of text, especially for languages with a lot of constructed words
% - Also good for cryptic texts with a lot of constructed ids, and article names, such as online stores or data bases


\newpage
\section{Contribution of This Work}
In the following list we want to shortly name the main proposed goals of this thesis, which we aim to obtain through sophisticated algorithms for subword unit segmentations:

\begin{itemize}
\item Improvement of the prediction quality through the model's ability to understand unknown words during the classification task and through the enhanced learning of known words due to a greater number of samples per word

\item Decrease of the model size and its parameters and consequently reduction of the required training time until a model's convergence
\end{itemize}

\noindent
Besides the improvements through word segmentation approaches, as a second objective we elaborate different techniques with the goal for the

\begin{itemize}
\item Improvement of the model's prediction quality for infrequently occurring labels
\end{itemize}
