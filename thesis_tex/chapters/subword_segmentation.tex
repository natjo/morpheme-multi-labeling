\chapter{Improvement of Multi-Label Text Classification Through Subword Unit Segmentation} \label{sec_subwords}
One of the main contributions of this work is the evaluation and comparison of different subword unit segmentations. An in-depth explanation of the theory behind each segmentation process as well as a more detailed explanation behind the motivation for this approach follows in the next sections.
We also present a number of commonly used modifications of our data set and training algorithm which is known to further improve the prediction quality of our model.


\section{Different Subword Unit Segmentation Algorithms}
Conventional text classification models learn to process text input word by word, transforming each word of the input sequence into its learned representation in an $N$-dimensional feature space. The resulting $N$-dimensional feature vector is then further processed by an arbitrary classification algorithm, which is in our case a neural network.\\
Problems oftentimes arise for words that have never been encountered during the training phase. The model has never learned to recognize any patterns containing these unseen words and thus has no means to predict reasonable labels for input sequences containing these words.\\
\\
A common solution to this issue is the introduction of a single numerical representation for all unknown words. This way the network can learn patterns containing unknown words and thus can reduce the input noise during prediction time. A comprehensive explanation of this approach is given in section \ref{sec_unknown_token}.\\
\\
Our approach is meant to decrease this issue by dividing unknown words into known subwords. We expect this to decrease the problem of unknown words through the division of unknown words into known subwords. We assume this in return to improve the overall prediction quality of the model as on the one side there exist fewer unknown words, on the other side there are more data points per feature as we reduce the overall number of features.\\
The following sections \ref{sec_basic_subwords} to \ref{sec_omitting_char_level} give a detailed explanation of the algorithms we applied during the experiments.

\subsection{Basic Design Concept} \label{sec_basic_subwords}
Languages contain complex grammatical structures and most words have a variety of inflections which enable them to adapt to their position and purpose in each sentence. For example the words \textit{"probably"} and \textit{"probability"} have the same word stem: \textit{"probab-"}, which indicates the meaning of the compositional words. While this identical meaning seems obvious and natural to a human speaker, a text classification algorithm will struggle to see this fact, as all similarities between these words get lost during the transformation into their respective numerical representations.\\
A similar problem arises through the usage of prefixes or postfixes, which alter the meaning of a word, yet retain the overall topic. Examples for this are the prefix \textit{"im-"}, which inverses the meaning as in the case of \textit{"improbable"}, or the postfix \textit{"-s"} which indicates the magnitude of the implied object.\\
This list of subword types is by far not exhaustive and is only meant to give the reader an insight into the key idea behind our approach. As languages have very complex and intricated rules for creating and inflecting words, no complete and definite list of subwords exists. This problem expands as there is not even a unique definition of finding subwords in a language.\\
For this reason we explored different possible approaches to segment a given text into its subword representation. A discussion of the to us most interesting concepts can be found in the next sections.\\
\\
Besides the advantage of an improved understanding of words, the segmentation is expected to reduce the number of unique features the classification algorithm needs to learn, since there is no requirement to learn for example a numerical representation of the word \textit{"improbable"}, if the algorithm already knows the subwords \textit{"im"} and \textit{"probable"}.\\
This reduced, embedded feature space also increases the number of training samples per word, as the words \textit{"improbable"} and \textit{"probability"} contain identical subwords.\\
Additionally it shows the importance of the recurrent neural network, as the order and combination of sequential features becomes even more relevant for subunits.\\
\\
The basic workflow for each approach is the same: we first train an independent model on the entire data set to learn the respective segmentation approach. Then we use this model to segment our original data set into the segmented input sequences. Despite the existence of pretrained models for most segmentation tasks, our data sets contained too many proper names, which models trained on public data sets have never seen and therefore are not able to properly divide. This was the reason we chose to train all models on our data sets.


\subsection{No Segmentation as Baseline}
The first approach we used is no segmentation, but processing each word as one distinct feature by the network. These word inputs are filtered as described in section \ref{sec_data_cleaning}. This is meant to give us a baseline for comparisons with more sophisticated segmentation algorithms.


\subsection{Morphemes}
As already discussed earlier, most words are composed of smaller subword units. Different combinations of such subwords can create words with different meaning.
\\If these subwords are the smallest grammatical unit which still carries a defined meaning, they are called morphemes \cite{beard1995lexeme}. Figure \ref{tikz_morpheme_example} depicts an example of the segmentation of a sentence into its morphemes through a pretrained segmentation model.

\begin{figure}[H]
    \centering
    \input{tikz/morpheme_example}
    \caption{Example of a segmentation into morphemes by \citet{virpioja2013morfessor}}
    \label{tikz_morpheme_example}
\end{figure}

The segmentation into morphemes has been widely utilized in natural language processing applications such as speech recognition (e.g. \cite{gelas2012developments}), speech retrieval (e.g. \cite{virpioja2013morfessor}) and machine translation (e.g. \cite{clifton2011combining}). As a result, well explored algorithms and their implementations are publicly available. Due to limited time we used the implementation by Virpioja et al. \cite{virpioja2013morfessor}.\\
Their algorithm tries to generate a so called optimal lexicon $\theta$ of morphemes, from which the entire training data $D_w$ can be reconstructed through composition solely of entries from this produced lexicon.\\
The cost function to evaluate the lexicon $\theta$ is derived using maximum a posteriori estimation, which is described in formula \ref{eq_MAP_morphemes}.

\begin{equation} \label{eq_MAP_morphemes}
    \theta_{MAP} = \underset{\theta}{\arg\max}\,p(\theta)\,p(D_w|\theta)
\end{equation}

Thus the goal is to maximize the product of the model prior $p(\theta)$, which follows the principle of the minimum description length, and the data likelihood $p(D_w|\theta)$, which describes how well the data set $D_w$ can be reconstructed using the given lexicon $\theta$. As usual, the cost function to minimize is set as the negative logarithm of the product:

\begin{equation} \label{eq_loss_morphemes}
    L(\theta,D_w) = -log\,p(\theta) - log\,p(D_w|\theta)
\end{equation}

\newpage
\subsubsection{Basic Description of the Algorithm}

The algorithm to learn the optimal lexicon $\theta$ is described in procedure \ref{algo_generate_morpheme_lexicon}.\\
It starts by initializing the lexicon with all unique words in the original data set $D_w$. Then it iterates multiple times over the current lexicon $\theta$. At each iteration step it tries to divide the currently from the lexicon chosen word into 2 segments, which would optimize, if they would replace the original word in the lexicon, the loss function $L(\theta,D_w)$ as described in equation \ref{eq_loss_morphemes}.\\
The optimization algorithm runs until the loss decreases less than a specified value $\epsilon$.

\begin{algorithm}[H]
    \caption{Generate Morpheme Lexicon}
    \label{algo_generate_morpheme_lexicon}
    \begin{algorithmic}
        \REQUIRE $\textit{originalData}\;D_w$
        \ENSURE $\textit{Lexicon}\;\theta$
        \STATE $\theta \leftarrow \text{unique words of }D_w$
        \STATE $L_{old} \leftarrow \infty$
        \STATE $L_{new} \leftarrow L(\theta,D_w)$
        \WHILE{$L_{new} < L_{old} - \epsilon$}
            \STATE $w \leftarrow \text{get next word from } \theta$
            \IF {$\text{division of }w \text{ improves }L$}
                \STATE $\theta \leftarrow \theta \text{ with }w\text{ replaced by its divisions}$
                \STATE $L_{old} \leftarrow L_{new}$
                \STATE $L_{new} \leftarrow L(\theta,D_w)$
            \ENDIF
        \ENDWHILE
    \end{algorithmic}
\end{algorithm}


\subsection{Byte Pair Encoding}
Byte pair encoding (BPE) follows a different key idea than the morpheme approach discussed in the last section.\\
While the morpheme segmentation focuses on dividing words into their smallest units, which still contain a distinct meaning, byte pair encoding is a simple form of data compression \cite{gage1994new}, and thus does not identify a logical meaning behind its segmentations. It simply tries to generate the smallest lexicon of subunits, which can still reconstruct the original data loss free.\\
It achieves this by iterating over the data set and by identifying the sequence of the two symbols, which appear the most frequently, at each step. It then replaces all occurrences of this symbol sequence by a single new symbol. This process is repeated until no symbol appears with a specified minimum frequency. Figure \ref{tikz_bpe_example} visualizes an example run of the algorithm.\\
Byte pair encoding was already successfully used in different machine learning fields such as machine translation (e.g. \cite{sennrich2015neural}) and speech recognition (e.g. \cite{zeyer2018improved}).

\begin{figure}[H]
    \centering
    \input{tikz/bpe_example}
    \caption{Example of a segmentation into byte pairs}
    \label{tikz_bpe_example}
\end{figure}


\subsection{Reason for Omitting Encoding on Character Level} \label{sec_omitting_char_level}
Another approach for segmenting the original text data is to divide each word into its characters. This concept can be found in the research community and has been shown to be successful in certain use cases (e.g. \cite{zhang2015character}, \cite{xiao2016efficient}). It's main advantage is the extreme generic feature space, removing the possibility of any unknown inputs, as is a remaining issue with our approaches.\\
Yet we identified a problem, which led us to the decision to not follow this approach further: while a segmentation into subwords reduces the feature space through the combination of learned subwords, a character based encoding takes this approach into the extreme, reducing the feature space depending on the language to under 30 different input features (one feature per character of the language).\\
All information between these characters must be learned and saved in the parameters of the neural network and since letters are basically meaningless on their own, we expect as a result an unproportionally large network and greatly increased training times to contain all the information available in training texts. While this approach can be feasible, as was already shown (e.g. \cite{zhang2015character}, \cite{xiao2016efficient}), we think the disadvantages of the increased training and memorizing complexity do not surpass the benefits gained through an extreme generic feature space.


\section{Pretrained Feature Embedding} \label{sec_pretrained_embedding}
As already discussed in the beginning of this chapter, each word is represented by a unique numerical vector. While it is possible to use a random vector as the representation for each word, a better prediction can be achieved through a more meaningful word embedding.\\
\\
The most straight forward way of improving the word representation is to learn its embedding as a part of the model. This means in the context of our neural network to create an embedding layer, which functions as a look up table to transform input words into numerical vectors. This embedding layer is initialized with random values and can be trained in the same way and concurrently with all following and preceding layers.\\
It should be noted that this embedding layer can yield an excessively large number of trainable parameters depending on the data set.\\
\\
Over the last years an increasing set of intelligent algorithms capable of learning meaningful word embeddings, separate from the text classification algorithm itself, were introduced.\\
One of the most successful algorithms is \textit{word2vec}, developed by Mikolov et al. in 2013 \cite{mikolov2013distributed}. Its key objective is to locate words with similar contexts in close proximity to one another in their embedded space. This representation can be obtained by using either of two methods: \textit{Skip-Gram} or \textit{Continuous Bag Of Words} (CBOW).\\
For the CBOW approach, the model learns to predict the current word from a window of surrounding words. As the name already hints, the order of the contextual words does not influence the prediction (\textit{bag-of-words} assumption).\\
The Skip-Gram approach works the other way around: the model learns to utilize the current word to predict the surrounding window of words.\\
\\
There are further improvements to the learning algorithm of word2vec, such as \textit{hierarchical softmax} and \textit{negative sampling}. The hierarchical softmax method utilizes a \textit{Huffman tree} to reduce required calculations, while the \textit{negative sampling method} improves the training time by reducing the number of samples evaluated at every step \cite{mikolov2013distributed}.\\
While there are pretrained word2vec models, our data sets proofed to have too many uncommon words, which pretrained models have never learned to transform into meaningful, numerical vectors. To train our own model, we used the implementation provided by the \textit{SciPy} Python library \cite{numpy}.


\section{Unknown Feature Handling Using Dedicated Token} \label{sec_unknown_token}
We already presented the problem of unknown words in the data set and the therewith related missing numerical representation.\\
Our solution to this problem is the introduction of a special symbol, which is used instead of an unknown word during the prediction of a new data sample. In order to learn a useful presentation of this symbol we randomly replaced words or subunits in the training set by this unknown token. As a result the model learns to handle unknown tokens. We only replaced words during training if they have already been seen by the network before. This ensures the largest possible embedding containing representations for the utmost number of different words.


