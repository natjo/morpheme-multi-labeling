\chapter{Background}
Before discussing the main subject, this chapter is meant to give an overview over the most important background information, which is strongly relevant for the understanding of this work. As this can not be an exhaustive collection of information, the focus is set on the relevant key ideas. We expect the reader to already have knowledge about the principles behind neural networks, the backpropagation algorithm utilized for training a model and similar topics in the field of machine learning.\\
Furthermore, we do not discuss every topic in detail for reasons of clarity and comprehensibility. In such a case we always supply references as a source to read further into the addressed topic.

\section{State of the Art Multi Label Machine Learning on Text} \label{sec_state_of_the_art}
As a significant task of natural language processing, various methods have been proposed by the research community and have progressively accomplished more and more sophisticated challenges.\\
One of the first approaches was to treat the multi-label classification task as a set of multiple single-label classification tasks. \citet{boutell2004learning} (\citeyear{boutell2004learning}) utilized \textit{support vector machines} (SVM) to learn these single-label classifications. Later on with the development of deep neural networks, such methods were applied to these multiple single-label classification tasks and achieved several improvements (e.g. \citet{nam2014large}, \citeyear{nam2014large}).\\
Nonetheless, this split into single-label classification tasks cannot model the internal correlations among labels, concealing viable information from any machine learning model.\\
Thus a series of neural network architectures was proposed over the course of the next years.\\
The network designed by \citet{kim2014convolutional} (\citeyear{kim2014convolutional}) uses multiple convolution layers to extract text features from words, which are then forwarded to a fully connected layer using a sigmoid function to output the probability distribution over the label space. This is a quite simple approach following up on the success of the convolutional architecture on image classification tasks.\\
In addition to this network, \citet{chen2017ensemble} (\citeyear{chen2017ensemble}) utilized similarly convolutional layers to capture local textual semantics but added subsequently an recurrent layer to capture global semantics and label correlations.\\
Furthermore \citet{lin2018semantic} (\citeyear{lin2018semantic}) successfully proposed a sequence to sequence based model similar to the former architecture by \citet{chen2017ensemble}. They combined a multi-level dilated convolutional network generating higher-level semantic unit representations with a corresponding hybrid attention mechanism utilizing one or more LSTM layers. This combination allows the model to extract both the information at the word-level and the level of the semantic unit for a classification. This model has a high complexity as it combines convolutional layers, LSTM layers and fully connected layers, finally requiring a sophisticated and difficult to optimize design for each particular situation.\\
\\
Other successful machine learning models have been proposed aside from neural networks:
\citet{prabhu2014fastxml} (\citeyear{prabhu2014fastxml}) designed a method utilizing a decision tree, which divides the feature space instead of the label space. Their proposed \textit{FastXML} algorithm achieves significantly higher accuracies by directly optimizing an nDCG based ranking loss function.\\
\citet{bhatia2015sparse} (\citeyear{bhatia2015sparse}) focused on generating a more meaningful input feature space.\\
The main technical contribution in their designed algorithm \textit{SLEEC} is a formulation for learning a small ensemble of local distance preserving embeddings, which can accurately predict infrequently occurring labels. On this newly generated latent space all samples are classified by using the \textit{k-nearest-neighbors} method. Beneath promising prediction results this approach also addresses our second discussed issue, we focus on in this work.\\
\\
None of these mentioned approaches utilize any kind of word segmentation, and up to now we only found smaller projects focusing on this topic such as  using morphemes for single-label text classification of messages in the African Chichewa language (\citet{munro2010subword}, \citeyear{munro2010subword}). Other projects utilized word segmentations for a different task as multi-label learning, such as learning part-of-speech tagging with morphemes (\citet{tamburini2016bilstm}, \citeyear{tamburini2016bilstm}) or utilizing byte pair encoding for machine translation models combining multiple languages in one hidden space (\citet{gehring2017convs2s}, \citeyear{gehring2017convs2s}).\\
\\
Therefore our objective is to evaluate the feasibility of different subword unit segmentation approaches for the task of multi-label classification using a neural network in this work.

\section{Introduction to Long Short-Term Memory Networks}


\subsection{The Problem with Conventional Network Architectures on Sequential Input}
A crucial problem with conventional neural network architectures, such as for example fully connected layers \cite{schmidhuber2015deep}, is learning the correlation of timely distanced features in a sequential input. As these network structures are not able to retain a state, i.e. memory of the last input, the entire input sequence must be processed at once. To reduce the complexity of this problem, the input sequence is often transformed into a \textit{bag-of-words} \cite{mccallum1998comparison}, by converting the sequence into a random set (bag) of its words, disregarding positional information (e.g. beginning/end of sentence) as well as relative information (e.g. adjective to noun relation).\\
\\
Therewith valuable information contained in the order of the input and not in the input features themselves can not be used by those neural networks anymore. To make this problem clearer, see the two following examples sentences:

\newpage
\begin{itemize}
  \item \textit{"I prefer dogs over cats"}
  \item \textit{"I prefer cats over dogs"}
\end{itemize}

Feeding these two sentences into a conventional neural network by applying a \textit{bag-of-words} approach (e.g. \textit{"cats, dogs, I, over, prefer"}) would produce the same output in both cases, as the transformation to a bag-of-words input would remove the order of the sequence. In order to enable the network to understand such ordered input sequences, it needs to be able to maintain an internal state, which contains information about previously seen information, in between input words. In other words, the network needs to be able to maintain a memory of previous inputs.\\
\\
\\
Neural networks with this characteristic are called \textit{"recurrent neural networks"} (RNN) \cite{giles1994dynamic}. Their nodes can maintain an internal state, which is fed recurrently to the node itself together with the sequential input during the computation of every new state. One of the most famous implementations of an RNN and the choice for our experiments is the \textit{long short-term memory} (LSTM) architecture \cite{hochreiter1997long}. \\
The rest of this section provides an explanation of the functional principals behind the LSTM.

\subsection{General Explanation}
A standard RNN is processing input by looking at the current input $x_t$ at time step $t$ as well as the previous output $h_{t-1}$. However, a basic RNN becomes unable to learn long-term relationships of inputs as the temporal distance between them becomes larger.\\
To address this issue, an advanced architecture, called long-short term memory was introduced originally by \citet*{hochreiter1997long} and is now one of the most popular architectures in sequential data learning \cite{chen2017ensemble}. Our implementation of the LSTM is based on the description of the network structure by the original authors \cite{gers1999learning}.\\
\\
The LSTM architecture is a layer of identical nodes, each consisting of the connections and functions as depicted in figure \ref{tikz_lstm}.

\begin{figure}[h]
\centering
    \input{tikz/lstm}
    \caption{Schematic of a long-short term memory cell}
    \label{tikz_lstm}
\end{figure}

Each node maintains a \textit{cell state} $c_t$, which is updated at each time step by a set of gates, which receive themselves input by the previous hidden state $h_{t-1}$ and the input of the current time step $x_t$.
These gates consist of the forget gate $f_t$ [\ref{eq_lstm_f}] and the input gate $i_t$ [\ref{eq_lstm_i}]. This cell state can be seen as the memory of the cell as it contains all former input and is preserved between time steps.\\
The output of each node is the cell state $c_t$ adjusted by the output gate $o_t$ [\ref{eq_lstm_o}], which receives input from $h_{t-1}$ and $x_t$ similarly to the other gates.\\
All gates are \textit{sigmoid functions}, meaning that they can only return values between 0 and 1, thus any value multiplied by a gate function will keep only a percentage of its original value, i.e. the gate allows only parts of the information to pass through.\\
\\
Below are all functions written down in detail. "$\sigma$" describes the sigmoid function, "$tanh$" denotes the hyperbolic tangent function and $\odot$ denotes the element wise multiplication.


%Equations and most ideas explained from paper A C-LSTM Neural Network for Text Classification
%Check again with http://colah.github.io/posts/2015-08-Understanding-LSTMs/ as this is referenced by tensorflow
\begin{equation}\label{eq_lstm_i}
    i_t = \sigma \left(W_i * [h_{t-1}, x_t] + b_i \right)
\end{equation}
\begin{equation}\label{eq_lstm_f}
    f_t = \sigma \left(W_f * [h_{t-1}, x_t] + b_f \right)
\end{equation}
\begin{equation}\label{eq_lstm_q}
    q_t = tanh \left(W_q * [h_{t-1}, x_t] + b_q \right)
\end{equation}
\begin{equation}\label{eq_lstm_o}
    o_t = \sigma \left(W_o * [h_{t-1}, x_t] + b_o \right)
\end{equation}
\begin{equation}\label{eq_lstm_c}
    c_t = f_t \odot c_{t-1} + i_t \odot q_t
\end{equation}
\begin{equation}\label{eq_lstm_h}
    h_t = o_t \odot tanh \left( c_t \right)
\end{equation}

To better understand the mechanism behind this architecture, we want to shortly elaborate the purpose of the different gates: $f_t$ controls to what extent the information from the previous cell state is kept for the calculation of the new state. Gate $i_t$ controls how much new information is going to be added to the new state and $o_t$ controls what to output based on the newly computed cell state $c_t$.\\
The LSTM architecture is explicitly designed for time-series data in order to learn long-term dependencies between temporal distanced input features. Therefore we chose LSTM as the optimal basis for our network architecture to learn such dependencies during our experiments.

\subsection{Bidirectional LSTM Networks} \label{sec_bilstm}
A reoccurring problem with LSTM Networks is the dilution of old information that is followed by a long input sequence \cite{xing2010brief}. While the first input features the network receives are theoretically saved throughout the entire input sequence as part of the cell state, they are forgotten more and more as the network traverses the input sequence, updating the cell state with newer information.\\
Eventually there is a possibility of a stronger weighing of features close to the end of the input sequence for the computation of the final prediction than for features processed by the network in the beginning.\\
\\
To overcome these limitations we evaluate the feasibility of a \textit{Bidirectional Long Short-Term Memory Network} (BiLSTM) as originally proposed by \citet{schuster1997bidirectional}.

\begin{figure}[H]
\centering
    \input{tikz/bilstm}
    \caption{Extension of a LSTM cell to a BiLSTM cell}
    \label{tikz_bilstm}
\end{figure}

Essentially a BiLSTM layer is a combination of two conventional LSTM layers placed to process the input features in opposite directions. Figure \ref{tikz_bilstm} displays such a structure: $\vec{x}_t$ is the input feature at time $t$, which is fed simultaneously into two LSTMs which in turn produce the outputs $\vec{h}_t^\rightarrow$ and $\vec{h}_t^\leftarrow$. These two outputs are then concatenated into a single vector $\vec{y}_t$ which can be further processed in succeeding layers.\\
Through this modification a BiLSTM is able to preserve information from past and future inputs equally, weighing both directions identically and can be trained simultaneously in the positive and the negative time direction.


\section{Dropout Technique} \label{sec_dropout_l2}
Large neural networks with a extensive number of parameters are powerful machine learning systems. However, \textit{overfitting}, which describes the problem of a model learning the exact details of the data points given during the training instead of learning their general patterns, can result in poor performance of unseen data points which follow the same pattern, yet have different precise values.\\
Overfitting is a serious problem in such large networks. \textit{Dropout} is a technique for addressing precisely this problem \cite{srivastava2014dropout}: the main idea is to randomly drop units (along with their connections) from the neural network during training. This prevents nodes from co-adapting too much to the exact data points during training and thus reduces overfitting.


\section{Over the Horizon of Neural Networks}
27\% of all papers released through one of the largest databases of scientific papers, \textit{"arXiv"}, mention the method \textit{neural networks} in their abstract, more mentions than the next four most famous machine learning approaches \textit{Bayesian networks}, \textit{Markov methods}, \textit{evolutionary algorithms} and \textit{support vector machines} over the last two years combined \cite{mit_review19trends}.\\
\\
This section is meant to give a short justification for our choice of neural networks and to explain the general tendency towards this kind of machine learning model in the research community. In our opinion, this trend, which partly coincides with the reasons behind our choice as well, gained such traction due to mainly two reasons:\\
\\
First of all, the amount of data available for training increased enormously over the last years, allowing models to learn from more data and in more specific topics than ever before. This immensely increased number of training samples allows the recognition of increasingly complex patterns and thus requires larger and more sophisticated models with a greater number of parameters to approximate this complexity.\\
The main advantage of neural networks is their easy scaling to large bodies of data. Other approaches do not scale comparably well, such as for example in the case of the \textit{support vector machine}, for which computing the kernel matrix becomes computationally infeasible for large data sets \cite{yu2003classifying}. While there exist methods for optimizing these models by complex modification of the original idea, neural networks already contain the ability to easily scale with a growing number of data points through simple extensions \cite{chen2014big}.\\
\\
The second reason for the current success of neural networks is their easy and great accessibility. No deep knowledge of the underlying mathematical theorems is required to set up a basic model and such a simple configuration can often achieve already sufficient results.\\
This also implicates that extensions of the basic models are easier to use and add to existing models, giving more freedom in optimizing a specific model.\\
Extensions such as parallel training on distributed systems and batch training are readily accessible through different frameworks due to neural network's inherent structure \cite{ben2018demystifying}. Large amounts of data require huge computational resources, which became available over the last years. The research community benefits from the more powerful processors, the easier to interact with infrastructures such as server clusters and sophisticated machine learning frameworks such as TensorFlow or PyTorch \cite{abadi2016tensorflow}. This availability and easy accessibility allowed us to quickly set up our models and train them more effectively compared to other conventional machine learning approaches.


\section{Evaluation Metrics}
An important design choice when designing a model is the selection of evaluation metrics used to measure the prediction performance. There exists a great variety of such metrics across the research community.\\
Each metric gives a different perspective into the model and thus the right metrics can help in gaining insights into the right or wrong behavior, while picking the wrong metrics could shade certain characteristics of the model and possibly disguise the - in reality - wrong behavior.


\subsection{Confusion Matrix}
The \textit{confusion matrix} \cite{stehman1997selecting}, also known as \textit{error matrix}, describes the basic reference values for more sophisticated metrics such as \textit{accuracy, precision or recall}.\\
The entries of the table describe the number of \textit{true positive, false positive, true negative} and \textit{false negative} classified examples. True and false indicate that an example was classified properly, positive and negative indicate if an example is described by the respective label. A general example of a confusion matrix can be seen in figure \ref{confusion_matrix}.


\begin{figure}[H] \label{confusion_matrix}
    \input{tikz/confusion_matrix}
    \caption{General Example of a Confusion Matrix}
\end{figure}

The elements of the confusion matrix display the total number of examples to which the respective properties apply and are computed as described below in equations \ref{eq_tp} to \ref{eq_fn}.\\
$N$ represents the total number of examples, $y_i$ is the predicted label and $y_i'$ is the ground truth for a label of example $i$. $\delta()$ represents the \textit{Dirac delta function}.

\begin{equation} \label{eq_tp}
TP = \sum_{i=0}^{N} \delta \big( y_i - 1 \big) * \delta \big( y_i' - 1 \big)
\end{equation}
\begin{equation} \label{eq_fp}
FP = \sum_{i=0}^{N} \delta \big( y_i \big) * \delta \big( y_i' - 1 \big)
\end{equation}
\begin{equation} \label{eq_tn}
TN = \sum_{i=0}^{N} \delta \big( y_i - 1 \big) * \delta \big( y_i' \big)
\end{equation}
\begin{equation} \label{eq_fn}
FN = \sum_{i=0}^{N} \delta \big( y_i \big) * \delta \big( y_i' \big)
\end{equation}


\subsection{Accuracy as a Flawed Metric}
An evaluation metric used in many scientific fields as well as in everyday life is \textit{accuracy}. The main reason for its wide use is its simplicity.\\
It describes the number of right classifications in proportion to all classifications, as described in equation \ref{eq_accuracy}:

\begin{equation} \label{eq_accuracy}
Accuracy = \frac{TP + TN}{TP + FP + TN + FN}
\end{equation}

Accuracy is not a reliable metric to measure the real performance of a classifier, since it can yield misleading results if the data set is unbalanced. For example, if we assume a data set with 990 false examples and 10 true examples, the classifier could simply learn to predict false for all examples, resulting in an accuracy of 99\%.\\
While this metric is indicating a proficient classifier, the model actually learned quite poorly.


\subsection{Precision \& Recall}
A more informative way of evaluating a model are the metrics \textit{precision} and \textit{recall}. Generally, these two metrics are used together, as it has been shown, that there is always a trade off between precision and recall \cite{buckland1994relationship}: It is often possible to increase one of the two metrics at the costs of reducing the other one. Therefore, by looking only at one of the two metrics the model can appear to be performing well, while in reality the other metric performs exceedingly bad.\\
Thus evaluating a model by both metrics combined shows its overall quality of prediction thorougher.

\subsubsection{Precision}
Precision in the context of classification is the percentage of correctly predicted, (true) positive labels in relation to all positively predicted (i.e. correctly and incorrectly predicted) labels. Equation \ref{eq_precision} describes this metric in mathematical terms:

\begin{equation} \label{eq_precision}
Precision = \frac{TP}{TP + FP}
\end{equation}

A high precision value (close to 1.0) indicates that the model's positive predictions are correct with a high probability. It is important to note, that precision gives no information of how many true positive labels the model identifies, but merely that the ones it predicts are indeed positive labels and not incorrectly positive predictions.\\
To make an assessment over how many right labels have been identified, commonly an already mentioned metric, recall, is used.

\subsubsection{Recall}
Recall in the context of classification is the percentage of correctly predicted, (true) positive labels in relation to all positive (i.e. predicted and not predicted) labels, including incorrectly predicted (false) positive labels. \textit{"True Positive Rate"} (TPR) is a synonym for recall.\\
Equation \ref{eq_recall} describes this metric in mathematical terms:

\begin{equation} \label{eq_recall}
Recall = \frac{TP}{TP + FN}
\end{equation}

A high recall value (close to 1.0) indicates that the model is able to identify all true label with a high probability. Similar to the precision it can lead to wrong conclusions if it is used as the only metric: If a model predicts every possible label as true, the recall will have the value 1.0, yet the model has not learned the underlying data at all. The precision in this case would presumably be close to 0.0.

\subsection{F1 Score}
The $F_1$ score is a metric with the feature to unite precision and recall in a single value \cite{rijsbergen1979}. It is defined as depicted in equation \ref{eq_f1}
\begin{equation} \label{eq_f1}
F_1 = \left( \frac{precision^{-1} + recall^{-1}}{2} \right)^{-1} = 2 * \frac{precision * recall}{precision + recall}
= \frac{2 * TP}{2 * TP + FN + FP}
\end{equation}

A high value (close to 1.0) represents high values of precision and of recall, which in turn is a sign of a model with a good prediction quality. Therefore the $F_1$ score is frequently used as the central metric to evaluate a model.\\\\

It should be mentioned that the $F_1$ score is a special case of the $F_\beta$ score, which is defined as followed:

\begin{equation} \label{eq_f_beta}
F_\beta = (1+\beta^2) * \frac{precision * recall}{(1+\beta^2) * precision + recall}
\end{equation}

Changing the $\beta$ value changes the focus on recall against prediction. For example a value of $\beta$ greater than 1 will decrease the $F_\beta$ score further for a low value of precision than for recall. In that case the metric focuses more on a good precision than a good recall.

\subsection{Macro \& Micro Metrics}
Evaluating a binary classification problem, containing only one label which can be either true or false, the metrics discussed above are sufficient.\\
Shifting from this simple problem to a problem with multiple labels and in our case even multiple labels per example, makes calculating separated metrics for each label cumbersome and unclear. Therefore we utilize two different methods of combining the separate metrics for each label into single, combined values as defined in \cite{zhang2014review}:\\
\\
\textit{macro-average:}
\begin{equation} \label{eq_macro}
B_{macro} = \frac{1}{N}\sum_{j=1}^N B\left(TP_j, FP_j, TN_j, TP_j\right)
\end{equation}
\\
\textit{micro-average:}
\begin{equation} \label{eq_micro}
B_{micro} = B\left(\sum_{j=1}^N TP_j, \sum_{j=1}^N FP_j, \sum_{j=1}^N TN_j, \sum_{j=1}^N TP_j\right)
\end{equation}

where $B$ can be any metric function, $N$ is the number of different labels and $j$ is the index of the current label.\\
\\

A macro-average will compute the metric independently for each label and then take the average (hence treating all labels equally), whereas a micro-average will aggregate the contributions of all examples to the metric independent from the label(s) they belong to. In other words, the macro-average only considers the metrics already calculated for each label, while the micro-average considers all the predictions for all examples equally, irrespective to which label they belong.\\
In a multi-label classification problem, a micro-average can be preferable if there is the suspicion of a strong label distribution imbalance, i.e. the different numbers of examples per label disperse widely.\\
\\
It can easily be seen, that macro-averages and micro-averages assume “equal weights" for labels and examples respectively, thus considering all labels and examples as equally important.

\section{Different Loss Functions and Optimizers} \label{sec_opt_loss}
Following the argumentation of the \textit{universal approximation theorem} \cite{hornik1989multilayer}, even a simple single hidden layer neural network with a finite number of neurons can represent any continuous function on compact subsets of $\mathbb{R}^n$. As our classification problem falls into this category, in theory we should be able to train a model, which is capable of making predictions of perfect quality.\\
Yet this is hard to achieve, as we are not limited by the capabilities of our model, but by the ability to learn and to reach this perfect configuration of parameters in our model.\\
\\
Therefore, we want to evaluate different optimization and loss functions as these are the keys to reaching a proficient model. This section is only meant to be an introduction and to give an overview. We refer to the respective literature for further insights.

\subsection{Loss Functions}
Loss functions quantify the severity of wrong and right prediction. The choice of the loss function allows us to set a focus on the model's prediction, punishing certain predictions stronger than others and thus giving the model an incline towards certain outcomes. One could for example emphasize on true positive predictions by increasing the loss for a false positive prediction compared to a false negative prediction.\\
We always assume the prediction of the network to be in the range of $[0,1]$ and not rounded to either 0 or 1 yet.

\subsubsection{Mean Squared Error}
This is one of the earliest used and simplest loss functions \cite{richard1991neural}. It finds applications in a variety of different scientific fields.\\
Equation \ref{eq_mse_loss} describes this loss over all $N$ labels of a given data point. $y$ corresponds to the predicted probability through the network and $y'$ to the ground truth of all labels.

\begin{equation} \label{eq_mse_loss}
l_{MSE}(y,y') = \frac{1}{N}\sum_{i=0}^{N}\left(y_i'-y_i\right)^2
\end{equation}

This loss function is used mainly for regression problems as it simply punishes the distance between prediction and ground truth. We thus expect a worse prediction quality using this compared to other loss functions tailored further for classification problems.


\subsubsection{Sigmoid Cross Entropy}
One of the most common loss functions in machine learning is \textit{Sigmoid Cross Entropy} \cite{baum1988supervised}. Its description with the same variables as in the last section can be found in equation \ref{eq_sce_loss}.

\begin{equation} \label{eq_sce_loss}
l_{SCE}(y,y') = - \sum_{i}^N \left( \underbrace{y_i' \log(y_i)}_\text{Loss for positive labels} + \underbrace{(1-y_i') \log (1-y_i)}_\text{Loss for negative labels}\right)
\end{equation}

The first term punishes the network for false positive predictions, while the second term accounts for false negative predictions. Through the $log$-function this loss punishes false predictions stronger, the further away from the right prediction the network is.

\subsubsection{Ranking Loss}
This loss function is inspired by the label-correlation aware loss function by \citet{yeh2017learning}.

\begin{equation} \label{eq_ranking_loss}
l_{ranking}(y,y') = \frac{1}{|y^0||y^1|} \sum_{(p_-,q_+) \in y^0 \times y^1}exp\left(\alpha *(p_- - q_+)\right)
\end{equation}

\begin{equation*}
y^0 \cap y^1 = \emptyset, \, y^0 \cup y^1 = y
\end{equation*}

$y_0$ denotes the set of all positive labels of $y$ and $y_1$ of all negative labels. $\alpha$ is a external parameter that controls a margin to further enforce true predictions.\\
As the authors of the original loss function describe it: minimizing the above loss function is equivalent to maximizing the prediction outputs of all positive-negative label attribute pairs, which implicitly enforces the preservation of label co-occurrence information \cite{yeh2017learning}. Such label dependency cannot be preserved with the former mentioned loss functions and we expect this to improve our results.


\subsection{Optimizers}
Alongside different loss functions we also evaluated different optimization algorithms which build on top of the concept of backpropagation \cite{hecht1992theory}. In order to not lengthen this section too far and as it is out of the scope of this work, we omitted the formula descriptions of the different optimization algorithms and leave it to the interested reader to follow the mentioned sources for a more detailed explanation.

\subsubsection{Stochastic Gradient Descent with Momentum}
Gradient descent is one of the earliest learning algorithms and the base for most more sophisticated algorithms \cite{mei2018mean}. It updates each parameter proportionally to the influence this parameter had on the final prediction of the network.\\
While the original gradient descent algorithm used all training data points before updating the network's parameters, \textit{stochastic} gradient descent uses only a subset of the entire training data set at each training step, thus improving the performance of the training phase.\\
Another improvement is the usage of \textit{momentum} during the update of all all parameters: the optimizer keeps an average over the previous backpropagated gradients, which influences the current parameter update. This reduces the effect of misleading outliers in the training set and can further improve the training efficiency.


\subsubsection{RMSProp}
RMSprop's largest improvement over stochastic gradient descent is its adaptive learning rate \cite{tieleman2012lecture}.\\
Rather than having a global learning rate as is the case for the stochastic gradient descent, this optimizer maintains a distinct learning rate for each parameter, which is iteratively updated with a running average computed from previous gradients. Given input values processed through each layer of a network might have different scales, the gradients are adapted to these fluctuations. This consequently mitigates the vanishing or exploding gradient problems and thus stabilizes the learning.


\subsubsection{Adam Optimizer}
The name Adam is derived from \textit{adaptive moment estimation} and was proposed only recently, in 2015 \cite{brownlee2017gentle}.\\
This optimizer computes individual adaptive learning rates for different parameters of the network from estimates of first and second moments of the gradients. This is an improvement over RMSProp as the latter only utilizes the first moment (i.e. mean) of the current gradients.\\
As stated by the original authors, this automatic update of the learning rate greatly improves the efficiency of the training process compared to former optimization algorithms.

