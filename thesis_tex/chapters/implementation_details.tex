\chapter{Implementation Details}
In the following sections of this chapter we present an in-depth discussion of the tools used as well as the training and evaluation processes applied during our experiments.


\section{Introduction to TensorFlow}
One of the greatest challenges in the field of neural networks is the amount of time and computational resources required in order to successfully train a model. Therefore, an efficient implementation is essential. In recent years, various frameworks have been developed, each with different goals such as increasing performance, easing accessibility, enabling modularity or distributed training.\\
\\
\textit{TensorFlow}, a widely used and successful framework, was developed by Google and originally released in 2015 \cite{Metz15tensorflowrelease}.\\
The software architecture behind TensorFlow allows for high performance, numerical computations on CPUs and GPUs. In conjunction with the large number of already implemented APIs from the developers at Google and the contributions by the open-source community this allows for an efficient and comparatively fast evaluation of networks with a large number of different layers, loss function and optimizers.\\
Further important advantages of the TensorFlow framework are its excellent logging functionalities and its debugging software called \textit{Tensorboard}. These tools give us valuable insights into the training process and evaluation metrics without obstructing the model's training capabilities.


\section{Training Environment}
As our data sets consisted of up to six million data points, a local machine running solely the training of a single model over multiple days was not feasible any more. Thus we decided to run our training on a cloud computing platform, in our case the \textit{Amazon Web Services} (AWS).\\
These platforms provide instances, which function as virtual computers, and have most attributes of a real computer including hardware (CPU(s) \& GPU(s) for processing, local/RAM memory, hard-disk/SSD storage).
This allowed us to easily scale our training processes by using more or fewer instances and by choosing different types of instances fitting the requisites of the current experiment.

\section{Data Processing Pipeline}
While the model architecture and the choice of the right training method play an important role in the later prediction quality, the data used for training and the training process itself are likewise crucial for the success of the model.\\
For this reason, we want to discuss in this section our general approach to preprocessing our data into a usable format and how it was further processed in our neural network.

\subsection{Data Cleaning} \label{sec_data_cleaning}
Our data sets consisted solely of text data. As there is a large number of different encodings for text data, we converted all data sets to the \textit{UTF-8} encoding in order to uniformly process them further.\\
Our definition of a feature is any sequence of characters surrounded by whitespace. As special characters such as "\textit{!?.,;:\/-"''}" contain viable information, we decided to treat these as words, too. Thus we surrounded these by whitespaces before further processing. This step additionally averts problems arising from inputs such as "\textit{dog, }" treated as a single word instead of "\textit{dog}" and "\textit{,}".\\
We also treated new lines as special characters, adding a special token to the input sequence representing new lines.\\
Due to the absence of labels in several data points, we introduced a new, specific label, which represents all data points without any labels. Our motivation to not discard these data points, even though they do not contain beneficial information about labels, they can still be used as negative prediction examples during training and validation, thus improving the overall performance.

\subsection{Data Preprocessing}
After cleaning the data sets, the next step was to do divide them into training and validation sets. We split all data sets into 80\% training and 20\% validation sets.\\
\\
The split was not entirely random, as we used one important restriction:\\
In the validation data, all labels of all samples are required to appear at least 20 times in the training data set. Otherwise these samples remain only in the training set. The precise threshold number was variable, after some short evaluations we determined this number to be fitting best for our purpose. The reason behind this restriction is that if a label was not seen often enough by the model during the training phase, it has no means of learning a proper prediction for this label and thus is neither able to accurately predict it during the validation phase.\\


\subsection{Training and Validation Phase}
We repeatedly run an evaluation of the model after a defined time period in order to track the progress of our training.\\
The TensorFlow framework allows to run an evaluation in parallel to the training, thus improving the overall training time by not halting the training during an evaluation.\\
The training and evaluation of all models was done on GPUs, as these are able to execute the mathematical operations more efficient than CPUs \cite{oh2004gpu}.

\section{Our Model Structure}
Finally we want to present the high-level structure of our model used for all later experiments in figure \ref{tikz_full_model}.

\begin{figure}[H]
    \centering
    \input{tikz/full_model}
    \caption{Visualization of the model structure used in our experiments}
    \label{tikz_full_model}
\end{figure}


The input sequence is presented feature by feature to the network. Features in our experiments can be either words or subunits, which are described in detail in chapter \ref{sec_subwords}.\\
In a first step these features are transformed either through a trained or through a random embedding into numerical vectors of a dimension of 200. We oriented ourselves by the current state of the art for the embedding dimension. These vectors are then presented to the BiLSTM layer as described in section \ref{sec_bilstm}.\\
Only after processing the last feature by the BiLSTM layer, its output is forwarded to a final, fully connected layer, which then computes an output prediction.


