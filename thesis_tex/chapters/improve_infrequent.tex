\chapter{Improvement of Prediction of Infrequent Labels}\label{sec_improve_macro}

\section{Motivation}
During our experiments on the different subword segmentation approaches we noticed a strong tendency of the network to learn frequently in the data set appearing labels, yet failing to properly predict less frequent labels. This assumption can be confirmed through the graphs in figure \ref{fig_MFR_conv_segments} of the previous chapter, which compare the prediction quality of the model as an average over similarly often appearing labels. \\
The most frequent 1\% of all labels, which appear 5,601,386 times in the training set have an average F1 score of 0.955, compared to an average F1 score of 0.614 for the least frequent 25\% which appear 84,518 times in the training set for the Mercateo-FR data set.\\
We expect the main reason behind this unbalanced learning behavior to be the unbalanced distribution of samples for each label in the training set; the more often the model sees samples of a certain label, the better it can adapt to predict these labels. Consequently it adapts better to the frequent labels than the less frequent labels, explaining the widely varying F1 scores for the different labels.


\section{Basic Idea}
The main reason for the superior prediction quality of the most frequent labels, in our opinion, is their over representation in comparison to the less frequent appearing labels. The 5 most often appearing labels appear as often as the least appearing 9516 labels.\\
In this chapter we discuss two different approaches to improve the model's prediction capabilities. In exchange for an improved prediction quality of the infrequent label we expect to loose some prediction quality of the most frequent labels. As it is often preferable to have a consistent prediction rate over all labels, we think of this drawback as acceptable.

\subsection{Over and Under Sampling} \label{sec_adjust_dataset}
As in the last section discussed, the major reason for the unequal prediction rates is the unequal distribution of samples per label in the training data. Therefore, our goal is to adjust the distribution to a more equal level. This problem has no straight forward solution in a multi label problem even when allowing the repetition of data points:\\
In a single label problem, the infrequent labels could simply be repeated sufficiently to achieve an adjusted data set containing the exact same number of each label if desired.\\
In a multi label problem most labels appear in combination with other labels on each data point. Thus by repeating the infrequent labels, oftentimes the frequent labels are repeated simultaneously and as a consequence the ratio of infrequent labels does not improve compared to the ratio of the frequent labels. This problem increases, the more unevenly the distribution of labels in the data set is, as this implies, that more data samples overall contain the frequent labels and as a result the repetition of these becomes more likely.


\subsubsection{General Algorithm}
In order to solve this problem optimally, we designed an algorithm that iterates multiple times over the original data set and adds data samples from this original data set to the new, adjusted data set depending on a cost function. It is going to repeat over the data set until either all labels appear at least a certain number of times in the new data set, or no more samples can be found in the original data set that could extend the new data set in a useful way, i.e. that would improve the ratio between frequent and infrequent labels in a beneficial way. The pseudo code to our approach can be seen in procedure \ref{algo_opt_infreq_labels}.


\begin{algorithm}
    \caption{Generate Adjusted Data Set}
    \label{algo_opt_infreq_labels}
    \begin{algorithmic}
        \REQUIRE $\textit{originalData}\;D_o, \textit{minScore}\;s_m, \textit{targetNumPointsPerLabel}\;n_t$
        \ENSURE $\textit{newData}\;D_n$
        \STATE $D_n \leftarrow \text{empty}$
        \STATE
        \WHILE{$\text{not all labels sufficiently in } D_n$}
        \STATE $\textit{currentDataPoint}\;p \leftarrow \text{ next data point of } D_o$
        \STATE $\textit{score}\;s_p \leftarrow 0$
        \FOR{$\textit{label}\;l \text{ in } p$}
        \STATE $\textit{NumPointsPerLabel}\;n_l \leftarrow \text{occurrences of } l \text{ in } D_n$
        \STATE $\delta \leftarrow  n_t - n_l$
        \STATE $s_p \leftarrow s_p + \text{scoreFunction(} \delta \text{)}$
        \ENDFOR
        \IF{$s_p \geq s_m$}
        \STATE $p \text{ add to } D_n$
        \ENDIF
        \ENDWHILE
    \end{algorithmic}
\end{algorithm}


% \begin{algorithm}
%     \caption{Generate Adjusted Data Set}
%     \label{algo_opt_infreq_labels}
%     \begin{algorithmic}
%         \REQUIRE $originalData, minScore, targetNumPointsPerLabel$
%         \ENSURE $newData$
%         \STATE $newData \leftarrow \text{empty}$
%         \STATE
%         \WHILE{$\text{not all labels sufficiently in } newData$}
%         \STATE $currentDataPoint \leftarrow \text{ next data point of } originalData$
%         \STATE
%         \STATE $totalScore \leftarrow 0$
%         \FOR{$label \text{ in } currentDataPoint$}
%         \STATE $occurencesLabel \leftarrow \text{occurences of } label \text{ in } newData$
%         \STATE $diff \leftarrow  targetNumPointsPerLabel - numPointsPerLabel$
%         \STATE $score \leftarrow \text{scoreFunction(} diff \text{)}$
%         \STATE $totalScore \leftarrow totalScore + score$
%         \ENDFOR
%         \STATE
%         \IF{$totalScore \geq minScore$}
%         \STATE $currentDataPoint \text{ add to } newData$
%         \ENDIF
%         \ENDWHILE
%     \end{algorithmic}
% \end{algorithm}

% \caption{Optimization algorithm for the infrequent label problem}

To decide if a data point from the original data set should be added to the new data set, the algorithm determines a score for each sample. If the score is over a certain margin, we chose a value of 0 in our case, the data point is added to the new data set. The score function is a sophisticated mathematical function, which can take into account a set of factors such as the frequency of each label or the current number of appearances in the new data set. Below we give an overview over the different score functions we have evaluated.

\subsubsection{Greedy Score Function}
\begin{equation} \label{eq_greedy_cost}
    f_{greedy}(x)=
    \begin{cases}
        \epsilon,              & \text{if } x > 0\\
        -1,              & \text{otherwise}
    \end{cases}
    \qquad x \in \mathbb{R}, \epsilon \in \mathbb{R}_+
\end{equation}

The most straightforward score function in our evaluation simply returns a positive value equal to the given margin, if at least one label of the data point is not sufficiently represented in the new data set yet. Function \ref{eq_greedy_cost} formulates this score function with $x$ as the difference between the target number and the current number of appearances of each label in the new data set and $\epsilon$ as the minimal score.\\
This score function guarantees the algorithm to converge, as there is no possibility that a data point, which could improve the total number of a label that requires more samples in the final set, would be dismissed.

\subsubsection{Exponential Score Function}
\begin{equation} \label{eq_exp_cost}
    f_{exp}(x, c)=
    \begin{cases}
        x^{2*\frac{c+p}{p}},        & \text{if } x > 0\\
        -1 * x^2,                              & \text{otherwise}
    \end{cases}
    \qquad x \in \mathbb{R}, c \in \mathbb{N}_+, p \in \mathbb{R}_+
\end{equation}

Often it is favorable to overproportionally prefer data points with labels, which appear exceedingly seldom. Therefore, we expect an \textit{exponential score function} to improve the new adjusted data set even further by giving these rare labels a higher impact on the score.\\
Function \ref{eq_exp_cost} describes this functionality. $x$ is defined as before the difference between the target number and the current number of appearances of each label in the new data set, $c$ is the count of iterations through the entire original data set so far and $p$ is a predefined parameter to control the increase of the score throughout later iterations.\\
The multiplier $\frac{c+p}{p}$ in the exponent increases with every iteration through the entire original data set. This guarantees the algorithm to converge as the missing labels get a higher score attributed for every iteration. As a result, a data point that might has not seemed favorable in the last iteration appears better in every consequent iteration, due to the increasing weight of the positive scores.


\subsubsection{Frequency Based Score Function}
\begin{equation} \label{eq_freq_cost}
    f_{freq}(x, c)=
    \begin{cases}
        \frac{1}{\sqrt{f_i}} * x^{2*\frac{c+p}{p}},        & \text{if } x > 0\\
        -1 * \frac{1}{\sqrt{f_i}} * x^2,                  & \text{otherwise}
    \end{cases}
    \qquad x \in \mathbb{R}, c \in \mathbb{N}_+, p \in \mathbb{R}_+, f_i \in [0;1]
\end{equation}

The focus of the \textit{frequency based score function} as described in equation \ref{eq_freq_cost} is to include more knowledge about the original data set in the score function. It contains the same elements as the \textit{exponential score function}, but extends this by an inverse frequency multiplier.\\
$x$, $c$ and $p$ are defined as in the last two score functions. $f_i$ describes the frequency of a label $l_i$ in the original data set.\\
The more often a label appears in the original data set, the lower will be its score and thus the less favorable it will appear to be used in the new, adjusted data set.\\
As it still contains the elements of the \textit{exponential score function}, this score function guarantees the algorithm to converge for the same reason as for the former score function.


\subsection{Inverse Frequency Loss} \label{sec_if_loss}
After discussing the approach of adjusting the data set in order to optimize training for an uneven distribution of labels, we now want to look further into different loss functions to counterbalance the uneven distribution of samples per label.\\
We want to achieve this by introducing an additional factor for each label to compensate for its ratio of appearances in the data set.\\

\begin{equation} \label{eq_if_loss}
    l_{ifl}(y,y') = - \sum_{i}^N \underbrace{\frac{1}{f_i}}_\text{frequency factor} * \underbrace{\left({y_i' \log(y_i) + (1-y_i') \log (1-y_i)}\right)}_\text{Sigmoid Cross Entropy}
\end{equation}

Equation \ref{eq_if_loss} describes our new loss function over all $N$ labels of a given data point. $y$ corresponds to the predicted probability through the network, $y'$ to the ground truth of all labels and $f_i$ is the frequency of a label $l_i$ similar to its usage in the last section.\\
The main idea of this loss function is to increase the loss generated by infrequent labels, as the multiplication factor $\frac{1}{f_i}$ is larger, the more seldom  a label appears in the data set. This increases the learning impact of rare labels against frequent labels and as a consequence the macro scores of the neural network.


\subsection{Positive-Negative Weighted Loss Function}
Another loss function we evaluated in order to improve our model is the positive-negative weighted loss function \cite{covington2016deep}. Its main idea is to change the influence towards the loss of the positive against the negative labels, as depicted in \ref{eq_weighted_loss}. All variables are defined as in the last section \ref{sec_if_loss}.

\begin{equation} \label{eq_weighted_loss}
    l_{weighted}(y,y') = - \sum_{i}^N \overbrace{p_{weight}}^\text{weight factor} * \underbrace{{y_i' \log(y_i) + (1-y_i') \log (1-y_i)}}_\text{Sigmoid Cross Entropy}
    % l_{weighted}(y,y') = - \sum_{i}^N \underbrace{{y_i' \log(y_i) * \overbrace{p_{weight}}^{\substack{\text{weight} \\ \text{factor}}} + (1-y_i') \log (1-y_i)}}_\text{Sigmoid Cross Entropy}
\end{equation}

$p_{weight}$ influences the impact of all positive labels. A value greater 1.0 enforces the network to incline towards predicting positive samples, and thus improving the recall, while a value of less than 1.0 enforces the network to tend towards predicting negative samples, and thus improving the precision.\\
Overall this loss function allows for a trade off between recall and precision by up- or down-weighting the cost of a positive error relative to a negative error.

\section{Results}
Finally we want to present the results from the approaches meant to improve the prediction quality of the infrequent labels as discussed in the last sections. For our evaluation we used the Mercateo-FR data set with the BPE segmentation. Further we used the same configuration for the neural network as we used for the comparison of the different segmentation approaches (see section \ref{sec_comp_m_fr_converge} for details).

\subsection{New Data Set Statistics}
Table \ref{table_statistics_adjusted_dataset} displays the key figures of the newly created, adjusted data sets created by the algorithm and score functions introduced in section \ref{sec_adjust_dataset}. The figures in the table do not exactly match the figures presented in table \ref{table_base_facts_data_sets} from chapter \ref{sec_anaylsis_datasets}. This is because we improved the number of samples per label only for labels, which appear in a sufficient number of examples. We ignored the remaining labels as these do not appear in the test set and thus we cannot measure any changes. To allow a fair comparison, we calculated the statistics only for labels occurring enough times, slightly altering the key figures.\\
\\
Regarding the parameters, we chose $p$, the scaling factor for each iteration's increase of the score function, to be 20 and a target number of data points per label to be 1000. Additionally, we introduced an early stopping to our algorithm: once the new data set increased by less than 0.5\% after a full iteration of the original data set, the algorithm finishes.

\begin{table}[H]
    \begin{center}
        \def\arraystretch{1.25}
        \begin{tabular}{| C{6.1cm} || C{1.7cm} | C{1.7cm} | C{1.9cm} | C{1.7cm}|}
            \hline
            & Original & Greedy & Exponential & Frequency \\
            \hline
            Number of data points                           & 5,128,844 & 864,681   & 780,161   & 7,869,316 \\
            \hline
            Avg. number of points per label             & 728       & 107       & 63        & 244 \\
            \hline
            Std. dev. of number of points per label     & 6464      & 233       & 58        & 480 \\
            \hline
            Avg. ratio of points per label            & 0.00090   & 0.00088   & 0.00087   & 0.00088 \\
            \hline
            Std. dev. of ratio of points per label & 0.000745  & 0.00166   & 0.000068  & 0.000127 \\
            \hline
        \end{tabular}
    \end{center}
    \caption{Key figures of the newly created data sets from the original Mercateo-FR data set using different score functions}
    \label{table_statistics_adjusted_dataset}
\end{table}

The standard deviation of the number of data points per label in the original data set is extremely high with a value of 6464. This means there is an enormous disparity between the number of samples for frequent compared to infrequent labels.\\
We can see a significant improvement in regards of this issue by all suggested algorithms. The exponential score function reduced the standard deviation the most, by circa 99.1\%.
The frequency based score function seemed impractical to use due to the long run time without significant advantages over the other score functions.\\
An interesting difference between the greedy score function and the exponential cost function is the different average number of points per label compared to the similar number of total points in the new data sets. We conclude from the better standard deviation of the exponential cost function, that it enabled the algorithm to select more useful data points with more suitable sets of labels compared to the greedy cost function, which possibly picked a number of data points, which contained a lower number of infrequent, still required labels.\\
\\
For the reasons discussed in this section, we chose to use the data set created by utilizing the exponential score function for further evaluations.


\subsection{Improved Model Prediction Performance}
We evaluated two different approaches: Training an already converged neural network further with the new training configurations, and training a network from start. We kept the original validation set for all evaluations, with one minor modification for the case of the training from start on the adjusted data set: as the adjusted data set contains less training examples in total, it consequently contained less unique features. Therefore we replaced all features in the validation set by the unknown token as discussed in section \ref{sec_unknown_token}, if they were not contained in the new training set.\\
Due to the reduced number of samples of the adjusted data set we noticed overfitting after multiple training epochs. Thus, when reporting exact scores, we picked numbers before a noticeable decline, but made sure to always pick all scores at the same training step.\\
\\
The next two pages present all graphs followed by a short discussion of each approach. The approach using the positive-negative weighted loss function is separately discussed in section \ref{sec_example_weighted_loss_results}.


\newpage
\subsubsection{Graphs of Experiments on the Converged Model}

\begin{figure}[H]
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_on_converged_all_macro_f1_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_on_converged_all_micro_f1_plot.pgf}
    \caption{Micro and macro F1 score for further training of a converged model using BPE segmentation on Mercateo-FR. Information before 1,200,000 steps is omitted, as the pretrained model is similar for identical}
    \label{fig_improve_macro_converged_f1}
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_on_converged_all_segments_0.00-0.01_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_on_converged_all_segments_0.75-1.0_plot.pgf}
    \caption{Macro F1 score for different sets of labels for further training of a converged model using BPE segmentation on Mercateo-FR. Information before 1,200,000 steps is omitted, as the pretrained model is identical}
    \label{fig_improve_macro_converged_segments}
\end{figure}


\newpage
\subsubsection{Graphs of Experiments on Training from Start}

\begin{figure}[H]
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_from_start_all_macro_f1_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_from_start_all_micro_f1_plot.pgf}
    \caption{Micro and macro F1 score using BPE segmentation on Mercateo-FR}
    \label{fig_improve_macro_from_start_f1}
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_from_start_all_segments_0.00-0.01_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_from_start_all_segments_0.75-1.0_plot.pgf}
    \caption{Macro F1 score for different sets of labels using BPE segmentation on Mercateo-FR}
    \label{fig_improve_macro_from_start_segments}
\end{figure}


\newpage
\subsubsection{Adjusted Data Set}
First we want to discuss the results using only the adjusted data set after training further from a converged model and training from start:

\paragraph{Further training from the converged model} resulted in worse scores than the original training approach by circa 0.02 on the macro F1 score and circa 0.06 on the micro F1 score. Additionally the issue of overfitting is easily recognizable.\\
On the other side, there are improvements in the least frequent 25\% of labels by a value of up to 0.033 before overfitting begins.

\paragraph{Further training from the converged model using dropout} should counterfeit the overfitting discussed in the last paragraph. Yet the model continues to overfit. We expect this is due to the largely reduced data set.

\paragraph{Training from start} improves the macro F1 score in the beginning by up to 0.10 at early states of the training compared to the training with the original data set. Yet this better score gets lost after circa 8 epochs, at which point the new approach converges while the training with the original data set still improves the model's prediction quality. \\
The micro F1 score stays worse compared to the original approach at all times.\\
We expect the reason behind the early advantage to be the fast repetition of all infrequent labels. Yet after a sufficient number of repetitions of the original data set the model apparently manages to learn the infrequent labels in both cases, but as the original data set has more data points in total to learn from, its final, converged scores are better.\\
\\
The results on the segments are more promising. While the most frequent 1\% of labels have a lower F1 score of circa 0.25, the least frequent 25\% of all labels have an improved F1 score by 0.14 compared to the original approach.\\
We have to note, that the model learning from the adjusted data set has already converged. On the other side, the model learning from the original data set converges only after 1,900,000 steps, circa 1,580,000 steps later, and then reaching a higher F1 score for the least frequent 25\% of 0.62 compared to 0.55.
This shows that our adjusted model follows the purpose it was designed for, by learning the least frequently occurring labels more efficient, yet it can not outperform learning from the original data set.


\subsubsection{Label Weighted Loss Function}
Subsequently we want to discuss the results using the label weighted loss function:

\paragraph{Further Training from the converged model} decreased all scores, the macro F1 score by about 0.012, the micro F1 score by about 0.003.

\paragraph{Training from the start} also decreased the score. As the graphs show, the model was not able to properly learn from the training data with this loss function. We expect the loss function to be too extreme, confusing the neural network with too irregular loss values for the different labels.\\
This is especially an issue for samples with both, very frequent and very infrequent labels. Such samples can, depending on the prediction, create extremely high and extremely low loss values for the different labels, rendering the model incapable of properly predicting these samples.


\subsubsection{Combination of Adjusted Data Set and Label Weighted Loss Function}

\paragraph{Further training from the converged model} showed significant improvements over the original approach: the macro F1 score improved by circa 0.012 against a loss in the micro F1 score of about 0.002.\\
The different segments of labels show even more compelling results: while the macro F1 score of the first 1\% declined by up to by about 0.04, yet the more infrequent the labels appear, the better their score becomes. The macro F1 score of the least frequent 25\% improved by up to a value of 0.07.

\paragraph{Training from start} showed similar results as the training with the label weighted loss alone. The score improved slightly, by about 0.052 in the case of the macro F1 score and 0.025 in the case of the micro F1 score. Yet these scores are still significantly worse than the original approach and the approach with the adjusted data set alone.


\subsection{Evaluations of the Positive-Negative Weighted Loss Function}\label{sec_example_weighted_loss_results}
Finally, we want to discuss the results of applying the positive-negative weighted loss function.\\
While this loss function significantly improved the micro and macro F1 scores during our experiments, we noticed that this is not due to an improvement of the prediction of infrequent labels - which is the main purpose of this chapter. While it seemed to improve the prediction quality measured by the F1 scores, it in fact only improved the recall while decreasing the precision as can be seen in table \ref{table_example_weighted_loss}.\\
\\
We would like to explain this issue in the following paragraph shortly:\\
The lower a value of precision or recall is, the stronger is its influence on the F1 score. In other words, if we have one metric with a value lower than the other one, an increase in this metric against the same decrease in the already higher one will improve the F1 score due to the higher impact of the lower score.\\

% \begin{equation}
%     F1(p-d,r+d) = 2 * \frac{(p-d) * (r+d)}{p - d + r + d} = 2 * \frac{pr + pd - rd - d^2}{p + r}
% \end{equation}
% \begin{equation}
%     \frac{\delta F1(p-d,r+d)}{\delta d} = 2 * \frac{p - r - 2d}{p + r}
% \end{equation}

In our case was the model's original macro precision already at 0.918 and its original macro recall only at 0.604. Therefore an increase in recall against a decrease in precision will improve the final macro F1 score.\\
As a consequence, we can not evaluate these results as an improvement of the prediction of infrequent labels.\\
\\
Nevertheless, since the results are still interesting, and - dependent on the use case - can further on show an improvement in the overall prediction quality of the model, we want to shortly discuss our results in this section.

We only discuss the evaluation of the training of the neural network from start, as the training behavior is very similar in the case of further training from a converged model. The graphs \ref{fig_mfr_improve_macro_f1_example_loss} to \ref{fig_mfr_improve_macro_segments_example_loss} show the evaluations during training, table \ref{table_example_weighted_loss} the final scores.

\paragraph{Training from the start}


\begin{table}[H]
    \begin{center}
        \def\arraystretch{1.25}
        \begin{tabular}{| C{4.0cm} || C{2.0cm} | C{2.0cm}| C{2.0cm} |}
            \hline
            Loss parameter & 1 (Original) & 2 & 10 \\ [0.5ex]
            \hline
            Macro F1 & 0.630 & 0.679 & 0.672 \\
            \hline
            Micro F1 & 0.884 & 0.892 & 0.835 \\
            \hline
            Macro precision & 0.918 & 0.876 & 0.730 \\
            \hline
            Macro recall & 0.604 & 0.777 & 0.678 \\
            \hline
            F1 of most frequent 1\% & 0.940 & 0.941 & 0.904 \\
            \hline
            F1 of least frequent 25\% & 0.387 & 0.472 & 0.512 \\
            \hline
        \end{tabular}
    \end{center}
    \caption{Different key evaluation metrics after 320,600 training steps for different loss parameters using BPE on Mercateo-FR}
    \label{table_example_weighted_loss}
\end{table}

We clearly see an improvement of the macro and micro F1 scores by 0.078 and 0.009 when using a weight of 2. As was already discussed, this does not imply an improvement in the overall prediction quality of the model, but only a trade off between prediction and recall as can be seen in the table: the macro recall is improved by 0.286 while the macro precision is reduced by 0.458. Since the micro metrics follow a similar behavior, we omitted their details from this comparison.\\
\\
In conclusion, we can see that the example based loss function can improve certain metrics in exchange for other prediction aspects. In the end it is the choice of the model designer to apply the best fitting loss function for the according use case. Yet, we do not see this loss function as applicable for our objective to improve the prediction quality of infrequently occurring labels.


\newpage
\begin{figure}[H]
    \vspace{-0.8cm}
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_macro_f1_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_micro_f1_plot.pgf}
    \caption{Micro and macro F1 scores for different loss parameters using BPE on Mercateo-FR}
    \label{fig_mfr_improve_macro_f1_example_loss}
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_macro_precision_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_macro_recall_plot.pgf}
    \caption{Macro precision and macro recall for different loss parameters using BPE on Mercateo-FR}
    \label{fig_mfr_improve_macro_prec_rec_example_loss}
    \showTwoGraphs{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_segments_0.00-0.01_plot.pgf}{img/graphs/M_FR_improve_macro_bpe_from_start_example_weighted_loss_segments_0.75-1.0_plot.pgf}
    \caption{Macro F1 scores of segments for different loss parameters using BPE on Mercateo-FR}
    \label{fig_mfr_improve_macro_segments_example_loss}
    \thispagestyle{strangepage}
\end{figure}
