#! /bin/bash
# set -x

# Executes the python scripts on all valid files in the 'files' directory
# use as ./run_on_all ACTION

ACTION=$1
PYTHON_EXEC='../venv/bin/python3.6'
FILE_DIRECTORY='files'
DESTINATION_FOLDER='all_plots'


if [ "$ACTION" == "generate-configs" ]; then
    find $FILE_DIRECTORY -name "csv_files" -printf '%p\n' | while read file; do
        $PYTHON_EXEC gen_default_config.py $file
    done
elif [ "$ACTION" == "generate-plots" ]; then
    find $FILE_DIRECTORY -name "config.json" -printf '%p\n' | while read file; do
        $PYTHON_EXEC plot_csv.py $file
    done

    echo "Create new symbolic links in $FILE_DIRECTORY for all plots"
    rm -r $DESTINATION_FOLDER
    mkdir -p $DESTINATION_FOLDER

    find $FILE_DIRECTORY -name "*.pgf" | while read file; do
        basename=`basename $file`
        ln -s "../$file" "$DESTINATION_FOLDER/$basename"
    done
elif [ "$ACTION" == "collect-plots" ]; then
    echo "Create new symbolic links in $FILE_DIRECTORY for all plots"
    rm -r $DESTINATION_FOLDER
    mkdir -p $DESTINATION_FOLDER

    find $FILE_DIRECTORY -name "*.pgf" | while read file; do
        basename=`basename $file`
        ln -s "../$file" "$DESTINATION_FOLDER/$basename"
    done
elif [ "$ACTION" == "remove-configs" ]; then
    find $FILE_DIRECTORY -name "config.json" -printf '%p\n' | while read file; do
        rm $file
    done
elif [ "$ACTION" == "remove-plots" ]; then
    find $FILE_DIRECTORY -regex "*.(svg|png|pgf)" -printf '%p\n' | while read file; do
        rm $file
    done
    # Remove old plot collection
    rm -r $DESTINATION_FOLDER
else
    echo "Unknown action \""$ACTION\"
    echo "Actions are \"generate-configs\", \"generate-plots\", \"remove-configs\", \"collect-plots\" and \"remove-plots\""
fi
