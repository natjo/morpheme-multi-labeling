import csv
import json
import sys
import os
import matplotlib
import matplotlib.pyplot as plt
import my_excepthook
import itertools as it
import numpy as np
from scipy import signal


# Relevant to create pgf files
matplotlib.rcParams.update({'pgf.rcfonts': False})



# Default column for the scores in tensorboard exported csv
VALUE_COLUMN = 2
STEP_COLUMN = 1

# True will show it but not save it, False will save it, but not show
SHOW_PLOT = False

DEFAULT_LABEL = 'label'

# Force all coordinate systems to have their lower left corner at 0,0
# If the config file specifies different, the config file value is used
DEFAULT_PLOT_TO_START_AT_ORIGIN = True


# Colors for the plots
# COLORS = it.cycle(['b', 'g', 'r', 'y'])

def main():
    config_list, main_dir = get_params()
    for config in config_list:
        csv_file_paths, output_plot_file_name, plot_info = extract_info_from_config(config, main_dir)
        check_for_default_labels(csv_file_paths, main_dir)
        print('Save final plot as "{}"'.format(output_plot_file_name))

        plot_data = load_csv_data(csv_file_paths)

        create_plot(plot_data, plot_info)
        if SHOW_PLOT:
            plt.show()
        else:
            # plt.gca().set_position([0, 0, 1, 1])
            plt.savefig(output_plot_file_name, transparent=True)

    return


def load_csv_data(csv_file_paths):
    plot_data = {}
    for label, csv_file_path in sorted(csv_file_paths.items()):
        values = get_values_from_csv(csv_file_path, VALUE_COLUMN)
        steps = get_values_from_csv(csv_file_path, STEP_COLUMN)
        plot_data[label] = {'values': values, 'steps':steps}

    return plot_data


def create_plot(plot_data, plot_info):
    plt.clf()
    set_options_plots()
    # plt.yscale("log")

    # plot the graphes
    for label, valueDict in plot_data.items():
        values = valueDict['values']
        steps = valueDict['steps']

        # Little hack to add a gaussian filter over data
        if(plot_info['use_smoothing']):
            w = int(plot_info['smoothing_window'])
            window = signal.gaussian(w, std=plot_info['smoothing_strength'])
            window = window/np.sum(window)
            values = np.convolve(values, window, mode='same')
            values = values[w:-w]
            steps = steps[w:-w]

        plt.plot(steps, values, label=label)
        # plt.loglog(steps, values, label=label)



    # Set limits of the plots
    if(not plot_info['limits']["plot_max_x"]):
        plot_info['limits']["plot_max_x"] = max([v['steps'][-1] for k,v in plot_data.items()])
    if(DEFAULT_PLOT_TO_START_AT_ORIGIN):
        if(not plot_info['limits']["plot_min_x"]):
            plot_info['limits']["plot_min_x"] = 0
        if(not plot_info['limits']["plot_min_y"]):
            plot_info['limits']["plot_min_y"] = 0

    plt.xlim(left=plot_info['limits']["plot_min_x"], right=plot_info['limits']["plot_max_x"])
    plt.ylim(bottom=plot_info['limits']["plot_min_y"], top=plot_info['limits']["plot_max_y"])

    plt.title(plot_info['title'])
    plt.xlabel("steps")
    # plt.ylabel("score")

    plt.legend(loc=2) # 0 is best, 0 to 10 would be always the same position


# Some default options that make the plot look nicer
def set_options_plots():
    ax = plt.subplot(111)
    ax.spines["top"].set_visible(False)
    # ax.spines["bottom"].set_visible(False)
    ax.spines["right"].set_visible(False)
    # ax.spines["left"].set_visible(False)

    plt.grid(True)



# Returns a list of values of one column
# Defaults to column 2, as this fits the tensorboard returned csv file
def get_values_from_csv(csv_file_path, column_to_return=2):
    try:
        with open(csv_file_path, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            # Skip first line as it is the header
            header = next(csv_reader)
            values = []
            for row in csv_reader:
                values.append(float(row[column_to_return]))
    except:
        print('Failed while reading "{}"'.format(csv_file_path))
        exit()

    return values


# Returns the path to the directory containing the given file
# steps_above must be positive, and represents how many directories to go up
def get_dir_path_above(file_path, steps_above=1):
    ups = [os.pardir] * steps_above
    return os.path.abspath(os.path.join(file_path, *ups))


# All information is saved in a config file
def get_params():
    if(len(sys.argv) != 2):
        print('Parameter missing\nUsage: "python {} CONFIG.JSON"'.format(sys.argv[0]))
        exit()
    config_file = sys.argv[1]
    with open(config_file) as f:
        config_list = json.load(f)

    main_dir = get_dir_path_above(config_file)
    return config_list, main_dir


# While the config file itself contains all relevant information, some additional information must be computed from the configuration file
def extract_info_from_config(config, main_dir):
    # This saves all additional info
    plot_info = {}
    plot_info['title'] = get_readable_plot_name(config["plot_title"])

    # The output file name will be [plot_file_prefix][plot_title]_plot. Instead, you could also use the key [output_plot_file_name], which will be used instead of the combination.
    if("output_plot_file_name" in config.keys()):
        output_plot_file_name = os.path.join(main_dir, config["output_plot_file_name"])
    else:
        #TODO: do I need the file extension?
        output_plot_file_name = os.path.join(main_dir, "{}_{}_plot.{}".format(config["plot_file_prefix"], config["plot_title"], config["output_format"]))

    csv_file_paths = {k: os.path.join(main_dir, config["csv_dir"], v) for k, v in config["csv_files"].items()}

    output_plot_file_name = output_plot_file_name.replace(" ", "_")

    # Add max and min values in x and y direction for the plots
    # If they do not exist in the config file, set it to None, what will make if to the default value
    limit_keys = ["plot_min_x", "plot_max_x", "plot_min_y", "plot_max_y", ]
    plot_info['limits'] = {key: config[key] if key in config.keys() else None for key in limit_keys}

    # If any parameters for smoothing exist, use it
    if("smoothing_window" in config.keys() and "smoothing_strength" in config.keys()):
        plot_info['use_smoothing'] = True
        plot_info['smoothing_window'] = config['smoothing_window']
        plot_info['smoothing_strength'] = config['smoothing_strength']
    else:
        plot_info['use_smoothing'] =  False


    return csv_file_paths, output_plot_file_name, plot_info


def get_readable_plot_name(name):
    if name == 'segments_0.00-0.01':
        return "Macro F1 score of the most frequent 1% of labels"
    if name == 'segments_0.75-1.0':
        return "Macro F1 score of least frequent 25% of labels"
    if name == 'macro_precision':
        return "Macro precision"
    if name == 'macro_recall':
        return "Macro recall"
    elif name == 'macro_f1':
        return "Macro F1"
    elif name == 'loss':
        return "Loss"
    elif name == 'micro_f1':
        return "Micro F1"
    else:
        return name


def check_for_default_labels(csv_file_paths, main_dir):
    if any(label.startswith(DEFAULT_LABEL) for label in csv_file_paths.keys()):
        print('Watch out, the config file in "{}" still contains default labels!'.format(main_dir))




if __name__ == '__main__':
    main()
