import json
import my_excepthook
import sys
import os
import json
from collections import OrderedDict
from pprint import pprint


"""
This script will generate default script containing all relevant information to create a plot. It specifies which csv files to use and how to call the graphs beneath more detailed configs.
"""

# Top level directory name containing everything.
# The config will concat all directory names up to this for the plot file name prefix
TOP_DIR = 'files'
DEFAULT_LABEL = 'label'


def main():
    csv_dir = get_params()
    main_dir = get_dir_path_above(csv_dir)
    config_path = os.path.join(main_dir, 'config.json')

    csv_subdirs = [os.path.join(csv_dir, d) for d in os.listdir(csv_dir)]

    configs = []
    for csv_subdir in csv_subdirs:
        configs.append(generate_config_dict(csv_subdir))

    save_as_json(configs, config_path)


def generate_config_dict(csv_subdir):
    config = OrderedDict()
    config["plot_title"] = os.path.basename(csv_subdir)
    config["plot_file_prefix"] = get_plot_prefix(csv_subdir)
    config["csv_dir"] = get_last_n_root_path(csv_subdir)
    config["output_format"] = "pgf"


    config["csv_files"] = {}
    for idx, csv_file in enumerate(os.listdir(csv_subdir)):
        config["csv_files"]['{}-{}'.format(DEFAULT_LABEL, idx)] = csv_file

    return config


# Returns the last n root directories from dir
def get_last_n_root_path(dir, n=2):
    root, base = os.path.split(dir)
    out_list = [base]
    for _ in range(n-1):
        root, base = os.path.split(root)
        out_list.insert(0, base)

    return os.path.join(*out_list)


def get_plot_prefix(csv_subdir):
    dir_names = []
    current_dir = get_dir_path_above(csv_subdir, 2) # 2 to ignore the csv_files folder
    dir_name = os.path.basename(current_dir)
    # Go up one directory level and save the current
    while dir_name != TOP_DIR:
        dir_names.insert(0, dir_name)
        current_dir = get_dir_path_above(current_dir)
        dir_name = os.path.basename(current_dir)

    return '_'.join(dir_names)



def save_as_json(configs, config_path):
    print('Save config file in "{}"'.format(config_path))
    with open(config_path, 'w') as outfile:
        json.dump(configs, outfile, indent=4, sort_keys=False) # False so the OrderedDict order is kept


# Returns the path to the directory containing the given file
# steps_above must be positive, and represents how many directories to go up
def get_dir_path_above(file_path, steps_above=1):
    ups = [os.pardir] * steps_above
    return os.path.abspath(os.path.join(file_path, *ups))


# All information is saved in a config file
def get_params():
    if(len(sys.argv) != 2):
        print('This will create a config file refering to all csv files in CSV_FOLDER/*/*.csv\nEach folder behind CSV_FOLDER will be seperated in the config\n\nHow to use: "python gen_default_config.py CSV_FOLDER"')
        exit()
    csv_dir = sys.argv[1]

    return csv_dir




if __name__ == '__main__':
    main()
