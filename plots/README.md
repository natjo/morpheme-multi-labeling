# How to use this
Created plots can be viewed with _ktikz_ available on Ubuntu. If created with a different format than .pgf, see for yourself how to view them.

## Where to save csv files from tensorboard
You need to follow a certain folder structer. All folder names contained inside a `[]` must be named like this. There can be as many sublevel folders as whished. It must only end in a `csv_files` folder containing metrics. The idea is to have each folder level describe a certain experimental setting, e.g. data set or layer sizes.

```
[files]
  └──dataset
        ├── evaluate_configs
        │   ├── morphemes
        │   │   └── [csv_files]
        │   │       ├── macro_f1
        │   │       │   ├── results1.csv
        │   │       │   └── results2.csv
        │   │       └── micro_f1
        │   └── words
        │       └── [csv_files]
        │           ├── macro_f1
        │           └── micro_f1
        └── train_until_convergence
            └── all
                └── [csv_files]
                    ├── macro_f1
                    └── micro_f1
```

The code will create a distinct plot for each `csv_files` folder, creating a graph for each .csv file inside this folder.
The final plot name will follow this naming: `dataset_evaluate_configs_morphemes_CUSTOMLABEL.pgf`, where CUSTOMLABEL, can be specified in a auto generated config.

## config files
The config files must be created to plot anything. `gen_default_config.py` will generate a config file on a specified `csv_files` folder. Multiple settings for the plot can be changed inside this config
Example:

```
python gen_default_config.py files/dataset/evaluate_configs/morphemes/csv_files
```

will create this file:


```
[files]
  └──dataset
        ├── evaluate_configs
        │   ├── morphemes
        │   │   ├── config.json          **NEW**
        │   │   └── [csv_files]
        │   │       ├── macro_f1
        │   │       │   ├── results1.csv
        │   │       │   └── results2.csv
        │   │       └── micro_f1
        │   └── words
        │       └── [csv_files]
        │           ├── macro_f1
        │           └── micro_f1
        └── train_until_convergence
            └── all
                └── [csv_files]
                    ├── macro_f1
                    └── micro_f1
```

## Creating the plot
To create the plot simply call `plot_csv.py files/dataset/evaluate_configs/morphemes/config.json` and it will create the plot in the same directory, with the name following the folder structure and the labels given in the config file.

## run_on_all.sh
This script can run the above mentioned python scripts on all fitting files inside the `files` folder. It can also create a new folder in the main directory and create or update symbolic links to all created plots.

## Use plots in latex
The .pgf format is perfect for matlab, all plots will look perfect! If you want to have two plots next to each other, you can use this command in latex, which even handles odd pages:

```
\usepackage{ifoddpage}

% to easily plot two graphs next to each other
\newcommand{\showTwoGraphs}[2] {
    \checkoddpage
    \ifoddpage
    {
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-2.0cm}
            \scalebox{0.67}{\input{#1}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{0.3cm}
            \scalebox{0.67}{\input{#2}}
        \end{minipage}
    }
    \else
    {
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-4.0cm}
            \scalebox{0.67}{\input{#1}}
        \end{minipage}\hfill
        \begin{minipage}[b]{0.5\textwidth}
            \hspace*{-1.5cm}
            \scalebox{0.67}{\input{#1}}
        \end{minipage}
    }
    \fi
}
```
