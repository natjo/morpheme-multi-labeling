EXEC_SCRIPT=scripts/execute_file.pl
PYTHON=~/morpheme-multi-labeling/venv/bin/python3
FILE=src/main.py
LOG=saved_runs
NUM_EVAL_DATA=100
DATASET=mercateo


run:
	@./$(EXEC_SCRIPT) $(FILE)  # @ will hide the function call itself

divide_data:
	./data/$(DATASET)/divide_test_training.pl ./data/$(DATASET)/general_data/data/* ./data/$(DATASET)/general_data
	./data/scripts/text_processing/create_all_labels.pl data/$(DATASET)/general_data/training_data.txt data/$(DATASET)/general_data
	# Don't use shuffle for mercateo datasets
	shuf -n $(NUM_EVAL_DATA) ./data/$(DATASET)/general_data/test_data.txt > ./data/$(DATASET)/general_data/eval_data.txt

preprocess_data_morphemes:
	./data/scripts/text_processing/run_mofessor.sh train-readable data/$(DATASET)/general_data/morfessor_training_data.txt data/$(DATASET)/morphemes/preprocessed
	$(PYTHON) data/scripts/numpy_processing/morphemes_to_numbers.py data/$(DATASET)/general_data/training_data.txt training.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/morphemes/preprocessed TRUE
	$(PYTHON) data/scripts/numpy_processing/morphemes_to_numbers.py data/$(DATASET)/general_data/eval_data.txt eval.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/morphemes/preprocessed FALSE
	$(PYTHON) data/scripts/numpy_processing/morphemes_to_numbers.py data/$(DATASET)/general_data/test_data.txt test.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/morphemes/preprocessed FALSE


preprocess_data_words:
	$(PYTHON) data/scripts/numpy_processing/words_to_numbers.py data/$(DATASET)/general_data/training_data.txt training.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/words/preprocessed TRUE
	$(PYTHON) data/scripts/numpy_processing/words_to_numbers.py data/$(DATASET)/general_data/eval_data.txt eval.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/words/preprocessed FALSE
	$(PYTHON) data/scripts/numpy_processing/words_to_numbers.py data/$(DATASET)/general_data/test_data.txt test.npz data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/words/preprocessed FALSE


preprocess_data_bpe:
	./data/scripts/text_processing/bpe_action.sh learn-bpe ./data/$(DATASET)/general_data/morfessor_training_data.txt ./data/$(DATASET)/bpe/preprocessed/learned_bpe.txt
	./data/scripts/text_processing/bpe_prepare_file.sh ./data/$(DATASET)/general_data/eval_data.txt ./data/$(DATASET)/bpe/preprocessed
	./data/scripts/text_processing/bpe_prepare_file.sh ./data/$(DATASET)/general_data/test_data.txt ./data/$(DATASET)/bpe/preprocessed
	./data/scripts/text_processing/bpe_prepare_file.sh ./data/$(DATASET)/general_data/training_data.txt ./data/$(DATASET)/bpe/preprocessed
	$(PYTHON) ./data/scripts/numpy_processing/bpe_to_numbers.py ./data/$(DATASET)/bpe/preprocessed/build_files/training_data_final.txt training.npz ./data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/bpe/preprocessed TRUE
	$(PYTHON) ./data/scripts/numpy_processing/bpe_to_numbers.py ./data/$(DATASET)/bpe/preprocessed/build_files/test_data_final.txt test.npz ./data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/bpe/preprocessed FALSE
	$(PYTHON) ./data/scripts/numpy_processing/bpe_to_numbers.py ./data/$(DATASET)/bpe/preprocessed/build_files/eval_data_final.txt eval.npz ./data/$(DATASET)/general_data/all_labels_data.txt data/$(DATASET)/bpe/preprocessed FALSE


preprocess_generate_word2vec_morpheme:
	$(PYTHON) ./data/$(DATASET)/morphemes/generate_word2vec_model.py
	$(PYTHON) ./data/$(DATASET)/morphemes/generate_word2vec_embedding.py

preprocess_generate_word2vec_words:
	$(PYTHON) ./data/$(DATASET)/words/generate_word2vec_model.py
	$(PYTHON) ./data/$(DATASET)/words/generate_word2vec_embedding.py


clean: clean-log

clean-data:
	 rm data/$(DATASET)/general_data/*.txt data/$(DATASET)/morphemes/preprocessed/* data/$(DATASET)/words/preprocessed/* data/$(DATASET)/bpe/preprocessed/*

clean-log:
	rm -r saved_runs/*

launch_tensorboard:
	venv/bin/tensorboard --reload_interval 1 --logdir $(LOG)
