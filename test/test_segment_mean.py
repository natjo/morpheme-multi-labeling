import tensorflow as tf
import my_excepthook



def get_segment_mean(input_tensor, segment_ends):
    # # ends = tf.constant(segment_ends)
    # start = tf.cast(tf.cast(tf.shape(input_tensor)[0],tf.float32) * segment_start, tf.int32)
    # end = tf.cast(tf.cast(tf.shape(input_tensor)[0],tf.float32) * segment_end, tf.int32)

    # # Range is including the first and excluding the last
    # segment_vals = tf.gather(input_tensor, tf.range(start, end), axis=0)

    # return tf.reduce_mean(segment_vals)



    out_list = []
    old_max_index = 0
    for idx in range(1,len(segment_ends)):
        new_max_index = int(segment_ends[idx] * input_tensor.shape[0].value)
        out_list.append([idx-1, new_max_index - old_max_index])
        old_max_index = new_max_index

    segments = tf.concat([tf.tile([num], [length]) for num, length in out_list], axis=0)
    return tf.segment_mean(input_tensor, segments)



def main():
    X = tf.placeholder(tf.float32, shape=[10])
    nums = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])/X
    in_tensor = pass_only_valid_nums(nums)
    parts = [0,0.2,0.6,1.0]
    out = get_segment_mean(in_tensor, parts)

    with tf.Session() as sess:
        o = sess.run([out], feed_dict={X: [1,1,1,1,1,0,0,0,0,0]})
        print(o)



def pass_only_valid_nums(ins):
    bool_mask = tf.logical_or(tf.is_nan(ins), tf.is_inf(ins))
    bool_mask = tf.logical_not(bool_mask)
    bool_mask = [True, True, True, True, True, False, False, False, False, False]
    masked = tf.boolean_mask(ins, bool_mask)
    num_elements = tf.reduce_sum(tf.cast(bool_mask, tf.float32))
    return tf.reshape(masked, [num_elements])

if __name__ == '__main__':
    main()

