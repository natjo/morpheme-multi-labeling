import tensorflow as tf
import numpy as np
import my_excepthook

tf.enable_eager_execution()


def main():
    in1 = tf.constant([0,1,2,3,4,5])
    in2 = tf.constant([0,1,2,0,0,5])
    div = in1/in2
    print(div)
    out = pass_only_valid_nums(div)
    print(out)


def pass_only_valid_nums(ins):
    bool_mask = tf.logical_or(tf.is_nan(ins), tf.is_inf(ins))
    bool_mask = tf.logical_not(bool_mask)
    masked = tf.boolean_mask(ins, bool_mask)
    num_elements = tf.reduce_sum(tf.cast(bool_mask, tf.float32))
    return tf.reshape(masked, [num_elements])



if __name__ == '__main__':
    main()
