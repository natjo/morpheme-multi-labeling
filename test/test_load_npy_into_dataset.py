import tensorflow as tf
import numpy as np
import my_excepthook



def main():
    data_path = './data/mercateo/morphemes/preprocessed_small/training.npz'
    batch_size = 10

    x, y = load_from_npz(data_path)
    out = tf.estimator.inputs.numpy_input_fn(x,y, batch_size=batch_size, num_epochs=None, shuffle=True)
    a1, a2 = out()
    print(out())
    print('--------')

    pi = tf.constant(3.14, name="pi")
    r = tf.placeholder(tf.float32, name="r")
    a = pi * r * r

    print('run session')
    with tf.Session() as sess:
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)
        o1 = sess.run([a1])
        o2 = sess.run([a1])
        print(o1)
        print(o2)


def load_from_npz(path, dtype=np.int32):
    npzfile = np.load(path)
    features = npzfile['arr_0'].astype(dtype)
    labels = npzfile['arr_1'].astype(dtype)
    return features, labels

if __name__ == '__main__':
    main()
