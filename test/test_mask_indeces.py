import tensorflow as tf
import numpy as np
import my_excepthook

tf.enable_eager_execution()


def main():
    X = tf.constant([0, 1,2,3,4,5])
    mask = [0,1,2,4]
    out = mask_tensor_by_index(X, mask)
    print(out)


def mask_tensor_by_index(tensor, mask):
    bool_mask = [i in mask for i in range(tensor.shape[0])]
    return tf.boolean_mask(tensor, bool_mask)



if __name__ == '__main__':
    main()
