import tensorflow as tf
from load import train_input_fn, eval_input_fn, load_data_info
import my_excepthook
from network import Network
import config
import logging
import os
import json
import numpy as np
import random
_RANDOM_SEED = 1607

# For some progress info during training
logging.getLogger().setLevel(logging.INFO)
# To be save for random stuff
tf.set_random_seed(_RANDOM_SEED)
np.random.seed(_RANDOM_SEED)
random.seed(_RANDOM_SEED)


def main():
    train_once(config.getConfig())
    # only_eval(config.getConfig())
    # train_defined_configs()


def train_defined_configs():
    configDict = config.getConfig()
    configDict['num_units'] = [512]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [1024]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [256]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [1536]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [1024]
    configDict['isBiLSTM'] = False
    train_once(configDict)


def train_once(configDict):
    """
    This function loads the datasets, creates a network and runs training/evaluation twice, once until no more dropout should be used. And then without dropout. It saves the config as a json file
    """
    vocab_size, num_labels_train, num_batches, label_weights = load_data_info(configDict['preprocessed_data_path'], configDict['batch_size_train'])
    if configDict['total_num_features']:
        vocab_size = configDict['total_num_features']

    if configDict['total_num_labels']:
        num_labels_train = configDict['total_num_labels']


    # Calculate how many steps should be trained with/without dropout
    max_steps = num_batches * configDict['epochs']
    if 'epochs_with_dropout' in configDict:
        steps_with_dropout = num_batches * configDict['epochs_with_dropout']
    else:
        steps_with_dropout = max_steps * configDict['perc_epochs_with_dropout']

    print('Train {} in {} steps ({} epochs), stop dropout after {} steps'.format(
                                                                            get_network_name(configDict),
                                                                            max_steps,
                                                                            configDict['epochs'],
                                                                            steps_with_dropout))

    network = get_network(vocab_size, num_labels_train, num_batches, label_weights, configDict)

    # Run with dropout
    if steps_with_dropout > 0.0:
        run_training_eval(network, steps_with_dropout, num_labels_train, configDict)
        # Save config before the dropout is removed
        save_configs(configDict)

    print("Turn off dropout")
    configDict['inner_keep_prob'] = 1.0
    configDict['outer_keep_prob'] = 1.0
    run_training_eval(network, max_steps, num_labels_train, configDict)

    if configDict['perc_epochs_with_dropout'] == 0.0:
        # In case no dropout is used
        save_configs(configDict)


def run_training_eval(network, num_steps, num_labels_train, configDict):
    train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn(configDict['preprocessed_data_path'],                                        configDict['batch_size_train']), max_steps=num_steps)
    eval_spec = tf.estimator.EvalSpec(input_fn=eval_input_fn(configDict['preprocessed_data_path'], configDict['batch_size_test']),
                                        throttle_secs=20, start_delay_secs=20, steps=configDict['max_num_batches_test'])

    estimator = get_estimator(network, num_labels_train, configDict)
    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


def get_estimator(network, num_labels_train, configDict):
    params = {
            'batch_size': configDict['batch_size_predict'],
            'num_labels_train': num_labels_train,
            'inner_keep_prob': configDict['inner_keep_prob'],
            'outer_keep_prob': configDict['output_keep_prob'],
            }
    runConfig = tf.estimator.RunConfig(
            log_step_count_steps=configDict['log_training_every_n_steps'],
            keep_checkpoint_max=1,
            save_checkpoints_secs=configDict['save_checkpoints_secs'],
            tf_random_seed=_RANDOM_SEED,
            model_dir=os.path.join(configDict['log_dir'], configDict['network_name'], get_network_name(configDict)),
            )
    estimator = tf.estimator.Estimator(
            network.model_fn,
            params = params,
            config = runConfig
            )
    return estimator


def only_eval(configDict):
    vocab_size, num_labels_train, num_batches = load_data_info(configDict['preprocessed_data_path'], configDict['batch_size_train'])

    if configDict['total_num_features']:
        vocab_size = configDict['total_num_features']

    if configDict['total_num_labels']:
        num_labels_train = configDict['total_num_labels']

    network = get_network(vocab_size, num_labels_train, num_batches, configDict)

    estimator = get_estimator(network, num_labels_train, configDict)

    estimator.evaluate(input_fn=eval_input_fn(configDict['preprocessed_data_path'], configDict['batch_size_test']),
                        steps=configDict['max_num_batches_test'], name='single')


def get_network(vocab_size, num_labels, num_batches, label_weights, configDict):
    # Check if embedding is used
    if configDict['use_embedding']:
        predefined_embedding = load_embedding(configDict['preprocessed_data_path'])
    else:
        predefined_embedding = None

    # Compute parameters for the learning rate
    decay_lr = (configDict['final_lr']/configDict['init_lr'])**(1.0/configDict['epochs'])

    # Define network
    n = Network(configDict['num_units'],
            vocab_size,
            configDict['embedding_dim'],
            num_labels,
            isBiLSTM=configDict['is_BiLSTM'],
            predefined_embedding=predefined_embedding,
            isEmbeddingTrainable=configDict['is_embedding_trainable'],
            loss_function=configDict['loss_function'],
            optimizer=configDict['optimizer'],
            init_lr=configDict['init_lr'],
            decay_lr=decay_lr,
            steps_per_epoch=num_batches,
            loss_pos_weight=configDict['loss_pos_weight'],
            label_weights=label_weights
            )

    return n


# def get_network_name(configDict):
#     return '{}_size_{}_embdim_{}_epochsWithDrop_{}_isBi_{}_optimizer_{}_loss_{}_lossParam_{}'.format(
#             configDict['network_name'],
#             '-'.join([str(i) for i in configDict['num_units']]),
#             configDict['embedding_dim'],
#             configDict['epochs_with_dropout'],
#             configDict['is_BiLSTM'],
#             configDict['optimizer'],
#             configDict['loss_function'],
#             configDict['loss_pos_weight'],)


def get_network_name(configDict):
    return '{}_size_{}_embdim_{}_epochsWithDrop_{}_isBi_{}_optimizer_{}_loss_{}'.format(
            configDict['network_name'],
            '-'.join([str(i) for i in configDict['num_units']]),
            configDict['embedding_dim'],
            configDict['epochs_with_dropout'],
            configDict['is_BiLSTM'],
            configDict['optimizer'],
            configDict['loss_function'],)


def save_configs(configDict):
    save_path = os.path.join(configDict['log_dir'], configDict['network_name'], get_network_name(configDict), 'usedConfig.json')
    print('Save network {} at "{}"'.format(get_network_name(configDict), save_path))
    with open(save_path, 'w') as fp:
        json.dump(configDict, fp, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
