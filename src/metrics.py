import tensorflow as tf
import utils


def metric_variable(name, shape, dtype=tf.int32, initializer=tf.zeros_initializer):
    """Create variable in `GraphKeys.(LOCAL|METRIC_VARIABLES`) collections."""
    return tf.get_variable(
            name,
            shape=shape,
            dtype=dtype,
            initializer=initializer,
            collections=[tf.GraphKeys.LOCAL_VARIABLES, tf.GraphKeys.METRIC_VARIABLES],
            )


def add_tf_metrics(labels_one_hot, predictions_one_hot, metrics_dict):
    accuracy = tf.metrics.accuracy(labels=labels_one_hot, predictions=predictions_one_hot, name='acc_op')
    metrics_dict['tf/accuracy'] = accuracy
    precision = tf.metrics.precision(labels=labels_one_hot, predictions=predictions_one_hot)
    metrics_dict['tf/precision'] = precision
    recall = tf.metrics.recall(labels=labels_one_hot, predictions=predictions_one_hot)
    metrics_dict['tf/recall'] = recall
    return metrics_dict


def def_streaming_metrics(predictions, groundTruth, num_labels):
    # Define variables
    with tf.variable_scope('internal_statistics'):
        tp_mic = metric_variable('tp_mic', shape=[])
        fp_mic = metric_variable('fp_mic', shape=[])
        fn_mic = metric_variable('fn_mic', shape=[])

        tp_mac = metric_variable('tp_mac', shape=[num_labels])
        fp_mac = metric_variable('fp_mac', shape=[num_labels])
        fn_mac = metric_variable('fn_mac', shape=[num_labels])

        # To check how many updates happened and as a trigger variable
        num_up_calls = metric_variable("num_update_calls", shape=[])

        # Update ops
        up_tp_mac = tf.assign_add(tp_mac, tf.count_nonzero(predictions * groundTruth, axis=0, dtype=tf.int32))
        up_fp_mac = tf.assign_add(fp_mac, tf.count_nonzero(predictions * (groundTruth - 1), axis=0, dtype=tf.int32))
        up_fn_mac = tf.assign_add(fn_mac, tf.count_nonzero((predictions - 1) * groundTruth, axis=0, dtype=tf.int32))

        up_tp_mic = tf.assign_add(tp_mic, tf.count_nonzero(predictions * groundTruth, axis=None, dtype=tf.int32))
        up_fp_mic = tf.assign_add(fp_mic, tf.count_nonzero(predictions * (groundTruth - 1), axis=None, dtype=tf.int32))
        up_fn_mic = tf.assign_add(fn_mic, tf.count_nonzero((predictions - 1) * groundTruth, axis=None, dtype=tf.int32))

        # Define update op
        # update_op = tf.group(up_tp_mic, up_fp_mic, up_fn_mic, up_tp_mac, up_fp_mac, up_fn_mac)
        update_op = [up_tp_mic, up_fp_mic, up_fn_mic, up_tp_mac, up_fp_mac, up_fn_mac]
        # Update all counters when the up_num_calls is executed
        with tf.control_dependencies(update_op):
            up_num_calls = tf.assign_add(num_up_calls, 1)


    with tf.variable_scope('update_metrics'):
        # Calculate all metrics

        # Per label statistics
        prec_per_label = utils.pass_only_valid_nums(tp_mac / (tp_mac + fp_mac))
        rec_per_label = utils.pass_only_valid_nums(tp_mac / (tp_mac + fn_mac))
        f1_per_label = utils.pass_only_valid_nums(2 * tp_mac / (2 * tp_mac + fp_mac + fn_mac))

        # Macro
        prec_mac = tf.reduce_mean(prec_per_label)
        rec_mac = tf.reduce_mean(rec_per_label)
        f1_mac = tf.reduce_mean(f1_per_label)

        # Micro
        prec_mic = utils.replace_nan_with_zero(tp_mic / (tp_mic + fp_mic))
        rec_mic = utils.replace_nan_with_zero(tp_mic / (tp_mic + fn_mic))
        f1_mic = utils.replace_nan_with_zero(2 * prec_mic * rec_mic / (prec_mic + rec_mic))


        metrics = {'metrics/micro_f1': (f1_mic, up_num_calls),
                   'metrics/macro_f1': (f1_mac, up_num_calls),
                   'metrics/micro_recall': (rec_mic, up_num_calls),
                   'metrics/macro_recall': (rec_mac, up_num_calls),
                   'metrics/micro_precision': (prec_mic, up_num_calls),
                   'metrics/macro_precision': (prec_mac, up_num_calls),
                   'statistics/micro_TP': (tp_mic, up_num_calls),
                   'statistics/micro_FP': (fp_mic, up_num_calls),
                   'statistics/micro_FN': (fn_mic, up_num_calls),
                   'num_update_calls_streaming': (num_up_calls, up_num_calls),}

        with tf.variable_scope('get_segments'):
            # The last labels do not occur in the test set, therefore they are not included in the segments
            # segments go to #labels: 15000, 4000, 1000, 450, 120, 50, 20
            segment_ends = [0.0, 0.01, 0.05, 0.15, 0.25, 0.5, 0.75, 1.0]
            metrics = add_segmented_metric('precision', prec_per_label, segment_ends[:], metrics, up_num_calls)
            metrics = add_segmented_metric('recall', rec_per_label, segment_ends[:], metrics, up_num_calls)
            metrics = add_segmented_metric('f1', f1_per_label, segment_ends[:], metrics, up_num_calls)

    return metrics, up_num_calls



def add_segmented_metric(name, metric, segment_ends, metricDict, up_num_calls):
    """
    name: specifies the kind of metric used in tensorboard later
    metric: the 2D tensor of metrics
    segment_ends: a list of percentages to specify the size of each segment, e.g. [0.0, 0.3, 1.0]
    metricDict: the dictionary the metrics should be appended to
    up_num_calls: the trigger for each metric to be updated
    """
    for idx in range(1, len(segment_ends)):
        segment_metric = utils.get_segment_mean(metric, segment_ends[idx-1], segment_ends[idx])
        key = 'segments/{}:{:.2f}-{:.2f}'.format(name, segment_ends[idx-1], segment_ends[idx])
        metricDict[key] = (segment_metric, up_num_calls)
    return metricDict



def add_old_metrics(predictions, labels):
    with tf.variable_scope('metrics'):
        # Variables are named by the subsets of binary classifications they represent
        predictions = tf.cast(predictions, tf.float32)
        labels = tf.cast(labels, tf.float32)

        TP = predictions * labels
        FN_2TP_FP = predictions + labels
        TP_FP = predictions
        TP_FN = labels

        with tf.variable_scope('f1_score'):
            macro_f1 = 2 * utils.reduce_sum_handle_nan(TP)/tf.reduce_sum(FN_2TP_FP)
            micro_f1 = 2 * utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(FN_2TP_FP,0))
        with tf.variable_scope('precision'):
            macro_precision = utils.reduce_sum_handle_nan(TP)/(utils.reduce_sum_handle_nan(TP_FP) + 1e-8) # handle no true predictions
            micro_precision = utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FP,0))
        with tf.variable_scope('recall'):
            macro_recall = tf.reduce_sum(TP)/tf.reduce_sum(TP_FN)
            micro_recall = utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FN,0))

        tf.summary.scalar('micro_F1', micro_f1)
        tf.summary.scalar('macro_F1', macro_f1)
        tf.summary.scalar('micro_precision', micro_precision)
        tf.summary.scalar('macro_precision', macro_precision)
        tf.summary.scalar('micro_recall', micro_recall)
        tf.summary.scalar('macro_recall', macro_recall)

    with tf.variable_scope('tf_metrics'):
        recall, recall_op = tf.metrics.recall(labels, predictions)
        precision, prec_op = tf.metrics.precision(labels, predictions)
        f1 = 2 * precision * recall / (precision + recall)

        tf.summary.scalar('tf_recall', recall)
        tf.summary.scalar('tf_precision', precision)
        tf.summary.scalar('tf_f1', f1)

    return tf.group(prec_op, recall_op)


