import tensorflow as tf


def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summary_'+name):
        with tf.name_scope('summaries_' + name):
            mean = tf.reduce_mean(var)
        tf.summary.scalar('mean_' + name, mean)
        with tf.name_scope('stddev_' + name):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev_' + name, stddev)
        tf.summary.scalar('max_' + name, tf.reduce_max(var))
        tf.summary.scalar('min_' + name, tf.reduce_min(var))
        tf.summary.histogram(name, var)


# See https://stackoverflow.com/a/47643427/4296244 for details
# name shoulf be fw or bw
def log_lstm_variables(cells, name):
    for idx, one_lstm_cell in enumerate(cells):
        one_kernel, one_bias = one_lstm_cell.variables
        # I think TensorBoard handles summaries with the same name fine.
        with tf.variable_scope('{}_lstm_layer_{}'.format(name, idx)):
            tf.summary.histogram("Kernel", one_kernel)
            tf.summary.histogram("Bias", one_bias)


def reduce_mean_handle_nan(ins):
    return tf.reduce_mean(replace_nan_with_zero(ins), name='reduce_mean_handle_nan')


def reduce_sum_handle_nan(ins):
    return tf.reduce_sum(replace_nan_with_zero(ins), name='reduce_sum_handle_nan')


def replace_nan_with_zero(ins):
    return tf.where(tf.is_nan(ins), tf.zeros_like(ins), ins)

# This function will transform a given tensor into a new tensor which does not contain any nans or infs
def pass_only_valid_nums(ins):
    # x = tf.constant([1,2,3])
    # m = [True, False, True]
    # print(tf.boolean_mask(x, m))
    bool_mask = tf.logical_or(tf.is_nan(ins), tf.is_inf(ins))
    bool_mask = tf.logical_not(bool_mask)
    masked = tf.boolean_mask(ins, bool_mask)
    return masked


def indexes2labels(indexes):
    model = load_numer2label('./data/general_data/all_labels_data.txt')
    labels = [getNumber2Label(model, i) for i in indexes]
    return labels


def getNumber2Label(model, number):
    if number in model.keys():
        return model[number]
    else:
        raise KeyError('"{}" is not a valid index'.format(number))


def load_numer2label(all_labels_path):
    number2label = {}
    with open(all_labels_path) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            number2label[idx] = line[:-1]
    return number2label


def get_total_trainable_parameters():
    total_parameters = 0
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        shape = variable.get_shape()
        variable_parameters = 1
        for dim in shape:
            variable_parameters *= dim.value
        total_parameters += variable_parameters
    return total_parameters



def get_segment_mean(input_tensor, segment_start, segment_end):
    start = tf.cast(tf.cast(tf.shape(input_tensor)[0],tf.float32) * segment_start, tf.int32)
    end = tf.cast(tf.cast(tf.shape(input_tensor)[0],tf.float32) * segment_end, tf.int32)

    # Range is including the first and excluding the last
    segment_vals = tf.gather(input_tensor, tf.range(start, end), axis=0)

    return tf.reduce_mean(segment_vals)
