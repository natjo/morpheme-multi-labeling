import tensorflow as tf
from metrics import def_streaming_metrics, add_tf_metrics
from loss_functions import define_ranking_loss, define_warp_loss


class Network():
    def __init__(self, num_units, vocab_size, embedding_dim, num_labels, isBiLSTM=True, predefined_embedding=None, isEmbeddingTrainable=False, loss_function='sigmoidCrossEntropy', optimizer='adam', init_lr=0.1, decay_lr=0.9, steps_per_epoch=9999, loss_pos_weight=1.0, label_weights=[]):
        self.num_units = num_units
        self.isEmbeddingTrainable = isEmbeddingTrainable
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.num_labels = num_labels
        self.inner_keep_prob = 1.0
        self.outer_keep_prob = 1.0
        self.predefined_embedding = predefined_embedding
        self.isBiLSTM = isBiLSTM
        self.loss_function = loss_function
        self.optimizer = optimizer
        self.init_lr = init_lr
        self.decay_lr = decay_lr
        self.steps_per_epoch = steps_per_epoch
        self.loss_pos_weight = loss_pos_weight
        self.label_weights = label_weights


    def model_fn(self, features, labels, mode, params):
        if mode == tf.estimator.ModeKeys.PREDICT:
            predicted_labels, logits = self.create_predict_graph(features, params)
            predictions = {
                    'class_ids': predicted_labels[:, tf.newaxis],
                    'probabilities': tf.nn.softmax(logits),
                    'logits': logits,
                    }
            return tf.estimator.EstimatorSpec(mode, predictions=predictions)

        if mode == tf.estimator.ModeKeys.EVAL:
            loss, metrics = self.create_eval_graph(features, labels, params)
            return tf.estimator.EstimatorSpec(
                    mode, loss=loss, eval_metric_ops=metrics)

        if mode == tf.estimator.ModeKeys.TRAIN:
            loss, train_op = self.create_train_graph(features, labels, params)
            return tf.estimator.EstimatorSpec(
                    mode, loss=loss, train_op=train_op)



    def _create_networkcore(self, features):
        # Load the embedding
        if self.predefined_embedding is None:
            embedding_init = tf.random_uniform_initializer(-1, 1)
            shape = [self.vocab_size, self.embedding_dim]
        else:
            embedding_init = tf.constant(self.predefined_embedding)
            shape = None

        embedding_matrix = tf.get_variable('input_symbol_embeddings',
                                        shape=shape,
                                        initializer=embedding_init,
                                        trainable=self.isEmbeddingTrainable)
        embedded_features = tf.nn.embedding_lookup(embedding_matrix, features)

        if self.isBiLSTM:
            fw_cells = original_fw_cells = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]
            bw_cells = original_bw_cells = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]

            fw_cells = [tf.nn.rnn_cell.DropoutWrapper(lstm, state_keep_prob=self.inner_keep_prob) for lstm in fw_cells]
            bw_cells = [tf.nn.rnn_cell.DropoutWrapper(lstm, state_keep_prob=self.inner_keep_prob) for lstm in bw_cells]

            # Output of lstms
            _, fw_out, bw_out = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(fw_cells, bw_cells, embedded_features, sequence_length=tf.count_nonzero(features, 1), dtype=tf.float32)

            fw_out_encoded = fw_out[-1].h
            bw_out_encoded = bw_out[-1].h
            core_out = tf.concat([fw_out[-1].h, bw_out[-1].h], axis=1)
            weights_shape =[self.num_units[-1] * 2, self.num_labels]

        else:
            lstms = original_lstm = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]
            lstms = [tf.nn.rnn_cell.DropoutWrapper(lstm,state_keep_prob=self.inner_keep_prob) for lstm in lstms]

            # Network structure
            cell = tf.contrib.rnn.MultiRNNCell(lstms)

            # Output of lstms
            stm_outputs, encoder_state = tf.nn.dynamic_rnn(cell, embedded_features, sequence_length=tf.count_nonzero(features, 1), dtype=tf.float32)
            # utils.log_lstm_variables(original_lstm, 'lstm')
            core_out = encoder_state[-1].h
            weights_shape =[self.num_units[-1], self.num_labels]

        # lstm output to final prediction
        with tf.variable_scope('output_weights'):
            size_core_out = int(core_out.get_shape()[1])
            weight_init = tf.truncated_normal([size_core_out, self.num_labels]) #, stddev=1.0/ math.sqrt(size_core_out))
            weights = tf.get_variable('output_weights', initializer=weight_init)
            # utils.variable_summaries(weights, name='output_weights')
        with tf.variable_scope('output_biases'):
            biases = tf.get_variable('output_biases', initializer=tf.constant(0.1, shape=[self.num_labels]))
            # utils.variable_summaries(biases, name='output_biases')

        return core_out, weights, biases


    def create_train_graph(self, features, labels, params):
        self.inner_keep_prob = params['inner_keep_prob']
        self.outer_keep_prob = params['outer_keep_prob']
        core_out, weights, biases = self._create_networkcore(features)
        core_out = tf.nn.dropout(core_out, self.outer_keep_prob)

        with tf.variable_scope('label_multi_hot_encoding'):
            labels_one_hot = tf.reduce_max(tf.one_hot(labels, self.num_labels), axis=1)

        with tf.variable_scope('output_calculations'):
            logits = tf.nn.bias_add(tf.matmul(core_out, weights), biases)

        with tf.variable_scope('loss_calculations'):
            loss = self.get_loss(labels_one_hot=labels_one_hot, logits=logits)

        optimizer = self.get_optimizer()
        train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())

        return loss, train_op




    def create_eval_graph(self, features, labels, params):
        core_out, weights, biases = self._create_networkcore(features)

        with tf.variable_scope('label_multi_hot_encoding'):
            labels_one_hot = tf.reduce_max(tf.one_hot(labels, self.num_labels), axis=1)

        with tf.variable_scope('output_calculations'):
            logits = tf.nn.bias_add(tf.matmul(core_out, weights), biases)
            predictions_one_hot = tf.cast(tf.greater(tf.nn.sigmoid(logits), 0.5), tf.float32)

        metrics, metrics_update_trigger = def_streaming_metrics(predictions_one_hot, labels_one_hot, params['num_labels_train'])

        with tf.variable_scope('loss_calculations'):
            loss = self.get_loss(labels_one_hot=labels_one_hot, logits=logits)

        return loss, metrics


    def create_predict_graph(self, features, params):
        core_out, weights, biases = self._create_networkcore(features)
        with tf.variable_scope('output_calculations'):
            logits = tf.nn.bias_add(tf.matmul(core_out, weights), biases)
            predictions_one_hot = tf.cast(tf.greater(tf.nn.sigmoid(logits), 0.5), tf.int32)


        pos_labels = tf.cast(tf.where(predictions_one_hot), tf.int32)
        predicted_labels = tf.dynamic_partition(pos_labels[:, 1], pos_labels[:, 0], params['batch_size_predict'])

        return predicted_labels, logits


    def get_loss(self, labels_one_hot, logits):
        if self.loss_function == 'sigmoidCrossEntropy':
            loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels_one_hot, logits=logits, name='sigmoidCrossEntropy'))
        elif self.loss_function == 'sigmoidCrossEntropyWithL2Reg':
            loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels_one_hot, logits=logits))
            l2_reg_variables = [tf.nn.l2_loss(tf.cast(tf_var, tf.float32)) for tf_var in tf.trainable_variables() if not ("bias" in tf_var.name)]
            l2 = sum(l2_reg_variables)
            loss += l2
        elif self.loss_function == 'sigmoidCrossEntropyWeightedFrequencies':
            loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels_one_hot, logits=logits, name='sigmoidCrossEntropyWeightedFrequencies')
            scaled_error = tf.multiply(loss, self.label_weights)
            loss = tf.reduce_mean(scaled_error)
        elif self.loss_function == 'sigmoidCrossEntropyWeightedLabels':
            loss = tf.nn.weighted_cross_entropy_with_logits(targets=labels_one_hot, logits=logits, pos_weight=self.loss_pos_weight, name='sigmoidCrossEntropyWeightedLabels')
            loss = tf.reduce_mean(loss)
        elif self.loss_function == 'mse':
            loss = tf.losses.mean_squared_error(labels_one_hot, logits)
        elif self.loss_function == 'rankingLoss':
            loss = define_ranking_loss(labels_one_hot, logits)
        elif self.loss_function == 'warpLoss':
            loss = define_warp_loss(labels_one_hot, logits)
        else:
            raise ValueError('Unknown loss function {}'.format(self.loss_function))

        return loss


    def get_optimizer(self):
        if self.optimizer == 'Adam':
            optimizer = tf.train.AdamOptimizer()
        elif self.optimizer == 'RMSProp':
            learning_rate = tf.train.exponential_decay(self.init_lr, tf.train.get_or_create_global_step(), self.steps_per_epoch, self.decay_lr)
            optimizer = tf.train.RMSPropOptimizer(learning_rate)
        elif self.optimizer == 'GradientDescent':
            learning_rate = tf.train.exponential_decay(self.init_lr, tf.train.get_or_create_global_step(), self.steps_per_epoch, self.decay_lr)
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        else:
            raise ValueError('Unknown optimizer {}'.format(self.optimizer))

        return optimizer
