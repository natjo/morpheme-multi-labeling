import numpy as np
import math
import os
import tensorflow as tf


def train_input_fn_OLD(features, labels, batch_size):
    dataset = tf.data.Dataset.from_tensor_slices((features, labels))
    dataset = dataset.shuffle(1000).repeat().batch(batch_size)

    return dataset.make_one_shot_iterator().get_next()


def train_input_fn(path, batch_size):
    data_path = os.path.join(path, 'training.npz')

    x, y = load_from_npz(data_path)
    return tf.estimator.inputs.numpy_input_fn(x,y, batch_size=batch_size, num_epochs=None, shuffle=True)


def eval_input_fn(path, batch_size):
    data_path = os.path.join(path, 'test.npz')

    x, y = load_from_npz(data_path)
    return tf.estimator.inputs.numpy_input_fn(x,y, batch_size=batch_size, num_epochs=None, shuffle=True)


def eval_input_fn_OLD(features, labels, batch_size):
    if labels is None:
        inputs = features
    else:
        inputs = (features, labels)

    dataset = tf.data.Dataset.from_tensor_slices(inputs)
    dataset = dataset.batch(batch_size)

    # return dataset
    return dataset.make_one_shot_iterator().get_next()


def load_data_info(path, batch_size):
    train_features, train_labels = load_from_npz(os.path.join(path, 'training.npz'), dtype=np.int32)
    test_features, test_labels = load_from_npz(os.path.join(path, 'test.npz'), dtype=np.int32)
    # label_weights = np.load(os.path.join(path, 'label_weights.npy')).astype(np.float32)
    label_weights = []

    num_labels_train = np.max(train_labels) + 1  # To account for the zero
    vocab_size = np.max([np.max(test_features), np.max(train_features)]) + 1  # To account for the zero
    dataset_size = train_labels.shape[0]
    num_batches = math.ceil(dataset_size/batch_size)

    print_statistics(train_features, train_labels, batch_size, path)
    print_statistics(test_features, test_labels, batch_size, path)

    return vocab_size, num_labels_train, num_batches, label_weights



def load_data_OLD(path, batch_size):
    train_features, train_labels = load_from_npz(os.path.join(path, 'training.npz'), dtype=np.int32)
    test_features, test_labels = load_from_npz(os.path.join(path, 'test.npz'), dtype=np.int32)

    num_labels_test = np.max(test_labels) + 1  # To account for the zero
    num_labels_train = np.max(train_labels) + 1  # To account for the zero
    vocab_size = np.max(train_features) + 1  # To account for the zero
    dataset_size = train_labels.shape[0]
    num_batches = math.ceil(dataset_size/batch_size)

    print_statistics(train_features, train_labels, batch_size, path)
    print_statistics(test_features, test_labels, batch_size, path)

    return (train_features, train_labels), (test_features, test_labels), (vocab_size, num_labels_train, num_labels_test, num_batches)


# name is only relevant for the printed output here
def print_statistics(features, labels, batch_size, path):
    num_labels = np.max(labels) + 1  # To account for the zero
    dataset_size =labels.shape[0]
    num_features = features.shape[1]
    num_batches = math.ceil(dataset_size/batch_size)
    print('In "{}": Loaded {} datapoints of dimension {} and {} labels in {} batches'
            .format(path, dataset_size,num_features,num_labels,num_batches))


def load_from_npz(path, dtype=np.int32):
    npzfile = np.load(path)
    features = npzfile['arr_0'].astype(dtype)
    labels = npzfile['arr_1'].astype(dtype)
    return features, labels
