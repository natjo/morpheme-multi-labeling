import my_excepthook
import numpy as np
from tqdm import tqdm
import config
import random
import pickle
import sys
import os

"""
How to use: "python words_to_number.py DATASET OUTPUT_NAME ALL_LABELS_PATH PREPROCESSED_DIR IS_TRAINING(TRUE|FALSE)"

Transforms the text data into numbers, no one hot encoding and no word2vec embedding.
It assigns each label and each morpheme respectively a number.
Call it like 'python data_to_numbers.py INPUT_DATA OUTPUT.npz'

Every token in the training is seen at least once before it can turn into an UNKNOWN_VALUE by the given chance.

If no UNKNOWN_VALUEs should be used at all, set IS_TRAINING to TRUE and in the configs 'perc_unknown_tokens' to -1
"""


WORD2NUMBER_MODEL = 'word2number.pcl'

DATA_TYPE = np.int32

EMPTY_VALUE = 0
UNKNOWN_VALUE = 1


def main():
    configDict = config.getConfig()
    data_path, all_labels_path, preprocessed_dir, output_name, isTraining = get_file_paths()

    # Load look up functionality
    label2number = load_label2Num(all_labels_path)
    word2number = load_word2number(os.path.join(preprocessed_dir, WORD2NUMBER_MODEL))  # the index of an element is the corresponding number to the element(=word)
    numCutOffs = 0  # Used to count the times the length of the datapoint extend the max length


    # Iterate over all data points
    with open(data_path) as f:
        lines = f.readlines()
        print('Found {} datapoints in {}'.format(len(lines), data_path))
        pbar = tqdm(total=len(lines))

        # Prepare empty output arrays
        all_features = np.empty((len(lines), configDict['input_sequence_max_length']), dtype=DATA_TYPE)
        all_labels = np.empty((len(lines), configDict['max_num_labels']), dtype=DATA_TYPE)
        for idx, line in enumerate(lines):
            pbar.update(1)
            # Last element is new line
            (labels, words) = parse_line(line[:-1])

            # Process labels
            (all_labels[idx,:], _) = labels2numbers(label2number, labels, 0, configDict['max_num_labels'])  # I don't count cut offs here

            # Process words
            (all_features[idx,:], numCutOffs) = words2numbers(word2number, words, numCutOffs, configDict['input_sequence_max_length'], isTraining, configDict['perc_unknown_tokens'])

        pbar.close()

    num_feature_points = all_features.shape[0] * all_features.shape[1]

    print('Transformed {} different words and {} labels to numbers'.format(len(word2number), len(label2number.keys())))
    print('Cut off {} percent of datapoints due to extending max length'.format(numCutOffs/len(lines)*100))
    print('Percentage of labels that never appear: {}'.format((1-float(len(np.unique(all_labels))/(np.max(all_labels)+1)))*100))
    print('Total number of labels: {}'.format(np.max(all_labels)))
    print('Percent zeros (empty data) in features: {}'.format((1-float(np.count_nonzero(all_features)) / num_feature_points)*100))

    total_num_unknown_tokens = float(np.sum(np.in1d(all_features.flatten(), UNKNOWN_VALUE)))
    print('Percent unknown tokens in features: {}'.format(total_num_unknown_tokens/ num_feature_points * 100))

    print('Dimensions of the data: {}, {}'.format(*all_features.shape))
    # Save all information as numpy matrices
    save_results(all_features, all_labels, os.path.join(preprocessed_dir, output_name))
    save_word2number(word2number, os.path.join(preprocessed_dir, WORD2NUMBER_MODEL))


# Read in all labels and predefine a list with numbers and their numbers
def load_label2Num(all_labels_path):
    label2num = {}
    with open(all_labels_path) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            label2num[line[:-1]] = idx
    return label2num


def labels2numbers(model, labels, numCutOffs, maxNumLabels):
    out = np.empty((maxNumLabels), dtype=DATA_TYPE)

    for idx, label in enumerate(labels):
        if idx >= maxNumLabels:
            numCutOffs += 1
            break
        out[idx] = getLabel2Number(model, label)

    if len(labels) < maxNumLabels:
        # Fill up the other labels with random existing ones
        for i in range(1, maxNumLabels - len(labels) + 1):
            out[-i] = random.choice(out[0:len(labels)])

    return out, numCutOffs


def getLabel2Number(model, label):
    if label in model.keys():
        return model[label]
    else:
        raise KeyError('"{}" is not a valid label'.format(label))


def words2numbers(model, words, numCutOffs, maxSequenceLength, isTraining, perc_unknown_tokens):
    out = np.full((maxSequenceLength), EMPTY_VALUE)
    for idx, word in enumerate(words):
        if idx >= maxSequenceLength:
            numCutOffs += 1
            break
        out[idx] = getWord2Number(model, word, isTraining, perc_unknown_tokens)
        # print('{} -> {}'.format(word, out[idx]))
    return out, numCutOffs


def getWord2Number(model, word, isTraining, perc_unknown_tokens):
    if word in model.keys():
        if isTraining:
            return model[word] if perc_unknown_tokens < random.uniform(0, 1) else UNKNOWN_VALUE
        else:
            return model[word]
    else:
        if isTraining:
            newNum = len(model) + 2  # Due to the predefined values
            model[word] = newNum
            return newNum
        else:
            return UNKNOWN_VALUE


def parse_line(line):
    l = line.split(';', 1)
    raw_labels = l[0].replace(u'\xa0', u' ')
    raw_words = l[1].replace(u'\xa0', u' ')

    labels = [w.strip() for w in raw_labels.split('|')]  # Remove whitespace around all words
    words = [w for w in raw_words.split(' ') if w!='<br>' and w!='']  # Filters out <br> and ''

    return (labels, words)


def get_file_paths():
    if(len(sys.argv) < 6):
        print('No dataset or output file specified.\nHow to use: "python data_to_number DATASET OUTPUT_NAME ALL_LABELS_PATH PREPROCESSED_DIR IS_TRAINING(TRUE|FALSE)"')
        exit()
    data_path = sys.argv[1]
    output_name = sys.argv[2]
    all_labels_path = sys.argv[3]
    preprocessed_dir = sys.argv[4]
    isTraining = True if sys.argv[5] == "TRUE" else False
    return data_path, all_labels_path, preprocessed_dir, output_name, isTraining


def save_results(features, labels, path):
    np.savez(path, features, labels)


def load_data(path):
    npzfile = np.load(path)
    features = npzfile['arr_0']
    labels = npzfile['arr_1']
    return features, labels


def load_word2number(path):
    try:
        model = pickle.load(open(path, 'rb'))
    except:
        model = dict()
    return model


def save_word2number(word2number, path):
    pickle.dump(word2number,open(path,'wb'))


if __name__ == '__main__':
    main()
