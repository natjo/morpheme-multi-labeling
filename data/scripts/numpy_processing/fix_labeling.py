import numpy as np
import my_excepthook


def main():




# Removes any non existing indices in between the max values of the given matrix
# changes the new array in place
def fix_labels(labels):
    unique_labels = set(np.unique(labels))

    print("Learn new mapping for labels")
    new_mapping = dict()
    max_new_val = 0
    for i in range(np.max(labels)+1):
        if i in unique_labels:
            new_mapping[i] = max_new_val
            max_new_val += 1

    # Apply new mapping
    print("Apply new mapping on labels")
    flat_for(labels, lambda el: new_mapping[el])
    return labels


def flat_for(a, f):
    a = a.reshape(-1)
    for i, v in enumerate(a):
        a[i] = f(v)



if __name__ == '__main__':
    main()
