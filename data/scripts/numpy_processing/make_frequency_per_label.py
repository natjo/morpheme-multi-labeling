import numpy as np
import os
import sys
import my_excepthook

def main():
    label_statistic_path, output_path = get_cli_params()

    label_weights = []
    # Iterate over all data points
    with open(label_statistic_path) as f:
        lines = f.readlines()
        print('Found {} labels in {}'.format(len(lines), label_statistic_path))
        for line in lines:
            label, num_appearences = parse_line(line[:-1])
            label_weights.append(1.0/num_appearences)

    output_file = os.path.join(output_path, 'label_weights')
    save_results(label_weights, output_file)
    print('Saved results to {}.npy'.format(output_file))



def parse_line(line):
    l = line.split(':', 1)

    label = l[0]
    num_appearences = int(l[1].strip())

    return label, num_appearences


def save_results(label_weights, path):
    np.save(path, label_weights)


def get_cli_params():
    if(len(sys.argv)<3):
        print('No dataset or output file specified.\nHow to use: "python {} LABEL_STATISTIC OUTPUT_PATH"'.format(sys.argv[0]))
        exit()

    label_statistic_path = sys.argv[1]
    output_path = sys.argv[2]

    return label_statistic_path, output_path

if __name__ == '__main__':
    main()
