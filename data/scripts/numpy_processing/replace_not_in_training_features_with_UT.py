import numpy as np
import sys
import my_excepthook


UNKNOWN_VALUE = 1


def main():
    test_path, train_path, output_path = get_cli_params()

    train_features, train_labels = load_data(train_path)
    test_features, test_labels = load_data(test_path)

    unique_train_features = set(np.unique(train_features))
    print("Number of unique features in the training set: {}".format(len(unique_train_features)))
    print("Number of unique features in the test set (before): {}".format(len(np.unique(test_features))))

    for i in range(test_features.shape[0]):
        for j in range(test_features.shape[1]):
            if test_features[i][j] not in unique_train_features:
                test_features[i][j] = UNKNOWN_VALUE

    print("Number of unique features in the test set (after): {}".format(len(np.unique(test_features))))
    # save_results(test_features, test_labels, output_path)


def save_results(features, labels, path):
    np.savez(path, features, labels)


def load_data(path):
    npzfile = np.load(path)
    features = npzfile['arr_0']
    labels = npzfile['arr_1']
    return features, labels


def get_cli_params():
    if(len(sys.argv)<4):
        print('No dataset or output file specified.\nHow to use: "python {} TEST_SET TRAINING_SET OUTPUT_FILE"'.format(sys.argv[0]))
        exit()

    test_path = sys.argv[1]
    train_path = sys.argv[2]
    output_path = sys.argv[3]

    return test_path, train_path, output_path


if __name__ == '__main__':
    main()
