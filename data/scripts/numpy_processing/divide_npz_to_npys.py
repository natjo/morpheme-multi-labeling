import numpy as np
import os
import sys

def main():
    if(len(sys.argv)<3):
        print('No dataset or output file specified.\nHow to use: "python {} PREPROCESSED_DATASET OUTPUT_DIR"'.format(sys.argv[0]))
        exit()

    path = sys.argv[1]
    out_dir = sys.argv[2]

    features, labels = load_data(path)

    np.save(os.path.join(out_dir, 'train_features.npy'), features)
    np.save(os.path.join(out_dir, 'train_labels.npy'), labels)



def load_data(path):
    npzfile = np.load(path)
    features = npzfile['arr_0']
    labels = npzfile['arr_1']
    return features, labels


if __name__ == '__main__':
    main()
