#! /bin/bash


# Trains or learns a model for bpe
# encoded file will be saved in the same directory as the input file
# Call it like this: ./bpe_action.sh learn-bpe|apply-bpe INPUT_FILE MODEL_PATH

SUBWORD_NMT=~/morpheme-multi-labeling/venv/bin/subword-nmt
ACTION=$1
FILE=$2
MODEL_PATH=$3

basePath=$(dirname "$FILE")
baseName=$(basename $FILE .txt)

MIN_FREQUENCY=5 #10 #2 #5
MAX_NUM_SYMBOLS=2000000


if [ "$ACTION" = "learn-bpe" ]; then
    $SUBWORD_NMT learn-bpe --min-frequency $MIN_FREQUENCY -s $MAX_NUM_SYMBOLS < $FILE > $MODEL_PATH
elif [ "$ACTION" = "apply-bpe" ]; then
    $SUBWORD_NMT apply-bpe -c $MODEL_PATH < $FILE > "${basePath}/${baseName}_encoded.txt"
else
    echo "Unknown action \""$ACTION\"
    echo "Actions are \"learn-bpe\" and \"apply-bpe\""
fi
