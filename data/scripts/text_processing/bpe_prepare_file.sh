#! /bin/bash

BUILD_PATH="build_files"

if [ $# -lt 2 ]
  then
    echo "Usage: '$0 INPUT_FILE PREPROCESSED_FOLDER"
    exit
fi


input_file=$1
preprocessed_path=$2
baseName=$(basename $input_file .txt)
script_dir=$(dirname "$0")
build_path="${preprocessed_path}/${BUILD_PATH}"


mkdir -p $build_path


echo "remove labels of ${baseName}"
/usr/bin/perl "${script_dir}/remove_labels.pl" $input_file "${build_path}/${baseName}_no_labels.txt"

echo "encode ${baseName} with bpe"
# Output is saved in same directory as input
/bin/bash "${script_dir}/bpe_action.sh" apply-bpe "${build_path}/${baseName}_no_labels.txt" "${preprocessed_path}/learned_bpe.txt"

echo "concat labels and text of ${baseName} again"
/usr/bin/perl "${script_dir}/concat_labels_text.pl" $input_file "${build_path}/${baseName}_no_labels_encoded.txt" "${build_path}/${baseName}_final.txt"
