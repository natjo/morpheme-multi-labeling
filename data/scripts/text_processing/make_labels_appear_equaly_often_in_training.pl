#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(shuffle sum min);
use Array::Utils qw(intersect);
use IO::Tee;


# This will repeat datapoints with labels that appear seldom in the TRAINING_INPUT
# It will only repeat labels that also appear in the TEST_INPUT

if (scalar @ARGV < 3) {
    print "inputfile missing\nusage: $0 TRAINING_INPUT TEST_INPUT OUTPUT_FILE [LOG_FILE]\n";
    exit;
}

my $logFile = 'log/make_equal.log';
if (scalar @ARGV == 4) {
    $logFile = $ARGV[3];
}

open my $log, '>'.$logFile or die "Can't write logfile: ".$logFile;
my $tee = new IO::Tee(\*STDOUT, $log);
say $tee 'Start at '.localtime().' with parameters '.@ARGV[0].', '.@ARGV[1].', '.@ARGV[2];

my $labelInputFile = @ARGV[0];
my $testInputFile = $ARGV[1];
my $outputFile = $ARGV[2];

my $appearencesPerKeyword = 2000; # was 1000
my $loopsUntilExpDoubles = 15; # was 20
my $minGrowth = 0.005; # was 0.005


# Load all lines
my @allLinesTraining = readInAllLines($labelInputFile);
say $tee "Loaded ".scalar @allLinesTraining." datapoints from training";
my @allLinesTest = readInAllLines($testInputFile);
say $tee "Loaded ".scalar @allLinesTest." datapoints from training";


# @allLinesTraining = shuffle @allLinesTraining;
# @allLinesTest = shuffle @allLinesTest;
# @allLinesTraining = @allLinesTraining[0 .. 10000];
# @allLinesTest = @allLinesTest[0 .. 2000];


# Create the statistics - label count
my %keywordCount = countKeywords(@allLinesTest);
say $tee "Identified ".scalar(keys %keywordCount)." unique labels in test";

# Calculate the frequency of each keyword
my %keywordFrequencies = map { $_ => sqrt(1/$keywordCount{$_}) }  keys %keywordCount;


# Go through the dataset, add each line if one of the labels didn't appear often enough in the training set yet
# Repeat this until enough labelpoints of all datasets are in the training data
my @trainingData;
my $loopCounter = 0;
my $prevSizeTrainingData = 0;
# Copy keywords from testset, set all counts to 0
my %numLabelsInTraining = map{ $_ => 0 } keys %keywordCount;
# This is only needed to check which labels are in training
my %checkLabelsInTraining = map{ $_ => 1 } keys %keywordCount;
my @labelsInTest = keys %numLabelsInTraining;



# Run while there is at least one element appearing too seldom on the new training
while (grep {$appearencesPerKeyword > $_} values %numLabelsInTraining){
    # Shuffle test data, so I don't always get the same datapoints at first
    # @allLinesTraining = shuffle @allLinesTraining;

    $loopCounter += 1;
    my $currSizeTrainingData = scalar @trainingData + 1;
    my $currentGrowth = ($currSizeTrainingData - $prevSizeTrainingData) / $currSizeTrainingData;
    say $tee 'Number of elements currently in new dataset: '.$currSizeTrainingData. ' (+ '.$currentGrowth.')';
    if ($currentGrowth < $minGrowth){
        say $tee 'Growth got too small';
        last;
    }
    $prevSizeTrainingData = $currSizeTrainingData;

    say $tee 'Start iterating through the training set for the '.$loopCounter.'. time...';
    $tee->flush;
    foreach my $line (@allLinesTraining){
        my @keywordsList = getKeywords($line);

        # Check that at least one label is in the test set
        if(intersect(@keywordsList, @labelsInTest)){
            # Filter out labels that are not relevant and get only their counts
            @keywordsList = grep {exists($checkLabelsInTraining{$_})} @keywordsList;
            my @numLabelsRelevant = map {$numLabelsInTraining{$_} } @keywordsList;

            # if(isDataPointRelevantFreq($loopCounter, \@keywordsList, \%numLabelsInTraining, \%keywordFrequencies)){
            if(isDataPointRelevantExp($loopCounter, @numLabelsRelevant)){
            # if(isDataPointRelevantBasic($loopCounter, @numLabelsRelevant)){
                # Add label to trainingset
                push @trainingData, $line;

                # Increment counter of all newly added labels
                foreach my $x (@keywordsList){
                    $numLabelsInTraining{$x} += 1;
                }
            }
        }
    }
}

say $tee '----------------------------------------------';
say $tee 'Original training set was '.scalar @allLinesTraining;
say $tee 'New training set is '.scalar @trainingData;



# # Shuffle the training data to mix the infrequent labels in the beginning with the newly added
@trainingData = shuffle @trainingData;


say $tee "Print new training data into ".$outputFile;
open OUTPUT_TRAINING_FILE, '>>', $outputFile || die "Could not open $outputFile";
foreach my $dataPoint (@trainingData){
    print OUTPUT_TRAINING_FILE $dataPoint;
}


say $tee 'Finished at '.localtime();


sub readInAllLines{
    my ($inputFile) = @_;
    my @allLines;
    open INPUTFILE, $inputFile || die "Could not open $inputFile\n";;
    while(my $line = <INPUTFILE>){
        push @allLines, $line;
    }
    return @allLines;
}


# Prints a dictionary with each label and its fraction compared to all labels
# Expects a dictionary with all labels and their total number of occurences as input
# relevantLabels is a list with all labels that should be counted individually. The rest will be collected in misc
# It returns the standard deviation
sub printLabelRatio{
    my ($keywordList, $relevantLabels) = (@_);

    my $totalNumKeywords = sum values %$keywordList;
    my %percLabels = map{ $_ => $keywordList->{$_} / $totalNumKeywords } keys %$keywordList;
    my @sortedRelevantLabels = sort { $percLabels{$b} <=> $percLabels{$a} } @$relevantLabels;


    # Print sorted labels by frequency
    my $cnt = 0; # Cound sum of percentages of relevant labels
    foreach my $label (@sortedRelevantLabels) {
        printf $tee "%-60s %s\n", $label, $percLabels{$label};
        $cnt += $percLabels{$label};
    }

    printf $tee "%-60s %s\n", '-------------------', '-------------------';
    printf $tee "%-60s %s\n", 'labels_not_in_train', 1 - $cnt;

    my $stdDev = std_dev(map { $percLabels{$_} } @sortedRelevantLabels);
    say $tee 'Standard deviation: '.$stdDev;
    return $stdDev;
}



sub countKeywords{
    my (@allLines) = @_;
    my %keywordCount;
    foreach my $line (@allLines){
        my @keywordsList = getKeywords($line);

        # Add counts of all found keywords
        foreach my $x (@keywordsList){
            $keywordCount{$x} += 1;
        }
    }
    return %keywordCount;
}


sub getKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line)[0];

    # Transform string into list of keywords
    my @keywordsList = split(/\|/, $keywordsString);
    @keywordsList = @keywordsList[0..min(4,$#keywordsList)];

    # Remove any whitespace around the keywords
    @keywordsList = grep(s/\s*$//g, @keywordsList);
    return @keywordsList;
}


# Return true or false if the datapoint should be added to the final list
sub isDataPointRelevantFreq {
    my ($loopCount, $keywordsList_ref, $numLabelsInNewData_ref, $keywordFrequencies_ref) = @_;

    my $sum = 0;
    foreach my $label (@{ $keywordsList_ref }){
        my $val = $appearencesPerKeyword - $numLabelsInNewData_ref->{$label};
        if($val > 0){
            # There are not enough data points of this label in the dataset yet
            $val = ($val+1) ** (2 * ($loopCount + $loopsUntilExpDoubles)/$loopsUntilExpDoubles);
        }else{
            # There are already enough data points of this label in the dataset
            $val = -1 * ($val ** 2)
        }
        $sum += $val * $keywordFrequencies_ref->{$label};
    }

    return $sum > 0 ? 1 : 0;
}


# Return true or false if the datapoint should be added to the final list
sub isDataPointRelevantExp {
    my ($loopCount, @numLabelsInNewData) = @_;

    my $sum = 0;
    foreach my $num (@numLabelsInNewData){
        my $val = $appearencesPerKeyword - $num;
        if($val > 0){
            # There are not enough data points of this label in the dataset yet
            $val = ($val+1) ** (2 * ($loopCount + $loopsUntilExpDoubles)/$loopsUntilExpDoubles);
        }else{
            # There are already enough data points of this label in the dataset
            $val = -1 * ($val ** 2)
        }
        $sum += $val;
    }

    return $sum > 0 ? 1 : 0;
}


# Return true or false if the datapoint should be added to the final list
sub isDataPointRelevantBasic {
    my ($loopCount, @numLabelsInNewData) = @_;

    if(grep{ $appearencesPerKeyword > $_ } values @numLabelsInNewData){
        return 1;
    }
    return 0;
}


# From http://andrewstechhints.blogspot.com/2010/02/standard-deviation-in-perl.html
sub average {
    my (@values) = @_;

    my $count = scalar @values;
    my $total = 0;
    $total += $_ for @values;

    return $count ? $total / $count : 0;
}

sub std_dev {
    my (@values) = @_;

    my $average = average(@values);
    my $count = scalar @values;
    my $std_dev_sum = 0;
    $std_dev_sum += ($_ - $average) ** 2 for @values;

    return $count ? sqrt($std_dev_sum / $count) : 0;
}
