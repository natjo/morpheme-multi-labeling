#! /bin/bash


# Trains or learns a model for mofessor
# Call it like this: ./run_mofessor.sh train|test INPUT_FILE MODEL_PATH

ACTION=$1
FILE=$2
MODEL_PATH=$3

MORFESSOR=~/morpheme-multi-labeling/venv/bin/morfessor

if [ "$ACTION" == "train-readable" ]; then
    $MORFESSOR -t $FILE -S $MODEL_PATH/model.txt
elif [ "$ACTION" == "train" ]; then
    $MORFESSOR -t $FILE -s $MODEL_PATH/model.bin
elif [ "$ACTION" == "test" ]; then
    $MORFESSOR -T $FILE -l $MODEL_PATH/model.bin
elif [ "$ACTION" == "test-readable" ]; then
    $MORFESSOR -T $FILE -L $MODEL_PATH/model.txt
else
    echo "Unknown action \""$ACTION\"
    echo "Actions are \"train-readable\", \"train\", \"test-readable\" and \"test\""
fi
