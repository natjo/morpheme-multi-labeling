#! /usr/bin/perl
require 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(reduce);


# Needs a dataset file in tills original format as input, prints statistics in std::out stream

my $inputFile = $ARGV[0];


my %keywordCount;
my $numDataPoints = 0;

# Create the statistics - label count
# Count all keywords to decide if they can be in the test set
open INPUTFILE, $inputFile || die "Could not open $inputFile\n";;
while(my $line = <INPUTFILE>){
    $numDataPoints++;
    my @keywordsList = getKeywords($line);

    # Add counts of all found keywords
    foreach my $x (@keywordsList){
        $keywordCount{$x} += 1;
    }
}

# Output
# Print number of appeareances of each label
foreach my $key (sort {$keywordCount{$b} <=> $keywordCount{$a}} keys %keywordCount){
    print "$key: $keywordCount{$key}\n";
}
print "--------------------\n";

# Print average number of labels per data point
my $numTotalLabels = reduce { $a + $b } values %keywordCount;
print "Average number of labels per datapoint: ", $numTotalLabels / $numDataPoints, "\n";
print "Total number of datapoints: $numDataPoints\n";
print "Total number of different labels ", scalar keys %keywordCount, "\n";





sub getKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line)[0];
    $keywordsString =~ s/\s+//;

    # Transform string into list of keywords
    my @keywordsList = split /\|/, $keywordsString;
    return @keywordsList;
}

