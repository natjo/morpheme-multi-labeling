#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(reduce min);
use File::Spec::Functions 'catfile';


# Needs a dataset file in tills original format as input, prints all labels in given directory
# It will only print labels that appear in the first 5 mentions

if (scalar @ARGV < 2) {
    print "inputfile missing\nusage: $0 INPUT_FILE OUTPUT_DIRECTORY\n";
    exit;
}

my $inputFile = $ARGV[0];
my $outDir = $ARGV[1];
my $outputAllLabels = catfile($outDir, "all_labels_data.txt");


my %keywordCount;
my $numDataPoints = 0;

# Create the statistics - label count
# Count all keywords to decide if they can be in the test set
open INPUTFILE, $inputFile || die "Could not open $inputFile\n";;
while(my $line = <INPUTFILE>){
    $numDataPoints++;
    my @keywordsList = getKeywords($line);

    # Add counts of all found keywords
    foreach my $x (@keywordsList){
        $keywordCount{$x} += 1;
    }
}

unlink $outputAllLabels;

# Output
# Print number of appeareances of each label
say "Print all labels into ".$outputAllLabels;
open OUTPUT_ALL_LABELS, '>>', $outputAllLabels || die "Could not open $outputAllLabels";
foreach my $key (sort {$keywordCount{$b} <=> $keywordCount{$a}} keys %keywordCount){
    print OUTPUT_ALL_LABELS "$key\n"
}


sub getKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line)[0];
    # $keywordsString =~ s/\s+//;

    # Transform string into list of keywords
    my @keywordsList = split /\|/, $keywordsString;
    @keywordsList = @keywordsList[0..min(4,$#keywordsList)];
    @keywordsList = grep(s/\s*$//g, @keywordsList);
    return @keywordsList;
}

