#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(reduce);

# Remove all labels but keep the lines, so later on they can be united again

if (scalar @ARGV < 2) {
    say "parameter missing\nusage: $0 INPUT_FILE_LABELS INPUT_FILE_TEXT OUTPUT_FILE";
    exit;
}

my $inputFileLabels = $ARGV[0];
my $inputFileText = $ARGV[1];
my $outputFile = $ARGV[2];



open OUTPUT_FILE, '>>', $outputFile || die "Could not open $outputFile";
open INPUT_FILE_LABELS, $inputFileLabels || die "Could not open $inputFileLabels\n";;
open INPUT_FILE_TEXT, $inputFileText || die "Could not open $inputFileText\n";;
while(my $lineLabels = <INPUT_FILE_LABELS>){
    $lineLabels = getKeywords($lineLabels);
    my $lineText = <INPUT_FILE_TEXT>;

    print OUTPUT_FILE $lineLabels.';'.$lineText;
}


sub getKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line, 2)[0];
    return $keywordsString;
}

