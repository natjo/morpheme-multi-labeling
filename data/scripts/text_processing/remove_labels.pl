#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(reduce);

# Remove all labels but keep the lines, so later on they can be united again

if (scalar @ARGV < 2) {
    say "parameter missing\nusage: $0 INPUT_FILE OUTPUT_FILE";
    exit;
}

my $inputFile = $ARGV[0];
my $outputFile = $ARGV[1];



open OUTPUT_FILE, '>>', $outputFile || die "Could not open $outputFile";
open INPUT_FILE, $inputFile || die "Could not open $inputFile\n";;
while(my $line = <INPUT_FILE>){
    my $newLine = removeKeywords($line);
    print OUTPUT_FILE $newLine;
}


sub removeKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line, 2)[1];
    return $keywordsString;
}

