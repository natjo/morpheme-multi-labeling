#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;


use File::Spec::Functions 'catfile';
use Data::Dumper;

# Produces a dataset file similar in the structure to the mercateo dataset
# Call like this:
# $ ./create_dataset_file.pl data_folder output.txt


my $data_folder = $ARGV[0];
my $labelsFile = catfile($data_folder, 'bookmark_tags.dat');
my $featuresFile = catfile($data_folder, 'bookmarks.dat');
my $labels2wordsFile = catfile($data_folder, 'tags.dat');
my $outputFile = $ARGV[1];


my %labels2words;
my @datapoints;


# Create dictionary to transform numbers to words of labels
open LABELS2WORDS_FILE, $labels2wordsFile || die "Could not open $labels2wordsFile\n";;
my $header = <LABELS2WORDS_FILE>;
while(my $line = <LABELS2WORDS_FILE>){
    my ($index, $label) = splitIndexWord($line);
    $labels2words{$index} = $label;
}
# print Dumper(\%labels2words);

my $currentTagID = "1";
my @currentLabels;
open INPUT_LABELS_FILE, $labelsFile || die "Could not open $labelsFile\n";;
$header = <INPUT_LABELS_FILE>;
open INPUT_FEATURES_FILE, $featuresFile || die "Could not open $featuresFile\n";;
$header = <INPUT_FEATURES_FILE>;

while(my $labelsLine = <INPUT_LABELS_FILE>){
    my ($dataID, $labelID) = split_dataID_labelID($labelsLine);
    if($dataID == $currentTagID){
        # Collect all tags
        push @currentLabels, $labels2words{$labelID};
    }else{
        # All tags found, unite labels and features into one string
        my $labelString = join '|', @currentLabels;
        my $line = <INPUT_FEATURES_FILE>;
        my $featureString = split_feature($line);
        push @datapoints, $labelString."; ".$featureString;

        # Reset everything for the next datapoint
        $currentTagID = $dataID;
        @currentLabels = $labels2words{$labelID};
    }
}

# Add the last label
my $labelString = join '|', @currentLabels;
my $line = <INPUT_FEATURES_FILE>;
my $featureString = split_feature($line);
push @datapoints, $labelString."; ".$featureString;

# print Dumper(@datapoints);

# Print the result into the specified file
open OUTPUT_FILE, '>', $outputFile || die "Could not open $outputFile";
foreach my $dataPoint (@datapoints){
    print OUTPUT_FILE $dataPoint."\n";
}

# process index and word from label file to create the dictionary
sub splitIndexWord {
    my ($line) = @_;
    $line =~ s/\r|\n//g;
    return split '\t', $line, 2;
}

sub split_dataID_labelID {
    my ($line) = @_;
    $line =~ s/\r|\n//g;
    my ($dataID, $labelID) = (split '\t', $line)[0,1];
    return $dataID, $labelID;
}

sub split_feature {
    my ($line) = @_;
    $line =~ s/\r|\n//g; # remove new line
    # index 2 is the description, index 3 is the website
    my @features = (split '\t', $line)[2,3];
    $features[1] =~ s/http:\/\/www\.//;  # Remove http://www. since this is contained in all addresses
    $features[1] =~ s/\/$//;  # Remove trailing / since this is also contained in all addresses;
    return join ' ; ', @features;
}
