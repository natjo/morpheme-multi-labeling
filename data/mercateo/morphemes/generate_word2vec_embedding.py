import numpy as np
from gensim.models import Word2Vec
import my_excepthook
import pickle


"""
This function uses the word2vec model and creates an embedding file containing the word2vec embedding of all morphemes.
"""


WORD2VEC_MODEL = 'data/mercateo/morphemes/preprocessed/word2vec.model'
MORPHEME2NUMBER_MODEL = 'data/mercateo/morphemes/preprocessed/morpheme2number.pcl'

OUTPUT_EMBEDDING_MATRIX_FILE = 'data/mercateo/morphemes/preprocessed/embedding_matrix.npy'

DATA_TYPE = np.float32

def main():
    word2vec_model = load_word2vec_model(WORD2VEC_MODEL)
    allMorphemes = load_morpheme2number(MORPHEME2NUMBER_MODEL)

    embedding_dim = len(word2vec_model.wv[list(allMorphemes.keys())[0]])
    vocab_size = len(allMorphemes) + 2  # for unknown value and for empty value

    # The first two vectors will be random, theses are the ones for unkwown and empty value
    embedding_matrix = np.zeros((vocab_size, embedding_dim), dtype=DATA_TYPE)
    for idx, morpheme in enumerate(allMorphemes):
        idx += 2  # for unkown value and for empty value
        embedding_matrix[idx,:] = word2vec_model.wv[morpheme]

    print('Save {}x{} word2vec embedding matrix'.format(vocab_size, embedding_dim))
    save_results(embedding_matrix)


def load_word2vec_model(model_path):
    return Word2Vec.load(model_path)


# Returns a list of morphemes. The indexes of the list correspond to its encoded number
def load_morpheme2number(modelPath):
    return pickle.load(open(modelPath, 'rb'))


def save_results(embedding_matrix):
    np.save(OUTPUT_EMBEDDING_MATRIX_FILE, embedding_matrix)


if __name__ == '__main__':
    main()
