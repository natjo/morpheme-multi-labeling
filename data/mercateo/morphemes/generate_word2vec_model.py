from gensim.models import Word2Vec
import morfessor
import config
from tqdm import tqdm

"""
Generate the word2vec model based on the morfessor training data
"""

INPUT_TEXT_PATH = 'data/mercateo/general_data/morfessor_training_data.txt'
MORFESSOR_MODEL = 'data/mercateo/morphemes/preprocessed/model.txt'
WORD2VEC_MODEL = 'data/mercateo/morphemes/preprocessed/word2vec.model'


def main():
    print('Load morfessor model from {}'.format(MORFESSOR_MODEL))
    morfessor_model = get_morfessor_model()

    print('Parse text into training array for word2vec model')
    morphemes = get_morphemes(morfessor_model, INPUT_TEXT_PATH)

    # Train the model and save it
    print('Train word2vec model')
    word2vec_model = Word2Vec(morphemes, size=config.EMBEDDING_DIM, window=config.WORD2VEC_WINDOW, min_count=1, workers=4)
    print('Trained the model successfully using {} training morphemes on a vocabulary of {} morphemes'
            .format(sum([len(m) for m in morphemes]), len(word2vec_model.wv.vocab)))
    word2vec_model.save(WORD2VEC_MODEL)
    # print(word2vec_model.wv.vocab.keys())
    # word2vec_model = Word2Vec.load(WORD2VEC_MODEL)

    # Just some tests
    # print(word2vec_model.wv[morphemes[0][0]])
    # print(word2vec_model.wv.vocab.keys())


# transform a string into a list of sentences
def getSentences(line):
    # Each line consists of multiple sentences divided by <br>
    sentences = line[:-1].split('<br>') # Last character is the new line symbol
    sentences = [s.replace(u'\xa0', u' ') for s in sentences]
    return sentences


def get_morfessor_model():
    io = morfessor.MorfessorIO()
    return io.read_any_model(MORFESSOR_MODEL)


# Transform each sentence into a list of morphemes. Returns a list of lists of morphemes
# input is a loaded morfessor model and the path to the file it should transform into morphemes
def get_morphemes(morfessorModel, file_name):
    allMorphemes = []
    with open(file_name) as f:
        lines = f.readlines()
        pbar = tqdm(total=len(lines))
        for line in lines:
            pbar.update(1)
            for s in getSentences(line):
                if s != ' ':
                    words = s.split()
                    # print(words)
                    morphemes = [morfessorModel.viterbi_segment(word)[0] for word in words]
                    # print('-> {}'.format(morphemes))
                    # morphemes is a list of lists, make it to a simple list of all elements
                    flat_morphemes = [item for sublist in morphemes for item in sublist]
                    allMorphemes.append(flat_morphemes)
    pbar.close()
    return allMorphemes



if __name__ == '__main__':
    main()
