import my_excepthook
import numpy as np
import morfessor
from tqdm import tqdm
from gensim.models import Word2Vec
import config
import sys


"""
Not in use anymore.
"""

ALL_LABELS = 'data/mercateo/general_data/all_labels_data.txt'
MORFESSOR_MODEL = 'data/mercateo/morphemes/preprocessed/model.txt'
WORD2VEC_MODEL = 'data/mercateo/morphemes/preprocessed/word2vec.model'

DATA_TYPE = np.float32

def main():
    data_path, output_path = get_file_paths()

    # Load look up functionality
    label2num = load_label2Num_dict(ALL_LABELS)
    morfessor_model = load_morfessor_model(MORFESSOR_MODEL)
    word2vec_model = load_word2vec_model(WORD2VEC_MODEL)

    num_morphemes = len(word2vec_model.wv.vocab)
    num_labels = len(label2num.keys())
    print('Create numpy encoding for a vocabulary of {} and {} labels'.format(num_morphemes, num_labels))

    # Iterate over all data points
    with open(data_path) as f:
        lines = f.readlines()
        print('Found {} datapoints in {}'.format(len(lines), data_path))
        pbar = tqdm(total=len(lines))
        all_labels = np.empty((len(lines), num_labels), dtype=DATA_TYPE)
        all_features = np.empty((len(lines), config.INPUT_SEQUENCE_MAX_LENGTH, config.EMBEDDING_DIM), dtype=DATA_TYPE)
        for idx, line in enumerate(lines):
            pbar.update(1)
            # Last element is new line
            (labels, words) = parse_line(line[:-1])

            # Process labels
            labelNumbers = [get_label_repr(label2num, l) for l in labels]
            all_labels[idx,:] = label_num_to_multi_hot(labelNumbers, num_labels)

            # Process words
            morphemes = [get_morpheme(morfessor_model, w) for w in words]  # Is a list of lists
            morphemes = [item for sublist in morphemes for item in sublist]
            all_features[idx,:,:] = morphemes2numbers(word2vec_model, morphemes)

        pbar.close()
    # Save all information as numpy matrices
    save_results(all_features, all_labels, output_path)


def parse_line(line):
    # print(line)
    l = line.split(';', 1)
    raw_labels = l[0].replace(u'\xa0', u' ')
    raw_words = l[1].replace(u'\xa0', u' ')

    labels = raw_labels.split('|')
    words = [w for w in raw_words.split(' ') if w!='<br>' and w!='']

    return (labels, words)


# Converts a list of labels(numbers) to the equivalent multi hot encoded representation
def label_num_to_multi_hot(labels, num_labels):
    encoding = np.zeros(num_labels, dtype=DATA_TYPE)
    for l in labels:
        encoding[l] = 1
    return encoding


def morphemes2numbers(model, morphemes):
    features = np.zeros((config.INPUT_SEQUENCE_MAX_LENGTH, config.EMBEDDING_DIM), dtype=DATA_TYPE)
    for idx, morpheme in enumerate(morphemes):
        if idx >= config.INPUT_SEQUENCE_MAX_LENGTH:
            break
        features[idx, :] = get_word2vec_repr(model, morpheme)
    return features


def load_label2Num_dict(all_labels_path):
    label2num = {}
    with open(all_labels_path) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            # print('{}: {}'.format(line[:-1], idx))
            label2num[line[:-1]] = idx

    return label2num


def load_morfessor_model(model_path):
    io = morfessor.MorfessorIO()
    return io.read_any_model(model_path)


def load_word2vec_model(model_path):
    return Word2Vec.load(model_path)


def get_morpheme(model, word):
    return model.viterbi_segment(word)[0]


def get_word2vec_repr(model, word):
    if word in model.wv.vocab.keys():
        return model.wv[word]
    else:
        raise KeyError('"{}" is not a learned morpheme'.format(word))


def get_label_repr(model, word):
    if word in model.keys():
        return model[word]
    else:
        raise KeyError('"{}" is not a valid label'.format(word))


def save_results(features, labels, path):
    np.savez(path, features, labels)


def load_data(path):
    npzfile = np.load(path)
    features = npzfile['arr_0']
    labels = npzfile['arr_1']
    return features, labels


def get_file_paths():
    if(len(sys.argv)<3):
        print('No dataset or output file specified.\nHow to use: "python data_to_number DATASET OUTPUT_FILE"')
        exit()
    data_path = sys.argv[1]
    output_path = sys.argv[2]
    return data_path, output_path


if __name__ == '__main__':
    main()
