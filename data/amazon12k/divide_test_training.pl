#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use List::Util qw(reduce shuffle min);
use File::Basename;
use File::Spec::Functions 'catfile';


# Process the intial data into test and training file and clean each
# 1. Calculate frequency of labels
# 2. Put labels with low frequency into training set
# 3. Divide the rest with a given ratio into test and training
# 4. Clean the data

#TODO Remove empty input lines in the data, they cause errors

if (scalar @ARGV < 1) {
    print "inputfile missing\nusage: $0 INPUT_FILE1 INPUT_FILE2 ... OUTPUT_DIRECTORY\n";
    exit;
}

my @inputFiles = @ARGV[0];
my $outDir = $ARGV[1];



my $minNumberLabels = 20;
my $ratioTestToTraining = 0.2;
my $numEvalDatapoints = 200;

# Calculate all output files dynamically
my $outputStatistics = catfile($outDir, "statistics.txt");
my $outputTraining = catfile($outDir, "training_data.txt");
my $outputTest = catfile($outDir, "test_data.txt");
my $outputEvalData = catfile($outDir, "eval_data.txt");
my $outputMorphemes = catfile($outDir, "morfessor_training_data.txt");

my @trainingData;
my @testData;


# During read in, the description gets removed
my @allLines = readInAllLines(@inputFiles);
say "Loaded ".scalar(@allLines)." datapoints from ".scalar(@inputFiles)." files";


# Create the statistics - label count
# Count all keywords to decide if they can be in the test set
my %keywordCount = countKeywords(@allLines);
say "Identified ".scalar(keys %keywordCount)." unique labels";


# Keep track which labels are already in the training set
my %labelsInTraining = map { $_ => 0} keys %keywordCount;

# Separate all datapoints that are not valid for testing
# Put the rest into test data for now
foreach my $line (@allLines){
    my @keywordsList = getKeywords($line);
    my $minValLabel = reduce {$keywordCount{$a} < $keywordCount{$b} ? $a : $b } @keywordsList;
    my $occurences = reduce {$a * $b} map { $labelsInTraining{$_} } @keywordsList;  # 0 if any element was never in the training set
    # Check how often a label occurs in total and if all labels of the datapoint are already in the training set
    if($keywordCount{$minValLabel} < $minNumberLabels || $occurences < scalar @keywordsList){
        # Needs to go into training set
        push @trainingData, $line;
        map {$labelsInTraining{$_}+=1} @keywordsList;
    }else{
        push @testData, $line;
    }
}


# Test if separation worked
my $occurences = reduce {$a * $b} map { $labelsInTraining{$_} } keys %labelsInTraining;
if ($occurences == 0){
    say "An error occurred: not all label appear in the training set";
}

# Shuffle test data, so I can always move the last element to the training data
@testData = shuffle @testData;

# Move last datapoint from testData to trainingData until the ratio is right
while((scalar @allLines * $ratioTestToTraining) < scalar @testData ){
    push(@trainingData, pop(@testData));
}

say "Created testData with ".scalar(@testData)." datapoints and trainingData with ".scalar(@trainingData)." datapoints";

# Shuffle the training data to mix the infrequent labels in the beginning with the newly added
@trainingData = shuffle @trainingData;


## Output in all files

# Clean first
unlink $outputStatistics;
unlink $outputTraining;
unlink $outputTest;
unlink $outputMorphemes;

# Print everything
say "Print statistics into ".$outputStatistics;
open OUTPUT_STATISTIC_FILE, '>>', $outputStatistics || die "Could not open $outputStatistics";
foreach my $key (sort {$keywordCount{$b} <=> $keywordCount{$a}} keys %keywordCount){
    print OUTPUT_STATISTIC_FILE "$key: $keywordCount{$key}\n";
}

say "Print test data into ".$outputTest;
open OUTPUT_TEST_FILE, '>>', $outputTest || die "Could not open $outputTest";
foreach my $dataPoint (@testData){
    my($labels, $text) = divideLabelsText($dataPoint);
    print OUTPUT_TEST_FILE $labels.' ; '.cleanLine($text);
}

say "Print training data into ".$outputTraining;
open OUTPUT_TRAINING_FILE, '>>', $outputTraining || die "Could not open $outputTraining";
foreach my $dataPoint (@trainingData){
    my($labels, $text) = divideLabelsText($dataPoint);
    print OUTPUT_TRAINING_FILE $labels.' ; '.cleanLine($text);
}

say "Print eval data into ".$outputEvalData;
# Shuffled list of indexes
my @shuffled_indexes = shuffle(0..$#testData);
# Get just N of them.
my @pick_indexes = @shuffled_indexes[ 0 .. $numEvalDatapoints - 1 ];
# Pick cards from @deck
my @evalDatapoints = @testData[ @pick_indexes ];
open OUTPUT_EVAL_FILE, '>>', $outputEvalData || die "Could not open $outputEvalData";
foreach my $dataPoint (@evalDatapoints){
    my($labels, $text) = divideLabelsText($dataPoint);
    print OUTPUT_EVAL_FILE $labels.' ; '.cleanLine($text);
}

say "Print morpheme training data into ".$outputMorphemes;
open OUTPUT_MORPHEME_FILE, '>>', $outputMorphemes || die "Could not open $outputMorphemes";
foreach my $line (@allLines){
    print OUTPUT_MORPHEME_FILE cleanForMorphemeTraining($line);
}


# Remove the last description during read in
sub readInAllLines{
    my (@inputFiles) = @_;
    my @allLines;
    foreach my $file (@inputFiles){
        open INPUTFILE, $file || die "Could not open $file\n";;
        while(my $line = <INPUTFILE>){
            #$line =~ s/;[^;]*$/\n/; # Removes the description
            push @allLines, $line;
        }
        close INPUTFILE;
    }
    return @allLines;
}


sub countKeywords{
    my (@allLines) = @_;
    my %keywordCount;
    foreach my $line (@allLines){
        my @keywordsList = getKeywords($line);

        # Add counts of all found keywords
        foreach my $x (@keywordsList){
            $keywordCount{$x} += 1;
        }
    }
    return %keywordCount;
}


sub getKeywords {
    my ($line) = @_;

    # Extract all keywords
    my $keywordsString = (split /;/, $line)[0];

    # Transform string into list of keywords
    my @keywordsList = split(/\|/, $keywordsString);
    @keywordsList = @keywordsList[0..min(4,$#keywordsList)];

    # Remove any whitespace around the keywords
    @keywordsList = grep(s/\s*$//g, @keywordsList);
    return @keywordsList;
}

sub cleanLine {
    my ($line) = @_;
    $line =~ s/\.!?/ $&<br> /g;      # add whitespace and newline to sentence ends
    $line =~ s/<br>/ $& /g;          # surround newlines with whitespace
    $line =~ s/<[pb\/]*>/ /g;        # remove things like <b>
    $line =~ s/[,;:\/()\"]/ $& /g;   # Surround special characters with whitespace
    # $line =~ s/.*?;//;               # Remove first part until ;
    $line =~ s/ +/ /g;               # Multi white space to one
    $line =~ s/^\s+//g;              # Remove white space from the left side of each line
    return $line;
}

sub divideLabelsText {
    my ($line) = @_;
    return split /;/, $line, 2;
}

sub cleanForMorphemeTraining {
    my ($line) = @_;
    $line =~ s/.*?;//;        # Remove first part until ;
    $line =~ s/<br>/ /g;      # remove line breaks <br>
    $line = cleanLine($line);
    return $line;
}
