#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;


use File::Spec::Functions 'catfile';
use Data::Dumper;

# Produces a dataset file similar in the structure to the mercateo dataset
# Call like this:
# $ ./create_dataset_file.pl amazon_data_dir output.txt


my $data_folder = $ARGV[0];
my $titlesFile = catfile($data_folder, 'titles.txt');
my $descriptionFile = catfile($data_folder, 'descriptions.txt');
my $labelsFile = catfile($data_folder, 'categories.txt');
my $outputFile = $ARGV[1];


my %id2labels;
my %id2title;
my %id2description;
my @datapoints;


## Load all information into hashmaps
say "Read in titles from $titlesFile";
open INPUT_FILE, $titlesFile || die "Could not open $titlesFile\n";;
while(my $line = <INPUT_FILE>){
    my ($id, $title) = split_titles($line);
    $id2title{$id} = $title;
}

# print Dumper(\%id2title);


say "Read in descriptions from $descriptionFile";
open INPUT_FILE, $descriptionFile || die "Could not open $descriptionFile\n";;
while(! eof INPUT_FILE){
    # get id
    my $line = <INPUT_FILE>;
    my $id = get_id_from_feature_file($line);
    # get description
    $line = <INPUT_FILE>;
    my $description = get_description_from_feature_file($line);
    # Ignore empty line
    $line = <INPUT_FILE>;
    $id2description{$id} = $description;
}

# print Dumper(\%id2description);


say "Read in labels from $labelsFile";
my $id = 'id not defined yet';
open INPUT_FILE, $labelsFile || die "Could not open $labelsFile\n";;
while(! eof INPUT_FILE){
    my $line = <INPUT_FILE>;
    $line = trim($line);
    if($line =~ /^[A-Z0-9]+$/){
        # new id
        $id = $line;
        @id2labels{$id} = ();
    }else{
        # add label
        push @{$id2labels{$id}}, $line;
    }

}

# print Dumper(\%id2labels);

unlink $outputFile;

## Print the new file
say "Print concatenated datapoints into $outputFile";
open OUTPUT_FILE, '>', $outputFile || die "Could not open $outputFile";
for my $id (keys %id2description) {
    # First set the label
    # print $id2title{$id}."\n";
    my $output = join '|', @{$id2labels{$id}};
    # Then add the title
    print "output: ".$output."\n";
    print "title: ".$id2title{$id}."\n";
    print "id: ".$id."\n";
    $output = $output.";".$id2title{$id};
    # Then add the description
    $output = $output.";".$id2description{$id};
    print OUTPUT_FILE $output."\n";
    # exit;
}


sub split_titles {
    my ($line) = @_;
    chomp $line;
    return split ' ', $line, 2;
}

sub get_id_from_feature_file {
    my ($line) = @_;
    chomp $line;
    return (split ' ', $line, 2)[1];
}

sub get_description_from_feature_file {
    my ($line) = @_;
    chomp $line;

    # Divide input into relevant parts
    my $DROP_STRING = "^product\/description:";
    my ($start_text, $description) = $line =~ m/($DROP_STRING) (.*)/g;

    if(!defined $start_text){
        say("ERROR occured in text identifying descriptions:\n".$line."\n");
    }

    return $description;
}

sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
