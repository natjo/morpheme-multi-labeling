#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;
use XML::LibXML;
use File::Find;


# Produces a dataset file similar in the structure to the mercateo dataset
# Call like this:
# $ ./parse_xml.pl data_folder output.txt


my $dataFolder = $ARGV[0];
my $outputFile = $ARGV[1];

open OUTPUT_FILE, '>', $outputFile || die "Could not open $outputFile";

my @allInputFiles;
# Find all files
find({ wanted => \&process_file, follow => 1,  no_chdir => 1 }, $dataFolder);

sub process_file {
    if (-f $_) {
        my ($title, $text, @labels) = extractInfoFromXML($_);

        my $outLine = createDataPointLine($title, $text, @labels);

        ## Print the new file
        print OUTPUT_FILE $outLine."\n";
    }
}



sub extractInfoFromXML{
    my ($file_path) = @_;

    my $title;
    my $text;
    my @labels;

    my $dom = XML::LibXML->load_xml(location => $file_path);

    $title = $dom->findvalue('newsitem/title');

    $text = join ' ', map{
        $_->to_literal()
    } $dom->findnodes('/newsitem/text/p');

    my @topics = grep {
        $_->{class} eq 'bip:topics:1.0';
    } $dom->findnodes('/newsitem/metadata/codes');
    if (@topics){
        @labels = map {
            $_->to_literal()
        } $topics[0]->findnodes('./code/@code');
    }else{
        push @labels, '_no_label_';
    }


    return ($title, $text, @labels);
}


sub createDataPointLine{
    my ($title, $text, @labels) = @_;
    return join('|', @labels).';'.$title.';'.$text;
}


