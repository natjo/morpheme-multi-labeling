import numpy as np
import pickle
from tqdm import tqdm
import os
import sys


FILE_NAME_ALL_LABELS='all_labels_data.txt'
FILE_NAME_ID_EVALS='eval_data_article_ids.txt'

DEFAULT_ID = '_'



def main():
    if(len(sys.argv)<2): print_usage()

    if(sys.argv[1] == 'words'):
        FILE_NAME_WORD2NUMBER='word2number.pcl'
    elif(sys.argv[1] == 'morphemes'):
        FILE_NAME_WORD2NUMBER='morpheme2number.pcl'
    else: print_usage()

    path = sys.argv[2]
    output_dir = os.path.join(path, 'out')
    if(os.path.exists(output_dir)):
        print("Directory \"{}\" already exists. Delete it first, so nothing in there could be accidently overwritten.".format(output_dir))
    os.makedirs(output_dir)

    # Load everything
    label2number = load_label2Num(os.path.join(path, FILE_NAME_ALL_LABELS))
    word2number = load_word2number(os.path.join(path, FILE_NAME_WORD2NUMBER))
    ids = load_evalIDs(os.path.join(path, FILE_NAME_ID_EVALS))

    # Transform datasets
    for file_name in ['eval', 'test', 'training']:

        print('Transform {}'.format(file_name))
        path_output = '{}/{}.csv'.format(output_dir, file_name)
        path_data = '{}/{}.npz'.format(path, file_name)
        features, labels = load_data(path_data)

        pbar = tqdm(total=len(labels))

        with open(path_output, 'w') as output_file:
            for i in range(len(labels)):
                num_features = np.count_nonzero(features[i])
                labels_as_numbers = [num2text_label(w, label2number) for w in labels[i]]
                output_file.write('{},{},{},{}\n'.format(
                                        ids[i] if file_name == 'eval' else DEFAULT_ID,
                                        len(set(labels[i])),
                                        ' '.join(labels_as_numbers),
                                        ' '.join([str(n) for n in features[i][:num_features]]),
                                        ))
                pbar.update(1)
        pbar.close()


    save_all_words(os.path.join(output_dir, 'words.csv'), word2number)
    save_all_labels(os.path.join(output_dir, 'keywords.csv'), label2number)


def num2text_label(num, model):
    return model[num]


def num2text_word(num, model):
    return model[num]


# model is a dictionary
def save_all_words(path, model):
    print("Save all words")
    with open(path, 'w') as output_file:
        for number, word in model.items():
            output_file.write('{}, {}\n'.format(word, number))


# model is a dictionary
def save_all_labels(path, model):
    print("Save all labels")
    with open(path, 'w') as output_file:
        for number, word in model.items():
            output_file.write('{}, {}\n'.format(word, number))



# Read in all labels and predefine a list with numbers and their numbers
def load_label2Num(all_labels_path):
    label2num = {}
    with open(all_labels_path) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            label2num[idx] = line[:-1]
    return label2num


def load_word2number(path):
    model = pickle.load(open(path, 'rb'))
    # Switch keys and values
    model = {y:x for x,y in model.items()}
    return model


def load_data(path):
    npzfile = np.load(path)
    features = npzfile['arr_0']
    labels = npzfile['arr_1']
    return features, labels


def load_evalIDs(path):
    with open(path) as f:
        ids = [l[:-1] for l in f.readlines()]
    return ids

def print_usage():
    print('Not word/morphemes or dataset path specified.\nHow to use: "python {} words|morphemes PREPROCESSED_PATH"'.format(sys.argv[0]))
    exit()


if __name__ == '__main__':
    main()
