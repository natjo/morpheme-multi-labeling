def getConfig():
    return getServerConfig()
    # return getAmazonConfig()
    # return getDeliciousConfig()
    # return getLocalConfig()
    # return getRCV1Config()


def getLocalConfig():
    config = getServerConfig()
    # config['log_dir'] = 'saved_runs_tmp'
    config['network_name'] = 'morphemes'
    config['preprocessed_data_path'] = './data/mercateo/morphemes/preprocessed_small'
    config['num_units'] = [128]
    config['batch_size_train'] = 4
    config['batch_size_test'] = 4
    config['max_num_batches_test'] = 100
    config['save_checkpoints_secs'] = 1
    config['log_training_every_n_steps'] = 50
    config['epochs'] = 10
    config['epochs_with_dropout'] = 5
    return config


def getDeliciousConfig():
    config = getServerConfig()
    config['network_name'] = 'morphemes_delicious'
    config['preprocessed_data_path'] = './data/delicious/morphemes/preprocessed'
    config['num_units'] = [512, 512]
    config['input_sequence_max_length'] = 60
    config['batch_size_train'] = 32
    config['batch_size_test'] = 128
    config['max_num_batches_test'] = 100
    config['epochs'] = 150
    config['epochs_with_dropout'] = 80
    config['embedding_dim'] = 300
    return config


def getAmazonConfig():
    config = getServerConfig()
    config['network_name'] = 'wordss_amazon_long'
    config['preprocessed_data_path'] = './data/amazon12k/words/preprocessed'
    config['num_units'] = [1024]
    config['input_sequence_max_length'] = 200
    config['batch_size_train'] = 64
    config['batch_size_test'] = 128
    config['max_num_batches_test'] = 100
    config['epochs'] = 40 #10
    config['epochs_with_dropout'] = 7
    return config


def getRCV1Config():
    config = getServerConfig()
    config['network_name'] = 'bpe_rcv1_eval_config'
    config['preprocessed_data_path'] = './data/rcv1/bpe/preprocessed'
    config['num_units'] = [1024]
    config['input_sequence_max_length'] = 350
    config['batch_size_train'] = 64
    config['batch_size_test'] = 128
    config['max_num_batches_test'] = 100
    config['epochs'] = 10
    config['epochs_with_dropout'] = 7
    return config


def getServerConfig():
    return {
        # General settings
        'log_dir': 'saved_runs',
        'network_name': 'bpe_always_dropout',
        'preprocessed_data_path': './data/mercateo-fr/bpe/preprocessed',

        'save_checkpoints_secs': 1800,  # This also corresponds to the time in between evaluations
        'log_training_every_n_steps': 900,

        # Training details
        'perc_test': 0.2,
        'eval_size': 100,
        'batch_size_predict': 1,
        'batch_size_train': 128,
        'batch_size_test': 256,
        'max_num_batches_test': 300,
        'inner_keep_prob': 0.8,
        'output_keep_prob': 0.5,
        'perc_epochs_with_dropout': 0.6,  # DEPRECATED
        'epochs_with_dropout': 8, # default 5
        'epochs': 8,  # default 8
        'use_embedding': False,
        'is_embedding_trainable': True,

        'loss_function': 'sigmoidCrossEntropy', # 'sigmoidCrossEntropy','sigmoidCrossEntropyWithL2Reg', 'rankingLoss', 'warpLoss', 'mse', 'sigmoidCrossEntropyWeightedFrequencies', 'sigmoidCrossEntropyWeightedLabels'
        'optimizer': 'Adam', # 'Adam','RMSProp','GradientDescent'
        'init_lr': 0.1, # Not used by all optimizers
        'final_lr': 0.001, # Not used by all optimizers
        'loss_pos_weight': 1, # Used by sigmoidCrossEntropyWeightedExamples

        # Setting the network itself
        'num_units': [512],
        'is_BiLSTM': True,

        # Used for data preprocessing
        'embedding_dim': 200,
        'max_num_labels': 5,
        'word2vec_window': 5,
        'input_sequence_max_length': 80,
        'perc_unknown_tokens': 0.1,  # -1 means no unknown tokens used

	'total_num_features': None,# 1557974, # for mercateo only title
	'total_num_labels': None, #20434, # for mercateo only title
        }
