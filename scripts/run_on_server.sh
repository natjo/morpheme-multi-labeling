#! /bin/bash


NOW=$(date +"%Y%m%d_%H%M%S")
LOG_PATH="saved_runs/run-$NOW.log"

source activate tensorflow_p36

touch $LOG_PATH  # Will throw an error before the real training starts, in case the file can't be created

echo "Run training and shut down afterwards!"
python src/main.py > $LOG_PATH
cat $LOG_PATH

# Only shutdown if no arguments were supplied
if [ -z "$1" ]
  then
    sudo shutdown now
fi
