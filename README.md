Code behind my Master thesis. No datasets are saved in this repository online.

Short description of the main folders and their purpose:
- *src*: contains the main code, defines the network and its training algorithms
- *AWS_tools*: shell scripts to ease the interaction with the AWS instances
- *data*: contains the raw and preprocessed 
- *data/scripts*: contains perl and python scripts to preprocess the datasets, but also to anaylze them
- *saved_runs*: location for evaluation information and saved checkpoints during training
- *thesis_tex*: my master thesis tex files

The makefile describes the most important interaction with the codebase.