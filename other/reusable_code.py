def train_defined_configs():

    ## Different optimizer
    configdict = config.getConfig()
    configdict['loss_function'] = 'sigmoidCrossEntropy'
    configdict['optimizer'] = 'RMSProp'
    train_once(configdict)

    configdict = config.getConfig()
    configdict['loss_function'] = 'sigmoidCrossEntropy'
    configdict['optimizer'] = 'RMSProp'
    train_once(configdict)

    configdict = config.getConfig()
    configdict['loss_function'] = 'sigmoidCrossEntropy'
    configdict['optimizer'] = 'GradientDescent'
    train_once(configdict)


    # Different loss functions with adam
    configdict = config.getConfig()
    configdict['loss_function'] = 'sigmoidCrossEntropyWithL2Reg'
    configdict['optimizer'] = 'Adam'
    train_once(configdict)

    configdict = config.getConfig()
    configdict['loss_function'] = 'mse'
    configdict['optimizer'] = 'Adam'
    train_once(configdict)


    # Different loss functions with RMSProp
    configdict = config.getConfig()
    configdict['loss_function'] = 'sigmoidCrossEntropyWithL2Reg'
    configdict['optimizer'] = 'RMSProp'
    train_once(configdict)

    configdict = config.getConfig()
    configdict['loss_function'] = 'mse'
    configdict['optimizer'] = 'RMSProp'
    train_once(configdict)

    configdict = config.getConfig()
    configdict['loss_function'] = 'rankingLoss'
    configdict['optimizer'] = 'RMSProp'
    train_once(configdict)


    # rankingLoss might be buggy, so I run it last
    configdict = config.getConfig()
    configdict['loss_function'] = 'rankingLoss'
    configdict['optimizer'] = 'Adam'
    train_once(configdict)



def train_defined_configs():
    configDict = config.getConfig()
    configDict['num_units'] = [256]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [512]
    train_once(configDict)

    configDict = config.getConfig()
    configDict['num_units'] = [1024]
    train_once(configDict)



def train_defined_configs():
    configdict = config.getConfig()
    configdict['network_name'] = 'morphemes_delicious'
    configdict['preprocessed_data_path'] = './data/delicious/morphemes/preprocessed'
    configdict['num_units'] = [512]
    train_once(configdict)

    configdict = config.getConfig()
    configdict['network_name'] = 'words_delicious'
    configdict['preprocessed_data_path'] = './data/delicious/words/preprocessed'
    configdict['num_units'] = [512]
    train_once(configdict)

    configdict = config.getConfig()
    configdict['network_name'] = 'morphemes_delicious'
    configdict['preprocessed_data_path'] = './data/delicious/morphemes/preprocessed'
    configdict['num_units'] = [1024]
    train_once(configdict)

    configdict = config.getConfig()
    configdict['network_name'] = 'words_delicious'
    configdict['preprocessed_data_path'] = './data/delicious/words/preprocessed'
    configdict['num_units'] = [1024]
    train_once(configdict)



def train_defined_configs():
    # Normal
    configDict = config.getConfig()
    train_once(configDict)

    # With weighted labels by inversed frequency
    configDict = config.getConfig()
    configDict['loss_function'] = 'sigmoidCrossEntropyWeightedLabels'
    train_once(configDict)

    # With weighted labels pos/neg
    configDict = config.getConfig()
    configDict['loss_function'] = 'sigmoidCrossEntropyWeightedExamples'
    configDict['loss_pos_weight'] = 2.0
    train_once(configDict)

    # With weighted labels pos/neg
    configDict = config.getConfig()
    configDict['loss_function'] = 'sigmoidCrossEntropyWeightedExamples'
    configDict['loss_pos_weight'] = 10.0
    train_once(configDict)

    # With weighted labels pos/neg
    configDict = config.getConfig()
    configDict['loss_function'] = 'sigmoidCrossEntropyWeightedExamples'
    configDict['loss_pos_weight'] = 100.0
    train_once(configDict)

    # With weighted labels pos/neg
    configDict = config.getConfig()
    configDict['loss_function'] = 'sigmoidCrossEntropyWeightedExamples'
    configDict['loss_pos_weight'] = 0.5
    train_once(configDict)

    # Dropout
    configDict = config.getConfig()
    configDict['dropout'] = 160+10
    train_once(configDict)
