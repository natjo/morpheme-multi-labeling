import tensorflow as tf
import os
import config
import time
import tqdm
import utils
import load

"""
All functionality to train or test the network.
"""

def run_training(loss, train_op, metric_op, inner_keep_prob, outer_keep_prob, train_init_op, test_init_op, num_batches):
    sess = tf.Session()
    merged, loss_write, train_writer, test_writer, saver = init_all(sess)
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    print('Start Training in {} epochs'.format(config.EPOCHS), flush=True)
    for epoch in range(config.EPOCHS):
        test(sess, test_init_op, loss, metric_op, num_batches, epoch, test_writer, merged)
        train(sess, train_init_op, loss, train_op, metric_op, inner_keep_prob,outer_keep_prob,num_batches,epoch,train_writer,loss_write)
        saver.save(sess, epoch)
    # One last test after the last training epoch
    test(sess, test_init_op, loss, metric_op, num_batches, epoch+1, test_writer, merged)
    print("Finished training sucessfully!\n-------------------", flush=True)


def train(sess, train_init_op, loss, train_op, metric_op, inner_keep_prob, outer_keep_prob, num_batches, epoch, train_writer, loss_write):
    pbar = utils.init_bar(epoch, num_batches, 'Train')
    sess.run(train_init_op)  # Switch to training data

    if config.SAVE_RUNTIMES:
        # First run of training get metadata, use no dropout for this one batch for sake of simplicity
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
        l, _, lw, global_step = sess.run([loss, train_op, loss_write, tf.train.get_global_step()],
                                        options=run_options, run_metadata=run_metadata)
        train_writer.add_run_metadata(run_metadata, 'step{}'.format(global_step))
        train_writer.add_summary(lw, global_step)

    while True:
        try:
            if epoch >= config.EPOCHS * config.PERC_EPOCH_WITH_DROPOUT:
                l, _, lw, global_step = sess.run([loss, train_op, loss_write, tf.train.get_global_step()])  # No drop out anymore
                utils.update_bar_post(pbar, l, 'off', isProgress=True)
            else:
                l, _, lw, global_step = sess.run([loss, train_op, loss_write, tf.train.get_global_step()])
                utils.update_bar_post(pbar, l, 'on', isProgress=True)

            train_writer.add_summary(lw, global_step)
        except tf.errors.OutOfRangeError:
            break
    pbar.close()


def test(sess, test_init_op, loss, metric_op, num_batches, epoch, test_writer, merged):
    pbar = utils.init_bar(epoch, num_batches/(1.0/config.PERC_TEST-1)+1, 'Test', 0.8)
    sess.run(test_init_op)  # Switch to training data
    reset_metrics_op = tf.variables_initializer(tf.get_collection(tf.GraphKeys.METRIC_VARIABLES))
    sess.run(reset_metrics_op)
    while True:
        try:
            l, _, m, global_step = sess.run([loss, metric_op, merged, tf.train.get_global_step()])
            utils.update_bar_post(pbar, l, 'off', isProgress=True)
        except tf.errors.OutOfRangeError:
            break
    test_writer.add_summary(m, global_step)
    pbar.close()


def run_eval(sess, labels, predictions, eval_init_op):
    sess.run(eval_init_op)
    print("Prediction run successfully")
    num2label = load.num2label(config.PREPROCESSED_DATA_PATH)
    num2articleID = load.num2articleID(config.PREPROCESSED_DATA_PATH)

    if labels != None:
        labels_out, predictions_out = sess.run([labels, predictions], {})
        for i in range(len(predictions_out)):
            articleID = num2articleID[i]
            labelString = ', '.join([num2label[l] for l in sorted(set(labels_out[i]))])
            predictionString = ', '.join([num2label[l] for l in sorted(set(predictions_out[i]))])
            print('{}:[{}] -> [{}]'.format(articleID, labelString, predictionString))
    else:
        predictions_out = sess.run([predictions])
        for i in range(len(predictions_out)):
            articleID = num2articleID[i]
            predictionString = ', '.join([num2label[l] for l in sorted(set(predictions_out[i]))])
            print('[{}]:[{}]'.format(articleID, predictionString))



def run_prediction(checkpointFolder, predictions, init_op, labels=None):
    sess = tf.Session()

    ckpts = tf.train.get_checkpoint_state(checkpointFolder).all_model_checkpoint_paths
    last_ckpt = ckpts[-1]  # Last one is the latest checkpoint
    print("Resuming of {}".format(checkpointFolder))
    saver = tf.train.Saver()
    saver.restore(sess, last_ckpt)
    # reinitialize local variables as they are not saved/restored
    sess.run(tf.local_variables_initializer())

    print("Run prediction")
    run_eval(sess, labels, predictions, init_op)


def init_all(sess):
    merged = tf.summary.merge_all()
    loss_write = tf.summary.merge_all(scope="losses")

    train_file_name = config.LOG_CHECKPOINT_DIR + '/log/train_' + config.NETWORK_NAME
    test_file_name = config.LOG_CHECKPOINT_DIR + '/log/test_' + config.NETWORK_NAME

    timeStr = time.strftime("%Y%m%d_%H%M%S")
    saver = CustomSaver(config.LOG_CHECKPOINT_DIR + '/checkpoints', config.NETWORK_NAME + "_" + timeStr)

    if(config.FILE_WITH_TIME):
        train_file_name += '.' + timeStr
        test_file_name += '.' + timeStr

    train_writer = tf.summary.FileWriter(train_file_name, sess.graph)
    test_writer = tf.summary.FileWriter(test_file_name)


    return merged, loss_write, train_writer, test_writer, saver


# Just a small helper class to handle proper naming during checkpoint creation
class CustomSaver():
    def __init__(self, checkpointFolder, folderName):
        self.checkpointFolder = checkpointFolder
        self.folderName = folderName
        self.saver = tf.train.Saver(max_to_keep=1)

        self.directory = os.path.join(checkpointFolder, folderName)
        os.makedirs(self.directory, exist_ok=True)
        print('Save checkpoints in {}'.format(self.directory))

    def save(self, sess, epoch):
        self.saver.save(sess, '{}/epoch{}.ckpt'.format(self.directory, epoch))
