import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' # Don't tell me I'm using more than 10% of my memory
import tensorflow as tf
from network import Network
import my_excepthook
from load import load_data, load_embedding
from run import run_training, run_eval, run_prediction
from metrics import def_streaming_metrics
import config
import utils
import random


def main():
    # train_once()
    # random_search()
    defined_configs()
    # random_search()
    # predict()

def train_once():
    train_init_op, test_init_op, eval_init_op, features, labels, vocab_size, num_labels, num_batches, num_labels_in_test = load_data(config.PREPROCESSED_DATA_PATH, config.PERC_TEST, config.BATCH_SIZE)

    # Check if embedding is used
    if config.USE_EMBEDDING:
        predefined_embedding = load_embedding(config.PREPROCESSED_DATA_PATH)
    else:
        predefined_embedding = None

    # Define network
    n = Network(config.NUM_UNITS,
            vocab_size,
            config.EMBEDDING_DIM,
            num_labels,
            config.IS_BILSTM,
            predefined_embedding=predefined_embedding,
            isEmbeddingTrainable=config.EMBEDDING_TRAINABLE)

    # Create graph
    loss, train_op, inner_keep_prob, outer_keep_prob, labels_one_hot, predictions_one_hot = n.create_traingraph(features, labels)

    # Define metrics
    metric_op = def_streaming_metrics(predictions_one_hot, labels_one_hot, num_labels, num_labels_in_test)

    print('Train network {} with {} trainable parameters'.format(
        config.NETWORK_NAME, utils.get_total_trainable_parameters()),
        flush=True)
    run_training(loss, train_op, metric_op, inner_keep_prob, outer_keep_prob, train_init_op, test_init_op, num_batches)


def predict():
    train_init_op, test_init_op, eval_init_op, features, labels, vocab_size, num_labels, num_batches, num_labels_in_test = load_data(config.PREPROCESSED_DATA_PATH, config.PERC_TEST, config.BATCH_SIZE)

    if config.USE_EMBEDDING:
        predefined_embedding = load_embedding(config.EMBEDDING_PATH)
    else:
        predefined_embedding = None

    n = Network(config.NUM_UNITS,
            vocab_size,
            config.EMBEDDING_DIM,
            num_labels,
            config.IS_BILSTM,
            predefined_embedding=predefined_embedding)

    predictions = n.create_predictgraph(features, config.EVAL_SIZE)
    run_prediction(config.CKPT_PATH, predictions, eval_init_op, labels)

# Needs to be adjusted to the changing configs
def get_name(nameBeginning):
    return '{}_size_{}-{}_embdim_{}_percWithDrop_{}_isBi_{}'.format(nameBeginning, config.NUM_UNITS[0], config.NUM_UNITS[1], config.EMBEDDING_DIM, config.PERC_EPOCH_WITH_DROPOUT, config.IS_BILSTM)


def random_search():
    for i in range(0,config.NUM_RUNS):
        config.NUM_UNITS[0] = random.randint(2,50) * 10
        config.NUM_UNITS[1] = random.randint(2,config.NUM_UNITS[0]/10) * 10
        config.EMBEDDING_DIM = random.randint(10,50) * 10
        config.PERC_EPOCH_WITH_DROPOUT = random.choice([0.5, 0.0])
        config.IS_BILSTM = random.choice([True, False])
        for _input in ['morphemes', 'words']:
            config.PREPROCESSED_DATA_PATH = './data/delicious/{}/preprocessed'.format(_input)
            config.NETWORK_NAME = get_name(_input)
            train_once()
            reset_everything()


def defined_configs():
    baseName = config.NETWORK_NAME

    config.NUM_UNITS = [150,50]
    config.PERC_EPOCH_WITH_DROPOUT = 0.5
    config.IS_BILSTM = True
    config.NETWORK_NAME = get_name(baseName)
    train_once()
    reset_everything()


    config.NUM_UNITS = [250,200]
    config.PERC_EPOCH_WITH_DROPOUT = 0.5
    config.IS_BILSTM = False
    config.NETWORK_NAME = get_name(baseName)
    train_once()
    reset_everything()


    config.NUM_UNITS = [250,200]
    config.PERC_EPOCH_WITH_DROPOUT = 1.0
    config.IS_BILSTM = True
    config.NETWORK_NAME = get_name(baseName)
    train_once()
    reset_everything()


    config.NUM_UNITS = [250,200]
    config.PERC_EPOCH_WITH_DROPOUT = 0.5
    config.IS_BILSTM = True
    config.NETWORK_NAME = get_name(baseName)
    train_once()
    reset_everything()


    config.NUM_UNITS = [400,300]
    config.PERC_EPOCH_WITH_DROPOUT = 0.5
    config.IS_BILSTM = True
    config.NETWORK_NAME = get_name(baseName)
    train_once()
    reset_everything()


def reset_everything(seed=1607):
    tf.set_random_seed(seed)
    tf.reset_default_graph()


if __name__ == '__main__':
    main()


