import tensorflow as tf
import utils

def metric_variable(name, shape, dtype=tf.int32, initializer=tf.zeros_initializer):
    """Create variable in `GraphKeys.(LOCAL|METRIC_VARIABLES`) collections."""
    return tf.get_variable(
            name,
            shape=shape,
            dtype=dtype,
            initializer=initializer,
            collections=[tf.GraphKeys.LOCAL_VARIABLES, tf.GraphKeys.METRIC_VARIABLES],
            )


def def_streaming_metrics(predictions, groundTruth, num_labels, num_labels_in_test):
    # Define variables
    with tf.variable_scope('internal_statistics'):
        tp_mic = metric_variable('tp_mic', shape=[])
        fp_mic = metric_variable('fp_mic', shape=[])
        fn_mic = metric_variable('fn_mic', shape=[])

        tp_mac = metric_variable('tp_mac', shape=[num_labels])
        fp_mac = metric_variable('fp_mac', shape=[num_labels])
        fn_mac = metric_variable('fn_mac', shape=[num_labels])

        # Update ops
        up_tp_mac = tf.assign_add(tp_mac, tf.count_nonzero(predictions * groundTruth, axis=0, dtype=tf.int32))
        up_fp_mac = tf.assign_add(fp_mac, tf.count_nonzero(predictions * (groundTruth - 1), axis=0, dtype=tf.int32))
        up_fn_mac = tf.assign_add(fn_mac, tf.count_nonzero((predictions - 1) * groundTruth, axis=0, dtype=tf.int32))

        up_tp_mic = tf.assign_add(tp_mic, tf.count_nonzero(predictions * groundTruth, axis=None, dtype=tf.int32))
        up_fp_mic = tf.assign_add(fp_mic, tf.count_nonzero(predictions * (groundTruth - 1), axis=None, dtype=tf.int32))
        up_fn_mic = tf.assign_add(fn_mic, tf.count_nonzero((predictions - 1) * groundTruth, axis=None, dtype=tf.int32))

        # Define update op
        update_op = tf.group(up_tp_mic, up_fp_mic, up_fn_mic, up_tp_mac, up_fp_mac, up_fn_mac)

    with tf.variable_scope('update_metrics'):
        # Calculate all metrics
        # Macro
        correction = num_labels / num_labels_in_test  # Not all labels appear in the test set, the mean is done over all labels though
        prec_mac = utils.reduce_mean_handle_nan(tp_mac / (tp_mac + fp_mac)) * correction
        rec_mac = utils.reduce_mean_handle_nan(tp_mac / (tp_mac + fn_mac)) * correction
        f1_mac = utils.replace_nan_with_zero(2 * prec_mac * rec_mac / (prec_mac + rec_mac))

        # Micro
        prec_mic = utils.replace_nan_with_zero(tp_mic / (tp_mic + fp_mic))
        rec_mic = utils.replace_nan_with_zero(tp_mic / (tp_mic + fn_mic))
        f1_mic = utils.replace_nan_with_zero(2 * prec_mic * rec_mic / (prec_mic + rec_mic))

    with tf.variable_scope('streaming_metrics'):
        tf.summary.scalar('micro_F1', f1_mic)
        tf.summary.scalar('macro_F1', f1_mac)
        tf.summary.scalar('micro_precision', prec_mic)
        tf.summary.scalar('macro_precision', prec_mac)
        tf.summary.scalar('micro_recall', rec_mic)
        tf.summary.scalar('macro_recall', rec_mac)

    with tf.variable_scope('statistics'):
        # Use reduce mean since reduce sum is already the micro value
        tf.summary.scalar('micro_TP', tp_mic)
        tf.summary.scalar('macro_TP', tf.reduce_mean(tp_mac))
        tf.summary.scalar('micro_FP', fp_mic)
        tf.summary.scalar('macro_FP', tf.reduce_mean(fp_mac))
        tf.summary.scalar('micro_FN', fn_mic)
        tf.summary.scalar('macro_FN', tf.reduce_mean(fn_mac))

    return update_op


def add_old_metrics(predictions, labels):
    with tf.variable_scope('metrics'):
        # Variables are named by the subsets of binary classifications they represent
        predictions = tf.cast(predictions, tf.float32)
        labels = tf.cast(labels, tf.float32)

        TP = predictions * labels
        FN_2TP_FP = predictions + labels
        TP_FP = predictions
        TP_FN = labels

        with tf.variable_scope('f1_score'):
            macro_f1 = 2 * utils.reduce_sum_handle_nan(TP)/tf.reduce_sum(FN_2TP_FP)
            micro_f1 = 2 * utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(FN_2TP_FP,0))
        with tf.variable_scope('precision'):
            macro_precision = utils.reduce_sum_handle_nan(TP)/(utils.reduce_sum_handle_nan(TP_FP) + 1e-8) # handle no true predictions
            micro_precision = utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FP,0))
        with tf.variable_scope('recall'):
            macro_recall = tf.reduce_sum(TP)/tf.reduce_sum(TP_FN)
            micro_recall = utils.reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FN,0))

        tf.summary.scalar('micro_F1', micro_f1)
        tf.summary.scalar('macro_F1', macro_f1)
        tf.summary.scalar('micro_precision', micro_precision)
        tf.summary.scalar('macro_precision', macro_precision)
        tf.summary.scalar('micro_recall', micro_recall)
        tf.summary.scalar('macro_recall', macro_recall)

    with tf.variable_scope('tf_metrics'):
        recall, recall_op = tf.metrics.recall(labels, predictions)
        precision, prec_op = tf.metrics.precision(labels, predictions)
        f1 = 2 * precision * recall / (precision + recall)

        tf.summary.scalar('tf_recall', recall)
        tf.summary.scalar('tf_precision', precision)
        tf.summary.scalar('tf_f1', f1)

    return tf.group(prec_op, recall_op)


