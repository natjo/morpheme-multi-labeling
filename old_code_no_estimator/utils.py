import tqdm
import tensorflow as tf
import logging
import time


# Not working currently
def set_tf_logger():
    # get TF logger
    log = logging.getLogger('tensorflow')
    log.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    timeStr = time.strftime("%Y%m%d_%H%M%S")
    fh = logging.FileHandler('log/tensorflow_{}.log'.format(timeStr))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    log.addHandler(fh)


def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summary_'+name):
        with tf.name_scope('summaries_' + name):
            mean = tf.reduce_mean(var)
        tf.summary.scalar('mean_' + name, mean)
        with tf.name_scope('stddev_' + name):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev_' + name, stddev)
        tf.summary.scalar('max_' + name, tf.reduce_max(var))
        tf.summary.scalar('min_' + name, tf.reduce_min(var))
        tf.summary.histogram(name, var)


# See https://stackoverflow.com/a/47643427/4296244 for details
# name shoulf be fw or bw
def log_lstm_variables(cells, name):
    for idx, one_lstm_cell in enumerate(cells):
        one_kernel, one_bias = one_lstm_cell.variables
        # I think TensorBoard handles summaries with the same name fine.
        with tf.variable_scope('{}_lstm_layer_{}'.format(name, idx)):
            tf.summary.histogram("Kernel", one_kernel)
            tf.summary.histogram("Bias", one_bias)


def reduce_mean_handle_nan(ins):
    return tf.reduce_mean(replace_nan_with_zero(ins), name='reduce_mean_handle_nan')


def reduce_sum_handle_nan(ins):
    return tf.reduce_sum(replace_nan_with_zero(ins), name='reduce_sum_handle_nan')


def replace_nan_with_zero(ins):
    return tf.where(tf.is_nan(ins), tf.zeros_like(ins), ins)


# perc_ncols is there to reduce or increase the total length of the bar
def init_bar(epoch, num_batches, name, perc_ncols=1.0):
    return tqdm.tqdm(total=int(num_batches),
            desc="{} (E:{})".format(name, epoch),
            ascii=True,
            ncols=int(100 * perc_ncols),
            bar_format='{desc}|{bar}| {n_fmt}/{total_fmt} [{elapsed}<{remaining}, {rate_fmt}{postfix}]',
            postfix={'-', 'loss', '-', 'dropout'})


def update_bar_post(bar, l, d, isProgress=False):
    bar.set_postfix(loss="{:0.6f}".format(l), dropout=d)
    if isProgress:
        bar.update(1)


def indexes2labels(indexes):
    model = load_numer2label('./data/general_data/all_labels_data.txt')
    labels = [getNumber2Label(model, i) for i in indexes]
    return labels


def getNumber2Label(model, number):
    if number in model.keys():
        return model[number]
    else:
        raise KeyError('"{}" is not a valid index'.format(number))


def load_numer2label(all_labels_path):
    number2label = {}
    with open(all_labels_path) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            number2label[idx] = line[:-1]
    return number2label

def get_total_trainable_parameters():
    total_parameters = 0
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        shape = variable.get_shape()
        variable_parameters = 1
        for dim in shape:
            variable_parameters *= dim.value
        total_parameters += variable_parameters
    return total_parameters
