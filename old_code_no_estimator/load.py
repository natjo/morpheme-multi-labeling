import tensorflow as tf
import numpy as np
import math
import os


# Returns:
# init_ops to switch datasets
# features and labels to feed to the network
# vocab_size is the number of different features
# num_labels is the number of different labels
# number_batches is the number of total batches during one run through the dataset
def load_data(path, perc_test, batch_size):
    print('Load all data from "{}"'.format(path))
    test_features, test_labels = load_from_npz(os.path.join(path, 'test.npz'), dtype=np.int32)
    train_features, train_labels = load_from_npz(os.path.join(path, 'training.npz'), dtype=np.int32)
    eval_features, eval_labels = load_from_npz(os.path.join(path, 'eval.npz'), dtype=np.int32)

    # Calculate some relevant data
    num_labels_in_test, _ = get_statistics(test_features, test_labels, perc_test, batch_size, 'test')
    get_statistics(eval_features, eval_labels, perc_test, batch_size, 'eval')
    num_labels, num_batches = get_statistics(train_features, train_labels, perc_test, batch_size, 'train')
    test_size = test_labels.shape[0]
    eval_size = eval_labels.shape[0]
    # TODO get max of train_features and test_features to find the highest number for a word
    vocab_size = np.max(train_features) + 1  # To account for the zero


    # Create the dataset objects
    with tf.variable_scope('dataset_preparation'):
        train_dataset = tf.data.Dataset.from_tensor_slices((train_features, train_labels)).shuffle(1000).batch(batch_size)
        test_dataset = tf.data.Dataset.from_tensor_slices((test_features, test_labels)).batch(batch_size)
        eval_dataset = tf.data.Dataset.from_tensor_slices((eval_features, eval_labels)).batch(eval_size)

        # Create generic iterator
        iterator = tf.data.Iterator.from_structure(train_dataset.output_types, train_dataset.output_shapes)

        # Create initializer for the two datasets
        test_init_op = iterator.make_initializer(test_dataset)
        train_init_op = iterator.make_initializer(train_dataset)
        eval_init_op = iterator.make_initializer(eval_dataset)

    features, labels = iterator.get_next(name='data_iterator')

    return train_init_op, test_init_op, eval_init_op, features, labels, vocab_size, num_labels, num_batches, num_labels_in_test


# TF can't handle shifting around large input dimensions. Using placeholders solves this
def create_dataset_from_numpy(features, labels):
    features_placeholder = tf.placeholder(features.dtype, features.shape)
    labels_placeholder = tf.placeholder(labels.dtype, labels.shape)
    return tf.data.Dataset.from_tensor_slices((features_placeholder, labels_placeholder))
    # If this is used, the placeholders need to be filled during the session runs like this:
    # sess.run(iterator.initializer, feed_dict={features_placeholder: features,
    #                                           labels_placeholder: labels})


def load_from_npz(path, dtype):
    npzfile = np.load(path)
    features = npzfile['arr_0'].astype(dtype)
    labels = npzfile['arr_1'].astype(dtype)
    return features, labels


def num2label(path):
    filePath = os.path.join(path, 'all_labels_data.txt')
    num2label = {}
    with open(filePath) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            num2label[idx] = line[:-1]
    return num2label


def num2articleID(path):
    filePath = os.path.join(path, 'eval_data_article_ids.txt')
    num2articleID = {}
    with open(filePath) as f:
        lines = f.readlines()
        for idx, line in enumerate(lines):
            num2articleID[idx] = line[:-1]
    return num2articleID


# name is only relevant for the printed output here
def get_statistics(features, labels, perc_test, batch_size, name=None):
    num_labels = np.max(labels) + 1  # To account for the zero
    dataset_size =labels.shape[0]
    num_features = features.shape[1]
    num_batches = math.ceil(dataset_size/batch_size)
    if(name):
        print('{}: Loaded {} datapoints of dimension {} and {} labels in {} batches'
                .format(name, dataset_size,num_features,num_labels,num_batches))
    return num_labels, num_batches


def load_embedding(path):
    return np.load(os.path.join(path, 'embedding_matrix.npy'))
