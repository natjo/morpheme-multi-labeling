import tensorflow as tf
import utils
import math


class Network():
    def __init__(self, num_units, vocab_size, embedding_dim, num_labels, isBiLSTM=True, predefined_embedding=None, isEmbeddingTrainable=False):
        self.num_units = num_units
        self.isEmbeddingTrainable = isEmbeddingTrainable
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.num_labels = num_labels
        self.predefined_embedding = predefined_embedding
        self.isBiLSTM = isBiLSTM
        self.inner_keep_prob = tf.placeholder_with_default(1.0, shape=None, name='inner_keep_prob')
        self.outer_keep_prob = tf.placeholder_with_default(1.0, shape=None, name='outer_keep_prob')


    def _create_networkcore(self, features):
        # Load the embedding
        if self.predefined_embedding is None:
            embedding_init = tf.random_uniform_initializer(-1, 1)
            shape = [self.vocab_size, self.embedding_dim]
        else:
            embedding_init = tf.constant(self.predefined_embedding)
            shape = None

        embedding_matrix = tf.get_variable('input_symbol_embeddings',
                                        shape=shape,
                                        initializer=embedding_init,
                                        trainable=self.isEmbeddingTrainable)
        embedded_features = tf.nn.embedding_lookup(embedding_matrix, features)

        if self.isBiLSTM:
            fw_cells = original_fw_cells = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]
            bw_cells = original_bw_cells = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]

            fw_cells = [tf.nn.rnn_cell.DropoutWrapper(lstm, state_keep_prob=self.inner_keep_prob) for lstm in fw_cells]
            bw_cells = [tf.nn.rnn_cell.DropoutWrapper(lstm, state_keep_prob=self.inner_keep_prob) for lstm in bw_cells]

            # Output of lstms
            _, fw_out, bw_out = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(fw_cells, bw_cells, embedded_features, sequence_length=tf.count_nonzero(features, 1), dtype=tf.float32)
            utils.log_lstm_variables(original_fw_cells, 'fw')
            utils.log_lstm_variables(original_bw_cells, 'bw')
            fw_out_encoded = fw_out[-1].h
            bw_out_encoded = bw_out[-1].h
            core_out = tf.concat([fw_out[-1].h, bw_out[-1].h], axis=1)
            weights_shape =[self.num_units[-1] * 2, self.num_labels]

        else:
            lstms = original_lstm = [tf.nn.rnn_cell.LSTMCell(num_units=size, name='lstm_cell_{}'.format(idx))
                    for idx, size in enumerate(self.num_units)]
            lstms = [tf.nn.rnn_cell.DropoutWrapper(lstm,state_keep_prob=self.inner_keep_prob) for lstm in lstms]

            # Network structure
            cell = tf.contrib.rnn.MultiRNNCell(lstms)

            # Output of lstms
            stm_outputs, encoder_state = tf.nn.dynamic_rnn(cell, embedded_features, sequence_length=tf.count_nonzero(features, 1), dtype=tf.float32)
            utils.log_lstm_variables(original_lstm, 'lstm')
            core_out = encoder_state[-1].h
            weights_shape =[self.num_units[-1], self.num_labels]

        # lstm output to final prediction
        with tf.variable_scope('output_weights'):
            size_core_out = int(core_out.get_shape()[1])
            weight_init = tf.truncated_normal([size_core_out, self.num_labels]) #, stddev=1.0/ math.sqrt(size_core_out))
            weights = tf.get_variable('output_weights', initializer=weight_init)
            utils.variable_summaries(weights, name='output_weights')
        with tf.variable_scope('output_biases'):
            biases = tf.get_variable('output_biases', initializer=tf.constant(0.1, shape=[self.num_labels]))
            utils.variable_summaries(biases, name='output_biases')

        return core_out, weights, biases


    def create_traingraph(self, features, labels):
        core_out, weights, biases = self._create_networkcore(features)
        core_out = tf.nn.dropout(core_out, self.outer_keep_prob)

        with tf.variable_scope('label_multi_hot_encoding'):
            labels_one_hot = tf.reduce_max(tf.one_hot(labels, self.num_labels), axis=1)

        with tf.variable_scope('output_calculations'):
            logits = tf.nn.bias_add(tf.matmul(core_out, weights), biases)
            predictions_one_hot = tf.cast(tf.greater(tf.nn.sigmoid(logits), 0.5), tf.int32)

        with tf.variable_scope('sigmoid_cross_entropy'):
            loss0 = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels_one_hot, logits=logits)
            loss = tf.reduce_mean(loss0)

        with tf.variable_scope('losses'):
            tf.summary.scalar('sigmoid_cross_entropy', loss)

        optimizer = tf.train.AdamOptimizer()
        train_op = optimizer.minimize(loss, global_step=tf.train.create_global_step())

        labels_one_hot = tf.cast(labels_one_hot, tf.int32)  # Preferred output dimension, since labels should not be float
        return loss, train_op, self.inner_keep_prob, self.outer_keep_prob, labels_one_hot, predictions_one_hot


    def create_predictgraph(self, features, batch_size):
        core_out, weights, biases = self._create_networkcore(features)

        with tf.variable_scope('output_calculations'):
            logits = tf.nn.bias_add(tf.matmul(core_out, weights), biases)
            predictions_one_hot = tf.cast(tf.greater(tf.nn.sigmoid(logits), 0.5), tf.int32)

            pos_labels = tf.cast(tf.where(predictions_one_hot), tf.int32)
            predictions = tf.dynamic_partition(pos_labels[:, 1], pos_labels[:, 0], batch_size)

        return predictions


    # def get_predictions(self, batch_size):
    #     pos_labels = tf.cast(tf.where(self.predictions_one_hot), tf.int32)
    #     predictions = tf.dynamic_partition(pos_labels[:, 1], pos_labels[:, 0], batch_size)
    #     return predictions
